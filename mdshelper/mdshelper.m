//
//  mdshelper.m
//  mdshelper
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "mdshelper.h"
#include <string.h>
#include <sys/stat.h>
#import <AppKit/AppKit.h>
#import "TCTaskWrapperWithBlocks.h"
#import "TCSConstants.h"
#import "TCSWebserverSetting.h"
#import "TCSConstants.h"
#define LOGFILE "/Library/Logs/mdshelper.log"

@interface mdshelper() <NSXPCListenerDelegate>
@property (atomic, strong, readwrite) NSXPCListener * listener;
@property (strong) NSString *installInstallMacOSScriptPath;
@property (strong) NSString *createMunkiRepoScript;
@property (strong) TCTaskWrapperWithBlocks *createBootDiskTaskWrapper;
@property (strong) TCTaskWrapperWithBlocks *task;
@property (assign) BOOL isCancelling;
@end
@implementation mdshelper
void logLine(char *line){

    FILE *f_log;
    if((f_log=fopen(LOGFILE, "a"))==NULL){
        fprintf(stderr, "error opening log file");
        return;
    }

    size_t len=strnlen(line, 1024);
    fwrite(line,len, 1, f_log);
    fclose(f_log);

}
+ (NSString *)machServiceName { return @"com.twocanoes.mdshelpertool"; }

+ (NSString *)exposedProtocolName { return @"mdshelperProtocol"; }

- (void)getVersionCallback:(void (^)(NSInteger))callback {

    NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    callback(versionString.integerValue);

}

-(BOOL)isTokenValid:(NSData *)token{
    int myFlags = kAuthorizationFlagDefaults |
    kAuthorizationFlagExtendRights ;

    AuthorizationExternalForm myExternalAuthorizationRef;

    memcpy(&myExternalAuthorizationRef, [token bytes], kAuthorizationExternalFormLength);
    AuthorizationRef newRef;
    OSStatus myStatus = AuthorizationCreateFromExternalForm (&myExternalAuthorizationRef,
                                                    &newRef);

    //kAuthorizationRightExecute
    AuthorizationItem right = {TCSAUTHRIGHT, 0, NULL, 0};
    AuthorizationRights rights = {1, &right};

    myStatus = AuthorizationCopyRights (newRef, &rights,kAuthorizationEmptyEnvironment, myFlags, NULL);

    if (myStatus==0) return YES;
    return NO;
}
-(BOOL)isCorrectlySignedClientWithPID:(int)pid{

    CFNumberRef value = NULL;
    CFDictionaryRef attributes = NULL;

    SecCodeRef code = NULL;
    OSStatus status;


    value = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &pid);
    if (!value) {

        logLine("Error creating number");
        return NO;

    }
    attributes = CFDictionaryCreate(kCFAllocatorDefault, (const void **)&kSecGuestAttributePid, (const void **)&value, 1, NULL, NULL);
    if (!attributes) {

        logLine("Error creating attributes");
        return NO;

    }
    status = SecCodeCopyGuestWithAttributes(NULL, attributes, kSecCSDefaultFlags, &code);
    if (status!=0) {

        logLine("Error creating SecCodeCopyGuestWithAttributes");
        return NO;

    }

    NSString *entitlement = @"anchor apple generic and certificate leaf[subject.OU] = UXP6YEHSPW";
    SecRequirementRef requirement = NULL;
    CFErrorRef errors = NULL;
    status = SecRequirementCreateWithStringAndErrors((__bridge CFStringRef)entitlement, kSecCSDefaultFlags, &errors, &requirement);
    if (status!=0) {

        logLine("Error creating SecRequirementCreateWithStringAndErrors");
        return NO;

    }
    status = SecCodeCheckValidity(code, kSecCSDefaultFlags, requirement);
    if (status!=0) {

        logLine("Request from invalid client. Refusing");
        return NO;

    }

    NSArray *runningApps=[[NSWorkspace sharedWorkspace] runningApplications];

    [runningApps enumerateObjectsUsingBlock:^(NSRunningApplication *app, NSUInteger idx, BOOL * _Nonnull stop) {

        if (app.processIdentifier == pid){
            NSString *currAppPath=[[app.bundleURL URLByAppendingPathComponent:@"Contents/Resources/installinstallmacos.py"] path];

            if ([self isScriptValid:currAppPath]==YES){

                self.installInstallMacOSScriptPath=currAppPath;

            }
            NSString *createMunkiRepoScriptPath=[[app.bundleURL URLByAppendingPathComponent:@"Contents/Resources/create_repo.sh"] path];

            if ([self isScriptValid:createMunkiRepoScriptPath]==YES){

                self.createMunkiRepoScript=createMunkiRepoScriptPath;

            }
            *stop=YES;
        }


    }];

    return YES;

}
-(BOOL)isScriptValid:(NSString *)scriptPath{

    BOOL isValid=NO;
    NSFileManager *fm=[NSFileManager defaultManager];
    if ([fm fileExistsAtPath:scriptPath]){

        NSDictionary *dict=[fm attributesOfItemAtPath:scriptPath error:nil];

        if (dict && [dict objectForKey: NSFilePosixPermissions] && [[dict objectForKey: NSFilePosixPermissions] intValue]==0755 && [dict objectForKey:NSFileOwnerAccountID] && [[dict objectForKey:NSFileOwnerAccountID] intValue]==0){
            isValid=YES;
        }
    }

    return isValid;
}
- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection
// Called by our XPC listener when a new connection comes in.  We configure the connection
// with our protocol and ourselves as the main object.
{

    if([self isCorrectlySignedClientWithPID:newConnection.processIdentifier]==NO){
        return NO;
    }

    assert(listener == self.listener);
#pragma unused(listener)
    assert(newConnection != nil);
//    os_log_error(self.log, "Privileged helper received a new connections");
    NSSet *incomingClasses = [NSSet setWithObjects:[TCSWebserverSetting class], [NSString class], [NSArray class],[NSNumber class],[NSDictionary class],[NSError class], [NSData class], nil];

    NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:NSProtocolFromString([[self class] exposedProtocolName])];
    [interface setClasses:incomingClasses forSelector:@selector(updateWebserverWithAuth:configurations:withCallback:) argumentIndex:1 ofReply:NO];
    newConnection.exportedInterface = interface;
    newConnection.exportedObject = self;
    [newConnection resume];


    return YES;
}
- (void)runXPCService
{

    // Tell the XPC listener to start processing requests.
        self.listener = [[NSXPCListener alloc] initWithMachServiceName:[[self class] machServiceName]];
        self.listener.delegate = self;
        [self.listener resume];

//     Run the run loop forever.

        [[NSRunLoop currentRunLoop] run];
}

- (void)stopRunningProcessesWithAuth:(nonnull NSData *)token callback:(nonnull void (^)(BOOL))callback {

    self.isCancelling=YES;
    if (self.task) [self.task terminate];
    if (self.createBootDiskTaskWrapper) [self.createBootDiskTaskWrapper terminate];
    callback(YES);
}
- (void)updateWebserverWithAuth:(nonnull NSData *)token configurations:(nonnull NSDictionary *)configuration withCallback:(nonnull void (^)(BOOL))callback {


    NSArray *configurations=[configuration objectForKey:@"webserverSettings"];
    __block BOOL hasSSL=[[configuration objectForKey:@"hasSSL"] boolValue];
    __block BOOL enablePHP7=[[configuration objectForKey:@"enablePHP7"] boolValue];


    NSMutableString *configString=[NSMutableString string];
    [configurations enumerateObjectsUsingBlock:^(TCSWebserverSetting *setting, NSUInteger idx, BOOL * _Nonnull stop) {

        [configString appendFormat:@"Listen %li\n<VirtualHost *:%li>\n",setting.port ,setting.port];

        if (setting.useTLS==YES){
            [configString appendString:@"SSLEngine On\n"];

        }
        if (setting.enablePHP7==YES){
            [configString appendString:@"<IfModule php7_module>\nAddType application/x-httpd-php .php\nAddType application/x-httpd-php-source .phps\n<IfModule dir_module>\nDirectoryIndex index.html index.php\n</IfModule>\n</IfModule>\n"];

        }

        [configString appendFormat:@"DocumentRoot \"%@\"\n",setting.path];
        [configString appendFormat:@"<Directory \"%@\">\n",setting.path];
        [configString appendString:@"AllowOverride None\nRequire all granted\nMultiviewsMatch Any\n"];
        if (setting.allowDirectoryListing==YES){
            [configString appendString:@"Options +Indexes\n"];
        }
        [configString appendString:@"</Directory>\n"];
        [configString appendString:@"</VirtualHost>\n"];



    }];
    if (hasSSL==YES){
        NSString *certificatePath=[configuration objectForKey:@"certificatePath"];
        NSString *keyPath=[configuration objectForKey:@"keyPath"];

        if (!certificatePath || !keyPath) {


            NSLog(@"SSL selected SSL certificate or key path was not set. Please select or create a SSL certificate in Security");

            callback(NO);
            return;
        }

        [configString insertString:[NSString stringWithFormat:@"LoadModule ssl_module libexec/apache2/mod_ssl.so\nSSLCertificateFile \"%@\"\nSSLCertificateKeyFile \"%@\"\n",certificatePath,keyPath] atIndex:0];
    }
    if (enablePHP7==YES){

        [configString insertString:@"LoadModule php7_module libexec/apache2/libphp7.so\n" atIndex:0];
       }
   

    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *err;

    if ([fm fileExistsAtPath:TCSAPACHECONFIG]){
        if([fm removeItemAtPath:TCSAPACHECONFIG error:&err]==NO){

            NSLog(@"%@",err.localizedDescription);
            callback(NO);
        }
    }

    if([configString writeToFile:TCSAPACHECONFIG atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){
        NSLog(@"%@",err.localizedDescription);
        callback(NO);

    }
    callback(YES);

}
- (void)stopWebserverWithAuth:(nonnull NSData *)token callback:(nonnull void (^)(BOOL))callback {

    if ([self isTokenValid:token]==YES) {
        int res=system("/usr/sbin/apachectl stop");
        if (res==0) callback(YES);
        callback(NO);
    }
    else {
        callback(NO);
    }

}

-(void)trustCertificateAtPath:(NSString *)certPath withCallback:(void (^)(NSError *err))callback{

    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:certPath]) {

        NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/bin/security" arguments:@[@"add-trusted-cert",@"-d",@"-r",@"trustRoot",@"-k",@"/Library/Keychains/System.keychain",certPath]];

        [task waitUntilExit];

        if (task.terminationStatus!=0) {


            NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"There was an error trusting the TLS certificate: %i",task.terminationStatus]}];
            callback(err);

        }
        else callback(nil);
    }
    else {
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The certificate does not exist at %@. Trusting failed.",certPath]}];

        callback(err);
    }

}
- (void)startWebserverWithAuth:(nonnull NSData *)token callback:(nonnull void (^)(BOOL))callback {

    if ([self isTokenValid:token]==YES) {

        system("/usr/sbin/apachectl start");
        callback(YES);
    }
    else {
        callback(NO);
    }
}
- (void)startMicroMDMWithAuth:(nonnull NSData *)token settings:(nonnull NSString *)settings callback:(nonnull void (^)(NSError * _Nullable))callback {

    if (settings){

        NSError *err;
        NSFileManager *fm=[NSFileManager defaultManager];

        NSString *settingsFilePath=@"/usr/local/mds-micromdm/etc/settings";
        if ([fm fileExistsAtPath:@"/usr/local/mds-micromdm/etc"]==NO){
            if([fm createDirectoryAtPath:@"/usr/local/mds-micromdm/etc/" withIntermediateDirectories:YES attributes:nil error:&err]==NO){

                callback(err);
                return;
            }


        }
        if([settings writeToFile:settingsFilePath atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){
            callback(err);
        }

        int res=system("/bin/launchctl load -w /Library/LaunchDaemons/com.twocanoes.mds.micromdm.plist");
        sleep(3);
        if (res!=0) {
            NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"There was an erorr starting the MDM Service."}];

            callback(err);
            return;
        }
    }
    callback(nil);

}

- (void)stopMicroMDMWithAuth:(nonnull NSData *)token callback:(nonnull void (^)(NSError * _Nullable))callback {
    int res=system("/bin/launchctl unload -w /Library/LaunchDaemons/com.twocanoes.mds.micromdm.plist");
    sleep(1);
    if (res!=0) {
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"There was an error stopping the MDM Service."}];

        callback(err);
        return;
    }
    callback(nil);

}

- (void)createMacOSInstallWithAuth:(nonnull NSData *)token volume:(nonnull NSString *)volumePath withInstaller:(nonnull NSString *)installerPath callback:(nonnull void (^)(BOOL, NSString * , NSError * _Nullable))callback {

    dispatch_async(dispatch_get_main_queue(), ^{


        NSFileManager *fm=[NSFileManager defaultManager];

        NSError *err;
        NSArray *contents;
        contents=[fm contentsOfDirectoryAtPath:volumePath error:&err];
        if(!contents){

            NSLog(@"error accessing volume: %@",err.localizedDescription);
            logLine((char *)err.localizedDescription.UTF8String);
            logLine("\n");
            NSError *err=[NSError errorWithDomain:@"TCS" code:-5 userInfo:@{NSLocalizedDescriptionKey:@"MDS does not have access to the selected volume. Please open System Preferences->Security & Privacy and allow MDS full disk access."}];

            callback(YES,nil,err);
            return;

        }

        if (volumePath && installerPath) {
            NSString *createInstallMediaPath=[installerPath stringByAppendingPathComponent:@"Contents/Resources/createinstallmedia"];


            self.createBootDiskTaskWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

            } endBlock:^{
                if (self.createBootDiskTaskWrapper.terminationStatus!=0) {

                    if (self.isCancelling==NO){
                    NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"There was an error creating the bootable disk on %@. Please verify that the disk is not write protected and that the macOS installer is not in a protected location.",volumePath]}];

                        callback(YES,nil,err);
                    }

                    else {
                        NSError *err=[NSError errorWithDomain:@"TCS" code:-100 userInfo:@{NSLocalizedDescriptionKey:@"cancelled"}];

                            callback(YES,nil,err);


                    }
                }
                else {
                    callback(YES,nil,nil);
                }

            } outputBlock:^(NSString *output) {

                logLine((char *)output.UTF8String);
                logLine("\n");
            } errorOutputBlock:^(NSString *errorOutput) {
                logLine((char *)errorOutput.UTF8String);
                logLine("\n");
            } arguments:@[createInstallMediaPath,@"--volume",volumePath,@"--nointeraction"]];

            [self.createBootDiskTaskWrapper startProcess];
    //        NSTask *task=[NSTask launchedTaskWithLaunchPath:createInstallMediaPath arguments:@[@"--volume",volumePath,@"--nointeraction"]];
    //
    //        [task waitUntilExit];
    //
    //
    //        if (task.terminationStatus!=0) {
    //
    //            NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"There was an error creating the bootable disk on %@. Please verify that the disk is not write protected and that the macOS installer is not in a protected location.",volumePath]}];
    //
    //            callback(err);
    //
    //        }
    //        else {
    //            callback(nil);
    //        }

        }
    });
}
- (void)createMunkiRepoWithAuth:(nonnull NSData *)token path:(nonnull NSString *)munkiRepoPath callback:(nonnull void (^)(NSError * _Nullable))callback {


    NSFileManager *fm=[NSFileManager defaultManager];


    if (!self.createMunkiRepoScript || [fm fileExistsAtPath:self.createMunkiRepoScript]==NO){
        NSLog(@"repo script not found");
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"repo create script not found or has incorrect permissions and owner."}];

        callback(err);
        return;
    }
    NSURL *fileURL=[NSURL fileURLWithPath:munkiRepoPath];

    NSTask *task=[NSTask launchedTaskWithLaunchPath:self.createMunkiRepoScript arguments:@[[fileURL description],munkiRepoPath]];

    [task waitUntilExit];

    if (task.terminationStatus!=0) {


        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Munki Repo could not be created: %i",task.terminationStatus]}];
        callback(err);

    }
    else callback(nil);
}

- (void)restartMicroMDMWithAuth:(nonnull NSData *)token callback:(nonnull void (^)(NSError * _Nullable))callback {

    int res;
    res=system("/bin/launchctl unload -w /Library/LaunchDaemons/com.twocanoes.mds.micromdm.plist");
    sleep(1);
    res=system("/bin/launchctl load -w /Library/LaunchDaemons/com.twocanoes.mds.micromdm.plist");
    sleep(1);
    if (res!=0) {
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"There was an error restarting the MDM Service."}];

        callback(err);
        return;
    }
    callback(nil);

}
- (void)setupMunkiReportWithAuth:(nonnull NSData *)token source:(nonnull NSString *)source destination:(nonnull NSString *)destination withCallback:(nonnull void (^)(NSError * _Nullable))callback {


    NSFileManager *fm=[NSFileManager defaultManager];
    __block NSError *err;
    __block BOOL hasError=NO;


    if ([fm fileExistsAtPath:destination]==NO){

        NSError *err;
        if([fm createDirectoryAtPath:destination withIntermediateDirectories:YES attributes:nil error:&err]==NO){

            callback(err);
            return;

//            [[NSAlert alertWithError:err] runModal];
//            return;
        }
    }

    NSArray *contents=[fm contentsOfDirectoryAtPath:destination error:&err];
    NSString *backupFolder;
    if (contents.count>0){

        NSString *folderName=[NSString stringWithFormat:@"backup-%@",[NSISO8601DateFormatter stringFromDate:[NSDate date] timeZone:[NSTimeZone localTimeZone] formatOptions:NSISO8601DateFormatWithInternetDateTime]];

        backupFolder=[destination stringByAppendingPathComponent:folderName];
        if([fm createDirectoryAtPath:backupFolder withIntermediateDirectories:NO attributes:nil error:&err]==NO){

            NSLog(@"%@",err.localizedDescription);
            callback(err);
        }
        [contents enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL * _Nonnull stop) {
            NSError *err;
            if ([filename hasPrefix:@"backup-"]) return;
            if([fm moveItemAtPath:[destination stringByAppendingPathComponent:filename] toPath:[backupFolder stringByAppendingPathComponent:filename] error:&err]==NO){

                NSLog(@"%@",err.localizedDescription);
                *stop=YES;
                hasError=YES;
                return;
            }

        }];
    }
    if(hasError==YES){
        callback(err);
        return;
    }
    NSArray *sourceContents=[fm contentsOfDirectoryAtPath:source error:&err];
    if (!sourceContents) {
        NSLog(@"%@",err.localizedDescription);
        callback(err);
        return;
    }
    [sourceContents enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([fm moveItemAtPath:[source stringByAppendingPathComponent:filename] toPath:[destination stringByAppendingPathComponent:filename] error:&err]==NO){
            NSLog(@"%@",err.localizedDescription);
            *stop=YES;
            hasError=YES;

        }

    }];
    if(hasError==YES){
        callback(err);
        return;
    }
    NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/sbin/chown" arguments:@[@"-R",@"_www:wheel",destination]];
    [task waitUntilExit];

    if (backupFolder){
        NSArray *filesToMove=@[@"config.php",@".env",@"composer.local.json",@"app/db/db.sqlite"];

        [filesToMove enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL * _Nonnull stop) {

            NSString *uniqueEnding=[[NSUUID UUID] UUIDString];
            if ([fm fileExistsAtPath:[destination stringByAppendingPathComponent:filename]]==YES){

                if([fm moveItemAtPath:[destination stringByAppendingPathComponent:filename] toPath:[destination stringByAppendingPathComponent:[filename stringByAppendingFormat:@"-%@",uniqueEnding]] error:&err]==NO){
                    NSLog(@"%@",err.localizedDescription);
                    *stop=YES;
                    hasError=YES;
                    return;

                }
            }
            if([fm fileExistsAtPath:[backupFolder stringByAppendingPathComponent:filename]]){

               if([fm moveItemAtPath:[backupFolder stringByAppendingPathComponent:filename] toPath:[destination stringByAppendingPathComponent:filename] error:&err]==NO){
                    NSLog(@"%@",err.localizedDescription);
                    *stop=YES;
                    hasError=YES;
                    return;
               }

            }
        }];
        if(hasError==YES){
            callback(err);
            return;
        }


        NSString *destinationUsersFolder=[destination stringByAppendingPathComponent:@"local/users"];
        NSString *backupUsersFolder=[backupFolder stringByAppendingPathComponent:@"local/users"];

        NSArray *backupUsersFolderContents=[fm contentsOfDirectoryAtPath:backupUsersFolder error:&err];
        if (backupUsersFolderContents) {
            [backupUsersFolderContents enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *backupCurrFilePath=[backupUsersFolder stringByAppendingPathComponent:filename];
                NSString *destinationCurrFilePath=[destinationUsersFolder stringByAppendingPathComponent:filename];
                if ([[[backupCurrFilePath pathExtension] lowercaseString] isEqualToString:@"yml"] &&
                    [fm fileExistsAtPath:destinationCurrFilePath]==NO){

                    if([fm copyItemAtPath:backupCurrFilePath toPath:destinationCurrFilePath error:&err]==NO){
                        hasError=YES;
                        *stop=YES;
                        return;
                    }
                }

            }];
            if(hasError==YES){
                      callback(err);
                      return;
                  }

        }

    }
    if ([fm fileExistsAtPath:[destination stringByAppendingPathComponent:@".env"]]==NO){

          if([@"AUTH_METHODS=LOCAL\n" writeToFile:[destination stringByAppendingPathComponent:@".env"] atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){
              callback(err);
              return;
          }
    }
    callback(nil);
}
- (void)restartWebserverWithAuth:(nonnull NSData *)token callback:(nonnull void (^)(BOOL))callback {

    int res=system("/usr/sbin/apachectl restart");
    if (res==0) callback(YES);
    callback(NO);
}
- (void)addMunkiReportWithAuth:(nonnull NSData *)token userFile:(nonnull NSString *)path contents:(nonnull NSString *)contents withCallback:(nonnull void (^)(NSError * err))callback {


    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:path]){
        NSLog(@"%@ already exists.",path);

        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The user already exists at %@.",path]}];
        callback(err);
        return;
    }

    NSError *err;
    if([contents writeToFile:path atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){

        NSLog(@"error writing file: %@",err.localizedDescription);
        callback(err);
        return;
    }
    callback(nil);

}
- (void)installInstallMacOSWithProductID:(nonnull NSString *)productID catalog:(nonnull NSString *)catalog workingPath:(nonnull NSString *)workingPath auth:(nonnull NSData *)token withCallback:(nonnull void (^)(BOOL))callback {



    if (self.installInstallMacOSScriptPath && [self isScriptValid:self.installInstallMacOSScriptPath]){

        dispatch_async(dispatch_get_main_queue(), ^{

            NSArray *arguments=@[self.installInstallMacOSScriptPath,@"--seedprogram",catalog,@"--productid",productID,@"--workdir",workingPath,@"--raw"];

            if ([catalog isEqualToString:@"Production"]){
                arguments=@[self.installInstallMacOSScriptPath,@"--productid",productID,@"--workdir",workingPath,@"--raw"];
            }


             self.task=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{
                 self.isCancelling=NO;

             } endBlock:^{

                 if (self.isCancelling==NO) {
                     if (self.task.terminationStatus!=0) {
                         NSLog(@"The OS Image was not completed successfully. Please check the log (/Library/Logs/mdshelper.log and try again.");

                         callback(NO);
                     }
                     else {
                         callback(YES);
                     }
                 }


                 
             } outputBlock:^(NSString *output) {
                 logLine((char *)output.UTF8String);
                 logLine("\n");
             } errorOutputBlock:^(NSString *errorOutput) {

                 logLine((char *)errorOutput.UTF8String);
             } arguments:arguments
            ];

             [self.task startProcess];
        });
    }
    else {
        NSLog(@"Script not valid");
    }
    
}
- (void)removeUsers:(nonnull NSArray *)users fromFolder:(nonnull NSString *)path withAuth:(nonnull NSData *)token withCallback:(nonnull void (^)(NSError * err))callback {


    BOOL isDir;
    __block NSError *err;
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:path isDirectory:&isDir] && isDir==YES){
        __block BOOL hasError=NO;
        [users enumerateObjectsUsingBlock:^(NSString *currUser, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *currUserPath=[[path stringByAppendingPathComponent:currUser] stringByAppendingPathExtension:@"yml"];
            if ([fm fileExistsAtPath:currUserPath]){
                if([fm removeItemAtPath:currUserPath error:&err]==NO){

                    hasError=YES;
                    *stop=YES;
                    return;
                }
            }

        }];
        if (hasError==YES){

            callback(err);
        }

    }
    else {
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"path at %@ does not exist",path]}];

        callback(err);
        return;
    }
    callback(nil);
}




@end
