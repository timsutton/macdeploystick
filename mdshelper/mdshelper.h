//
//  mdshelper.h
//  mdshelper
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "mdshelperProtocol.h"
#include <signal.h>
// This object implements the protocol which we have defined. It provides the actual behavior for the service. It is 'exported' by the service to make it available to the process hosting the service over an NSXPCConnection.
@interface mdshelper : NSObject <mdshelperProtocol>
+ (NSString *)machServiceName;
+ (NSString *)exposedProtocolName;
- (void)runXPCService;
@end
