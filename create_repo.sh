#!/bin/bash

repo_url="$1"
rep_path="$2"


if [ ! -d "$rep_path" ]; then
    mkdir "$rep_path"
fi

cd "$rep_path"

mkdir "${rep_path}/catalogs"
mkdir "${rep_path}/icons"
mkdir "${rep_path}/manifests"
mkdir "${rep_path}/pkgs"
mkdir "${rep_path}/pkgsinfo"

chgrp -R admin "${rep_path}"
chmod -R 775 "${rep_path}"

defaults write  com.googlecode.munki.munkiimport repo_url -string "${repo_url}"
defaults write  com.googlecode.munki.munkiimport default_catalog -string "MainCatalog"

if [ ! -e "${rep_path}/manifests/site_default" ]; then
    /usr/local/munki/manifestutil new-manifest site_default
fi

