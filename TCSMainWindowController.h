//
//  TCSMainWindowController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/30/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSMainWindowController : NSWindowController

@end

NS_ASSUME_NONNULL_END
