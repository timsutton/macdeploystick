#!/bin/bash
#Copyright 2018-2020 Twocanoes Software, Inc

source ./com_twocanoes_mds_workflow_script_config.sh

report(){
    local message=$1
    local status=$2

    echo $message:$status
    if [ ${network_available} ]; then
        /usr/bin/curl -X POST  --data-urlencode "message=${message}" --data-urlencode "serial=${serial}" --data-urlencode "status=${status}" "${logging_url_string}"
    fi

}
##
# Determine if the network is up by looking for any non-loopback
# internet network interfaces.
##
CheckForNetwork()
{

    local test

    test=$(ifconfig -a inet 2>/dev/null | sed -n -e '/127.0.0.1/d' -e '/0.0.0.0/d' -e '/inet/p' | wc -l)
    if [ "${test}" -gt 0 ]; then
        NETWORKUP="YES"
    else
        NETWORKUP="NO"
    fi
}


SCRIPTDIR=$(/bin/pwd)

serial=$(ioreg -l | awk '/IOPlatformSerialNumber/ { print $4;}'|tr -d '\"')

if [ -n "${workflow_should_configure_wifi}" ] && [ -n "${workflow_wifi_ssid}" ] && [ -n "${workflow_wifi_password}" ] ; then

    /usr/sbin/networksetup -setairportnetwork en0 "${workflow_wifi_ssid}" "${workflow_wifi_password}"
    /usr/sbin/networksetup -setairportnetwork en1 "${workflow_wifi_ssid}" "${workflow_wifi_password}"
    sleep 10
fi

if [ -n "${workflows_should_wait_for_network}" ] ; then

        for i in {1..36}; do

            CheckForNetwork
            if [ "${NETWORKUP}" == "YES" ]; then
                echo "Network is up"
                break
            else
                echo "Network not up. sleeping for 5 seconds."
                sleep 5
            fi

        done

fi

if [ -n "${logging_url_string}" ]; then
    if curl --connect-timeout 5 "${logging_url_string}" ; then
        network_available=1
    fi
fi
if [ -n "${workflow_should_create_user}" ] ; then

    /usr/sbin/createhomedir -c

    report "adding ssh keys" "script"

    if [ -d "$2/sshkeys" ]; then

        pushd "$2/sshkeys"
        for key_file_name in *; do
            echo "creating ssh key for ${key_file_name}"
            user_home="/Users/${key_file_name}"
            /bin/mkdir -p "${user_home}/.ssh"
            /bin/cp "${key_file_name}" "${user_home}"/.ssh/authorized_keys
            /bin/chmod 600 "${user_home}"/.ssh/authorized_keys
            /bin/chmod 700 "${user_home}"/.ssh
            /usr/sbin/chown -R "${key_file_name}:staff" "${user_home}"/.ssh
        done

        popd
        /bin/echo "allowing admin to reboot"
        /bin/echo "%admin ALL=(ALL) NOPASSWD:/sbin/reboot" >> /etc/sudoers.d/reboot

    fi


    if [ -n "${workflow_create_password_hint}" ]; then
        /usr/bin/dscl . -merge "${user_home}" hint "${workflow_create_password_hint}"
            /usr/bin/defaults write com.apple.loginwindow RetriesUntilHint -int 3
    fi
fi

if [ -n "${worksflows_should_enable_password_hints}" ]; then

    /usr/bin/defaults write com.apple.loginwindow RetriesUntilHint -int 3

fi
if [ -n "${workflows_should_trust_munki_client_certificate}" ]; then

    /usr/bin/security add-trusted-cert -r trustRoot -d -k "/Library/Keychains/System.keychain" -e hostnameMismatch "$2/munki_certificate.pem"

fi

if [ -n "${workflows_should_trust_server_certificate}" ]; then

    /usr/bin/security add-trusted-cert -r trustRoot -d -k "/Library/Keychains/System.keychain" -e hostnameMismatch "$2/server_certificate.pem"

fi

if [ -n "${workflows_munki_workflow_url}" ]; then

/usr/bin/defaults write /Library/Preferences/ManagedInstalls SoftwareRepoURL "${workflows_munki_workflow_url}"

fi
echo "Setting Variables from NVRAM if needed"
for num in 1 2 3 4 5 6 7 8 9; do
    VALUE=$(/usr/sbin/nvram "com.twocanoes.mds.var${num}"| sed $'s/com.twocanoes.mds.var.*\t//')
    if [ "${VALUE}" != "" ]; then
        declare -x "mds_var${num}"="${VALUE}"
    fi
done

echo "setting workflow name from NVRAM if needed"

WORKFLOWNAME_VALUE=$(/usr/sbin/nvram "com.twocanoes.mds.workflowname"| sed $'s/com.twocanoes.mds.workflowname\t//')
if [ "${WORKFLOWNAME_VALUE}" != "" ]; then
    declare -x "mds_workflowname"="${WORKFLOWNAME_VALUE}"
fi





if [ -n "${workflows_should_set_computer_name}" ]; then
    report "setting computer name" "configuration"

    NAME=$(/usr/sbin/nvram com.twocanoes.mds.ComputerName| sed $'s/com.twocanoes.mds.ComputerName\t//')

    RESOLVED_NAME=$(eval echo "$NAME")


    echo setting computer name to $NAME
    if [ -n "${RESOLVED_NAME}" ]; then
        /usr/sbin/scutil --set ComputerName "$RESOLVED_NAME"
        if [ "$?" -ne 0 ] ; then

            sleep 30
            /usr/sbin/scutil --set ComputerName "$RESOLVED_NAME"

        fi
        /usr/sbin/scutil --set HostName "$RESOLVED_NAME"
        /usr/sbin/scutil --set LocalHostName "$RESOLVED_NAME"
        /usr/bin/defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server.plist NetBIOSName "$RESOLVED_NAME"
    fi

    /usr/sbin/nvram -d com.twocanoes.mds.ComputerName

fi

if [ -d "$2/com.twocanoes.mds.scripts" ]; then


    cd "$2/com.twocanoes.mds.scripts"

    for current_script in ./* ; do
        if [ -f "${current_script}" ] && [ -x "${current_script}" ]; then
              report "running script ${current_script} $1 $2 $3" "script"
             "${current_script}" "$1" "$2" "$3"
        fi
    done
fi
if [ -n "${workflows_remove_nvram_vars}" ]; then
    echo "removing NVRAM variables if needed"
    for num in 1 2 3 4 5 6 7 8 9; do
        /usr/sbin/nvram -d "com.twocanoes.mds.var${num}"
    done
fi

if [ -d "$2/com.twocanoes.mds.profiles" ]; then
    cd "$2/com.twocanoes.mds.profiles"
    for i in ./*.mobileconfig; do
        report "installing profile ${i}" "profile"

        profiles install -type configuration -path "$i"
    done
fi
if [ -n "${workflows_should_enable_ard}" ]; then
    report "enabling ARD" "configuration"

   /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -allowAccessFor -allUsers -privs -all -clientopts -setmenuextra -menuextra yes
    /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -restart -agent

fi


if [ -n "${workflows_should_enable_ssh}" ]; then
    report "enabling SSH" "configuration"

    /bin/launchctl load -w /System/Library/LaunchDaemons/ssh.plist

fi


if [ -n "${workflows_should_enable_screen_sharing}" ]; then
    report "enabling screen sharing" "configuration"

    /bin/launchctl enable system/com.apple.screensharing
    /bin/launchctl bootstrap system /System/Library/LaunchDaemons/com.apple.screensharing.plist
fi

if [ -n "${workflows_should_skip_setup_assistant}" ]; then
    report "touching AppleSetupDone to skip setup assistant" "configuration"
    /usr/bin/touch /private/var/db/.AppleSetupDone

        if [ -n "${workflows_should_enable_location_services}" ]; then
            report "Enabling location services" "configuration"

            defaults write /var/db/locationd/Library/Preferences/ByHost/com.apple.locationd LocationServicesEnabled -int 1
    fi

fi



if [ -n "${workflows_should_skip_privacy_setup}" ]; then

    report "writing preferences to skip privacy setup" "configuration"
    # Determine OS version
    osvers=$(sw_vers -productVersion | awk -F. '{print $2}')
    sw_vers=$(sw_vers -productVersion)

    # Determine OS build number

    sw_build=$(sw_vers -buildVersion)

    # Checks first to see if the Mac is running 10.7.0 or higher.
    # If so, the script checks the system default user template
    # for the presence of the Library/Preferences directory. Once
    # found, the iCloud, Data & Privacy, Diagnostic and Siri pop-up
    # settings are set to be disabled.

    if [[ ${osvers} -ge 7 ]]; then

        user_template_dir="$3/System/Library/User Template"
        
        for USER_TEMPLATE in "${user_template_dir}"/*
        do
            echo ${USER_TEMPLATE}
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeeCloudSetup -bool TRUE
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant GestureMovieSeen none
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant LastSeenCloudProductVersion "${sw_vers}"
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant LastSeenBuddyBuildVersion "${sw_build}"
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeePrivacy -bool TRUE
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeeTrueTonePrivacy -bool TRUE
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeeTouchIDSetup -bool TRUE
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeeSiriSetup -bool TRUE
            /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeeScreenTime -bool TRUE
        done

        # Checks first to see if the Mac is running 10.7.0 or higher.
        # If so, the script checks the existing user folders in /Users
        # for the presence of the Library/Preferences directory.
        #
        # If the directory is not found, it is created and then the
        # iCloud, Data & Privacy, Diagnostic and Siri pop-up settings
        # are set to be disabled.

        for USER_HOME in "$3/Users"/*
        do
            USER_UID=`basename "${USER_HOME}"`
            if [ ! "${USER_UID}" = "Shared" ]; then
                if [ ! -d "${USER_HOME}"/Library/Preferences ]; then
                    /bin/mkdir -p "${USER_HOME}"/Library/Preferences
                    /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library
                    /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library/Preferences
                fi
                if [ -d "${USER_HOME}"/Library/Preferences ]; then
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeeCloudSetup -bool TRUE
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant GestureMovieSeen none
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant LastSeenCloudProductVersion "${sw_vers}"
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant LastSeenBuddyBuildVersion "${sw_build}"
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeePrivacy -bool TRUE
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeeTrueTonePrivacy -bool TRUE
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeeTouchIDSetup -bool TRUE
                    /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeeSiriSetup -bool TRUE
                    /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant.plist
                fi
            fi
        done
    fi
fi



if [ -n "${worksflows_should_run_software_update}" ]; then
    report "running software update -i -a"
    /usr/sbin/softwareupdate -i -a
fi


if [ -n "${worksflows_should_reboot}" ]; then
    report "script to install profiles and run scripts complete. Rebooting" "success"

    report "rebooting in 1 minute"
    /sbin/shutdown -r +1m
elif [ -n "${worksflows_should_shutdown}" ]; then
    report "script to install profiles and run scripts complete. Shutting down." "success"

    report "shutting down in 1 minute"
    /sbin/shutdown -h +1m

else
    report "script to install profiles and run scripts complete" "success"

fi


