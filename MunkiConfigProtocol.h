//
//  MunkiConfigProtocol.h
//  MDS
//
//  Created by Timothy Perfitt on 9/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#ifndef MunkiConfigProtocol_h
#define MunkiConfigProtocol_h
#import "TCSWorkflow.h"
@protocol MunkiConfigProtocol <NSObject>

-(TCSWorkflow *)workflow;

@end


#endif /* MunkiConfigProtocol_h */
