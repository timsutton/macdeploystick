#!/bin/sh

this_dir=$(dirname $0)
source ${this_dir}/../build/bitbucket_creds.sh 

prebeta_filename="${1}"

filename=$(basename "${prebeta_filename}")

echo "Uploading ${prebeta_filename}"
if [ -f "${prebeta_filename}" ]; then

	curl --progress-bar -X POST "https://${bitbucket_username}:${bitbucket_password}@api.bitbucket.org/2.0/repositories/twocanoes/macdeploystick/downloads" --form files=@"${prebeta_filename}" > /tmp/curl.log
	
	
fi





