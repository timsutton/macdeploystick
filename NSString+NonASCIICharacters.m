//
//  NSString+NonASCIICharacters.m
//  MDS
//
//  Created by Timothy Perfitt on 4/27/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "NSString+NonASCIICharacters.h"

@implementation NSString (NonASCIICharacters)
-(BOOL)hasNonASCIICharacters{


    return ([self cStringUsingEncoding:NSASCIIStringEncoding]==NULL);
    
}
@end
