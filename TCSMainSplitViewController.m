//
//  TCSMainSplitViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/7/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMainSplitViewController.h"
#import "ViewController.h"
#import "TCSServerMainViewController.h"
#import "TCSUtility.h"
#import "TCSLogWindowController.h"

@interface TCSMainSplitViewController () <TCSItemSelectionProtocol>
@property (weak) IBOutlet NSSplitViewItem *mainWindowSplitViewItem;
@property (strong) TCSLogWindowController *logWindowController;
@end

@implementation TCSMainSplitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//
    NSStoryboard *mainStoryboard = [NSStoryboard storyboardWithName:@"Main" bundle: nil];
    NSStoryboard *workflowEditorStoryboard = [NSStoryboard storyboardWithName:@"WorkflowEditor" bundle: nil];

    ViewController *workflowViewControlller=[workflowEditorStoryboard instantiateControllerWithIdentifier:@"workflowController"];
    TCSServerMainViewController *sidebarController=[mainStoryboard instantiateControllerWithIdentifier:@"sidebarStoryboardID"];

    sidebarController.delegate=self;
    NSSplitViewItem *mainItem=[NSSplitViewItem splitViewItemWithViewController:workflowViewControlller];
    NSSplitViewItem *sidebarItem=[NSSplitViewItem splitViewItemWithViewController:sidebarController];

    [self addSplitViewItem:sidebarItem];
    [self addSplitViewItem:mainItem];
//    [self toggleSidebar:self];

    [sidebarController setNextResponder:workflowViewControlller];



}

//- (BOOL)splitView:(NSSplitView *)splitView shouldCollapseSubview:(NSView *)subview forDoubleClickOnDividerAtIndex:(NSInteger)dividerIndex{
//    [super splitView:splitView shouldCollapseSubview:subview forDoubleClickOnDividerAtIndex:dividerIndex];
//    return NO;
//}
////- (CGFloat)splitView:(NSSplitView *)splitView constrainSplitPosition:(CGFloat)proposedPosition ofSubviewAt:(NSInteger)dividerIndex{
////
////    if (proposedPosition<200.0) return 200.0;
////    return proposedPosition;
////}
- (void)selectedViewControllerWithStoryboardName:(nonnull NSString *)sbName storyboardID:(nonnull NSString *)storyboardID {

    NSStoryboard *storyboard = [NSStoryboard storyboardWithName:sbName bundle: nil];
    NSViewController *viewController;
    if ([storyboardID isEqualToString:@"logStoryboardID"]){

        if (!self.logWindowController) {

            self.logWindowController=[storyboard instantiateControllerWithIdentifier:storyboardID];

        }
        viewController=(NSViewController *)self.logWindowController;
    }
    else {
            viewController=[storyboard instantiateControllerWithIdentifier:storyboardID];
    }


    NSSplitViewItem *splitViewItem=[NSSplitViewItem splitViewItemWithViewController:viewController];

    if ([self splitViewItems] && [self splitViewItems].count>0){
        [self removeSplitViewItem:[[self splitViewItems] lastObject]];

        [self addSplitViewItem:splitViewItem];
        TCSServerMainViewController *controller=(TCSServerMainViewController *)[[[self splitViewItems] firstObject] viewController];
        controller.nextResponder=viewController;


//        [[self splitViewItems] objectAtIndex:0].nextResponder=self;
    }

    
}

- (BOOL)validateMenuItem:(NSMenuItem *)menuItem{
    NSViewController *vc=[[[self splitViewItems] lastObject] viewController];

    if ([vc respondsToSelector:@selector(showLogButtonPressed:)]){
        return YES;

    }
    else if  ([vc respondsToSelector:@selector(newWorkflow:)]){
        return YES;
    }
    return NO;
}
//-(IBAction)newWorkflow:(id)sender{
//        NSViewController *vc=[[[self splitViewItems] lastObject] viewController];
//
//        if ([vc respondsToSelector:@selector(newWorkflow:)]){
//
//            [vc performSelector:@selector(newWorkflow:) withObject:sender afterDelay:0];
//    //        [vc showLogButtonPressed:sender];
//
//        }
//}

-(IBAction)showLogButtonPressed:(id)sender{

    NSViewController *vc=[[[self splitViewItems] lastObject] viewController];

    if ([vc respondsToSelector:@selector(showLogButtonPressed:)]){

        [vc performSelector:@selector(showLogButtonPressed:) withObject:sender afterDelay:0];
//        [vc showLogButtonPressed:sender];

    }
}
- (IBAction)openMunkiAdmin:(id)sender {

    [TCSUtility openMunkiAdmin:self];
}



@end
