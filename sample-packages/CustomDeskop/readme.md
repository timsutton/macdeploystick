# Custom Initial Desktop Background

## Overview
This package will set a custom desktop picture in the user template and for all existing users (that have home directories in /Users). It works by copying a new desktoppicture.db to Library/Application Support/Dock/ in each user's home folder and the English.lproj user directory in the user template. The custom desktop picture is located in /Library/Desktop Pictures and is called MDSCustomDesktop.jpg. If one does not already exist, a sample one is created.

## How to use:
1. Install the package
1. Copy a desktop picture to /Library/Desktop Pictures/MDSCustomDesktop.jpg

All current users and new users will have the new desktop by default. They will be able to change it if they want.

**Note that the current user needs to log out to see the new desktop picture.**

