#!/bin/bash

# This script creates a user named mdsuser with a password from the variable mds_var1. 
# If used in MDS, set up the workflow variable mds_var1 to prompt for a user password. 
# Add this script to the specified script folder and add the Resources folder to the scripts 
# folder at the same level as the script.

# This script uses the create_user.py created by Greg Neagle and is licensed under the 
# included Apache license. Many thanks to Greg for this and all his other projects. 
# see https://github.com/munki

# Note: the create user will be an admin.

#Edit these:
# username is the username of the user.
username="mdsuser"
# passsword will be populated with the contents of mds_var1. Change to a different variable
# if you want to.
password="${mds_var1}"
#The user will have a UID of 602. If you want a different UID, change it to the value you want.
uid=602

#DO NOT EDIT BELOW
target="$3"
identifier=`uuidgen`
version=1

if [ -z "${target}" ]; then
	echo you must specify 3 args, of which the 3rd one is the target volume. this is the same order for a package installer so the script can be called easily from mds.
	exit -1
fi

script_dir=${0%/*}



if [ -z "${mds_var1}" ]; then
	echo "no password specified in mds_var1. No user will be created. that is all"
	exit 0
fi



path="${script_dir}/user-${username}.pkg"

"${script_dir}"/Resources/createuserpkg -n "${username}" -a -p "${password}" -u "${uid}" -V "${version}" -i "${identifier}"  "${path}"

installer -target "${target}" -pkg "${path}"