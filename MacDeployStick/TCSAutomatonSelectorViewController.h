//
//  TCSAutomatonSelectorViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 5/21/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMainWindowRightViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSAutomatonSelectorViewController : TCSMainWindowRightViewController
- (IBAction)createAutomatonButtonPressed:(id)sender;
@end

NS_ASSUME_NONNULL_END
