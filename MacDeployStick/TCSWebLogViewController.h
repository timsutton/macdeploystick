//
//  TCSWebLogViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/13/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMainWindowRightViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSWebLogViewController : TCSMainWindowRightViewController <NSWindowDelegate>
@property (strong) IBOutlet NSArrayController *arrayController;
@property (strong) NSMutableArray *logArray;
@end

NS_ASSUME_NONNULL_END
