//
//  TCSMDMConfigureViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 7/29/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSMDMConfigureViewController : NSViewController 

@end

NS_ASSUME_NONNULL_END
