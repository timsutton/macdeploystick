//
//  TCSMDMWebController.m
//  MDS
//
//  Created by Timothy Perfitt on 7/30/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSMDMWebController.h"
#import "TCWebConnection.h"
#import <AppKit/AppKit.h>
#import "TCSDepProfile.h"
#import "Criollo/Criollo.h"
#import "DeviceMO+CoreDataClass.h"
#import "MDMDataController.h"
#import "NSData+Base64.h"
#import "TCSUtility.h"
@interface TCSMDMWebController()
@property (strong) NSMutableArray *profilesToAdd;
@property (strong) CRHTTPServer *server;
@property (assign) BOOL isAvailable;
@property (strong) TCWebConnection *wc;
@property (strong) NSMutableArray *addedProfileIDs;
@end
@implementation TCSMDMWebController
+ (id)sharedController {
    static TCSMDMWebController *sharedWebController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedWebController = [[self alloc] init];

    });


    return sharedWebController;
}
-(void)sendPayload:(NSDictionary *)payload toEndpoint:(NSString *)  endpoint httpAction:(NSString *)action onCompletion:(TCWebCompletionBlock)completionBlock onError:(TCWebErrorBlock)errorBlock{


    NSURL *baseURL=[NSURL URLWithString:[NSString stringWithFormat:@"https://%@:%@",[USERDEFAULTS objectForKey:SERVERHOSTNAME],[USERDEFAULTS objectForKey:MDMSERVERPORT]]];

    self.wc=[[TCWebConnection alloc] initWithBaseURL:baseURL];

    [self.wc sendRequestToPath:endpoint type:action payload:payload basicAuthUsername:@"micromdm" basicAuthPassword:self.authAPIKey onCompletion:^(NSInteger responseCode, NSData *responseData) {

        completionBlock(responseCode,responseData);
    } onError:^(NSError *error) {
        errorBlock(error);

    }];
}
-(void)dealloc{

}

-(void)updateIsAvailable{
    //    https://mdscentral.local:8443
    dispatch_async(dispatch_get_main_queue(), ^{

    [self sendPayload:@{} toEndpoint:@"/" httpAction:@"GET" onCompletion:^(NSInteger responseCode, NSData *responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{

            if (responseCode<300){

                self.isAvailable=YES;
            }
            else {
                self.isAvailable=NO;
            }
        });

    } onError:^(NSError *error) {
        NSLog(@"%@",error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(), ^{

            self.isAvailable=NO;
        });
    }];
});

}
-(void)createUsers:(NSArray *)inUsers{

    [inUsers enumerateObjectsUsingBlock:^(TCSWorkflowUser *currUser, NSUInteger idx, BOOL * _Nonnull stop) {

        NSString *pwHash=[[TCSUtility calculateShadowHashData:currUser.password] base64EncodedString];


        if (!currUser.userUUID) return;
        NSDictionary *user=@{@"user":@{@"uuid":currUser.userUUID, @"user_shortname":currUser.shortName,@"user_longname":currUser.fullName,
                                       @"password_hash":pwHash, @"hidden":currUser.isHidden==NO?@(NO):@(YES)}};
        [self sendPayload:[NSDictionary dictionaryWithDictionary:user] toEndpoint:@"/v1/users" httpAction:@"PUT" onCompletion:^(NSInteger responseCode, NSData *responseData) {

        } onError:^(NSError *error) {
            NSLog(@"%@",error.localizedDescription);
        }];

    }];

}

-(void)updateProfiles:(NSArray *)inPathArray onCompletion:(void (^)(NSArray *profiles))completion{

    self.profilesToAdd=[NSMutableArray array];

    [inPathArray enumerateObjectsUsingBlock:^(NSString *currPath, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([[[currPath.lastPathComponent pathExtension] lowercaseString] isEqualToString:@"mobileconfig"]==YES){
            NSDictionary *profileDictionary=[NSDictionary dictionaryWithContentsOfFile:currPath];
            if (profileDictionary && profileDictionary[@"PayloadIdentifier"]){
                [self.profilesToAdd addObject:
                 @{
                     @"id":profileDictionary[@"PayloadIdentifier"],
                     @"profilePath":currPath
                 }];

            }

        }
    }];

    self.addedProfileIDs=[NSMutableArray array];
    [self addNextProfileWithCompletionBlock:completion];

}

-(void)addNextProfileWithCompletionBlock:(void (^)(NSArray *profiles))completion{

    if (self.profilesToAdd.count==0){
        completion(self.addedProfileIDs);
    }
    else {
        TCSMDMWebController *newSelf=self;
        [self addProfileFromPath:[[self.profilesToAdd firstObject] objectForKey:@"profilePath"] identifier:[[self.profilesToAdd firstObject] objectForKey:@"id"] onCompletion:^(BOOL success) {

            if (success==YES){
                [newSelf.addedProfileIDs addObject:[[newSelf.profilesToAdd firstObject] objectForKey:@"id"]];
            }
            [newSelf.profilesToAdd removeObjectAtIndex:0];
            [newSelf addNextProfileWithCompletionBlock:completion];

        }];

    }


}

-(void)updateDEPProfile:(NSDictionary *)inProfileDict onCompletion:(void (^)(BOOL success))completion{

    
    NSMutableDictionary *mutableProfileDict=[inProfileDict mutableCopy];

    self.devices=[[MDMDataController dataController] allDevices];
    if(self.devices && self.devices.count>0){
        NSMutableArray *deviceArray=[NSMutableArray array];
        [self.devices enumerateObjectsUsingBlock:^(DeviceMO *currDevice, NSUInteger idx, BOOL * _Nonnull stop) {

            if (currDevice.serial){
                [deviceArray addObject:currDevice.serial];
            }
        }];
        if (deviceArray.count>0){
            [mutableProfileDict setObject:deviceArray forKey:@"devices"];
        }
        [self sendPayload:[NSDictionary dictionaryWithDictionary:mutableProfileDict] toEndpoint:@"/v1/dep/profiles" httpAction:@"PUT" onCompletion:^(NSInteger responseCode, NSData *responseData) {


            completion(YES);

        } onError:^(NSError *error) {
            NSLog(@"%@",error.localizedDescription);
            completion(NO);
        }];


    }
    else {
        completion(YES);
    }
    

}

-(void)deleteBlueprintNamed:(NSString *)inName onCompletion:(void (^)(BOOL success))completion{



    [self sendPayload:@{@"names":@[inName]} toEndpoint:@"/v1/blueprints" httpAction:@"DELETE" onCompletion:^(NSInteger responseCode, NSData *responseData) {
        completion(YES);
    } onError:^(NSError *error) {
        completion(NO);
    }];

}
-(void)deleteProfileWithID:(NSString *)inProfileID onCompletion:(void (^)(BOOL success))completion{

    [self sendPayload:@{@"ids":@[inProfileID]} toEndpoint:@"/v1/profiles" httpAction:@"DELETE" onCompletion:^(NSInteger responseCode, NSData *responseData) {
        completion(YES);
    } onError:^(NSError *error) {
        completion(NO);
    }];

}
-(void)deleteDevicesWithIDs:(NSArray *)inDeviceUDIDArray onCompletion:(void (^)(BOOL success))completion{
    //    {"Opts":{"udids":["564DD86C-0A2B-A442-D80D-F5D158BA57A"],"serials":null}}

    [self sendPayload:@{@"Opts":@{@"udids":inDeviceUDIDArray}} toEndpoint:@"/v1/devices" httpAction:@"DELETE" onCompletion:^(NSInteger responseCode, NSData *responseData) {
        completion(YES);
    } onError:^(NSError *error) {
        completion(NO);
    }];

}
-(void)deleteProfileWithIDs:(NSArray *)inProfileIDArray onCompletion:(void (^)(BOOL success))completion{

    [self sendPayload:@{@"ids":inProfileIDArray} toEndpoint:@"/v1/profiles" httpAction:@"DELETE" onCompletion:^(NSInteger responseCode, NSData *responseData) {
        completion(YES);
    } onError:^(NSError *error) {
        completion(NO);
    }];

}
-(void)blockDevices:(NSArray *)inUDIDArray onCompletion:(void (^)(BOOL success))completion{

    __block BOOL didSucceed=YES;
    [inUDIDArray enumerateObjectsUsingBlock:^(DeviceMO *currDevice, NSUInteger idx, BOOL * _Nonnull stop) {
        [self sendPayload:@{} toEndpoint:[NSString stringWithFormat:@"/v1/devices/%@/block",currDevice.udid] httpAction:@"POST" onCompletion:^(NSInteger responseCode, NSData *responseData) {

        } onError:^(NSError *error) {
            didSucceed=NO;
        }];
    }];
    completion(didSucceed);


}
-(void)installApps:(NSArray *)inAppsArray onDevices:(NSArray *)inDevices onCompletion:(void (^)(BOOL success))completion{


    [inDevices enumerateObjectsUsingBlock:^(DeviceMO *currDevice, NSUInteger idx, BOOL * _Nonnull stop) {
        [inAppsArray enumerateObjectsUsingBlock:^(NSString *currApp, NSUInteger idx, BOOL * _Nonnull stop) {

            [self sendMDMCommand:@"InstallEnterpriseApplication" identifier:nil deviceUDID:currDevice.udid pin:nil filePayload:nil app:currApp];
        }];
    }];


    completion(YES);

}
-(void)eraseDevices:(NSArray *)inDeviceArray withPIN:(NSString *)pin onCompletion:(void (^)(BOOL success))completion{

    [inDeviceArray enumerateObjectsUsingBlock:^(DeviceMO *currDevice, NSUInteger idx, BOOL * _Nonnull stop) {

        [self sendMDMCommand:@"EraseDevice" identifier:nil deviceUDID:currDevice.udid pin:pin filePayload:nil app:nil];
    }];


    completion(YES);

}
-(void)unblockDevices:(NSArray *)inDeviceArray onCompletion:(void (^)(BOOL success))completion{

    __block BOOL didSucceed=YES;
    [inDeviceArray enumerateObjectsUsingBlock:^(DeviceMO *currDevice, NSUInteger idx, BOOL * _Nonnull stop) {
        [self sendPayload:@{} toEndpoint:[NSString stringWithFormat:@"/v1/devices/%@/unblock",currDevice.udid] httpAction:@"POST" onCompletion:^(NSInteger responseCode, NSData *responseData) {

        } onError:^(NSError *error) {
            didSucceed=NO;
        }];
    }];
    completion(didSucceed);


}

-(void)addProfileFromPath:(NSString *)inPath identifier:(NSString *)identifier onCompletion:(void (^)(BOOL success))completion{

    NSLog(@"profile identifier : %@",identifier);
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:inPath]==NO) {
        completion(NO);

    }
    NSData  *profileData=[NSData dataWithContentsOfFile:inPath];

    dispatch_async(dispatch_get_main_queue(), ^{

        [self sendPayload:@{@"profile":@{@"Identifier":identifier,@"Mobileconfig":[profileData base64EncodedString]}} toEndpoint:@"/v1/profiles" httpAction:@"PUT" onCompletion:^(NSInteger responseCode, NSData *responseData) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];

            NSLog(@"%@",responseDict);
            if (responseDict && responseDict[@"error"]) {



                NSLog(@"ERROR : %@",responseDict[@"error"]);
                completion(NO);
            }
            else {
                completion(YES);
            }
        } onError:^(NSError *error) {
            completion(NO);
        }];
    });

}
-(void)updateBlueprint:(NSDictionary *)inBlueprint onCompletion:(void (^)(BOOL success))completion{

    
    NSMutableDictionary *mutableProfileDict=[inBlueprint mutableCopy];

    self.devices=[[MDMDataController dataController] allDevices];
    NSMutableArray *deviceArray=[NSMutableArray array];
    [self.devices enumerateObjectsUsingBlock:^(DeviceMO *currDevice, NSUInteger idx, BOOL * _Nonnull stop) {

        if (currDevice.serial){
            [deviceArray addObject:currDevice.serial];
        }
    }];
    __block NSDictionary *payload=[NSDictionary dictionaryWithDictionary:mutableProfileDict];

    [self deleteBlueprintNamed:@"MDS Blueprint" onCompletion:^(BOOL success) {

        if (success==YES){
            [self sendPayload:payload toEndpoint:@"/v1/blueprints" httpAction:@"PUT" onCompletion:^(NSInteger responseCode, NSData *responseData) {
                NSError *err;

                NSDictionary *resp=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:&err];

                if(!resp){
                    NSLog(@"%@",err.localizedDescription);
                }
                else {
                    NSString *profileUUID=payload[@"blueprint"][@"uuid"];
                    NSLog(@"profileuuid:%@",profileUUID);
                    if (profileUUID) {
                        [self applyToAutoAssigner:profileUUID completion:^(BOOL success) {
                            completion(success);
                            return;
                        }];
                    }
                    else NSLog(@"could not get dep profile uuid");
                }

            } onError:^(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
                completion(NO);
            }];
        }
        else {
            completion(NO);
            return;

        }
    }];
    return;


}



-(void)applyToAutoAssigner:(NSString *)inProfileUUID completion:(void(^)(BOOL success))completion{
    [self sendPayload:@{@"profile_uuid":inProfileUUID,@"filter":@"*"} toEndpoint:@"/v1/dep/autoassigners" httpAction:@"POST" onCompletion:^(NSInteger responseCode, NSData *responseData) {

        completion(YES);
    } onError:^(NSError *error) {
        NSLog(@"%@",error.localizedDescription);
        completion(NO);
    }];
}

-(void)stopWebserver{


    [self.server stopListening];
    self.server=nil;
}
-(void)startWebServerWithCertificatePath:(NSString *)certPath privateKeyPath:(NSString *)keyPath{


    if (self.server ) [self.server stopListening];
    if (!self.server) {
        self.server = [[CRHTTPServer alloc] init];
    }
    self.server.isSecure = NO;

    // Credentials: DER-encoded certificate and public key
    //    self.server.certificatePath = certPath;
    //    self.server.certificateKeyPath = keyPath;
    //

    //        }
    [self.server add:@"/" block:^(CRRequest * _Nonnull request, CRResponse * _Nonnull response, CRRouteCompletionBlock  _Nonnull completionHandler) {

        NSDictionary *requestDict = request.body;

        if (requestDict && requestDict[@"acknowledge_event"]){
            NSDictionary *event=requestDict[@"acknowledge_event"];

            NSString *commandUUID=event[@"command_uuid"];
            NSString *payload=event[@"raw_payload"];
            NSString *status=event[@"status"];

            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
            dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
            NSDate *dateCreated = [dateFormatter dateFromString:event[@"created_at"]];
            NSString *udid=event[@"udid"];

            if (udid && status ){
                dispatch_async(dispatch_get_main_queue(), ^{

                    [[MDMDataController dataController] addResponseForDevice:udid status:status commandUUID:commandUUID dateCreated:dateCreated payload:payload];
                });
            }
        }
        else {


        }
        [response sendString:@""];

    }];

    NSError *err;
    [self.server startListening:&err portNumber:9999 interface:@"127.0.0.1"];

    if (err) {
        NSLog(@"error starting listener:%@",err.localizedDescription);
        [[NSAlert alertWithError:err] runModal];

    }
}


-(void)checkAPNSCertLoadedStatus:(void (^)(BOOL isLoaded))block{

    dispatch_async(dispatch_get_main_queue(), ^{

        [self sendPayload:nil toEndpoint:@"/v1/config/certificate" httpAction:@"GET" onCompletion:^(NSInteger responseCode, NSData *responseData) {


            if (responseCode>299) block(NO);

            else block(YES);
        } onError:^(NSError *error) {

            NSLog(@"%@",error.localizedDescription);
        }];
    });

}
-(void)checkPushTokenLoadedStatus:(void (^)(BOOL isLoaded))block{
    dispatch_async(dispatch_get_main_queue(), ^{

        [self sendPayload:nil toEndpoint:@"/v1/dep-tokens" httpAction:@"GET" onCompletion:^(NSInteger responseCode, NSData *responseData) {

            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];


            if (responseDict && responseDict[@"dep_tokens"]!=[NSNull null] && [responseDict[@"dep_tokens"] count]>0) {

                block(YES);
            }
            else block(NO);


        } onError:^(NSError *error) {
            block(NO);
        }];

    });

}

-(void)deleteProfileFromDeviceWithUDID:(NSString *)udid profileIdentiifer:(NSString *)identifier{
    [self sendMDMCommand:@"RemoveProfile" identifier:identifier deviceUDID:udid pin:nil filePayload:nil app:nil];

    if (udid) {
        [[TCSMDMWebController sharedController] refreshDataFromDeviceWithUDID:udid];
    }


}
-(void)deleteProfilesFromDeviceWithUDID:(NSString *)udid profileIdentiifers:(NSArray *)identifiers{
    [identifiers enumerateObjectsUsingBlock:^(NSString *currIdentifier, NSUInteger idx, BOOL * _Nonnull stop) {
        [self sendMDMCommand:@"RemoveProfile" identifier:currIdentifier deviceUDID:udid pin:nil filePayload:nil app:nil];

    }];

    if (udid) {
        [[TCSMDMWebController sharedController] refreshDataFromDeviceWithUDID:udid];
    }


}

-(void)refreshDataFromDeviceWithUDID:(NSString *)udid{

    [@[@"DeviceInformation",@"ProfileList",@"CertificateList",@"SecurityInfo"] enumerateObjectsUsingBlock:^(NSString * command, NSUInteger idx, BOOL * _Nonnull stop) {
        [self sendMDMCommand:command identifier:nil deviceUDID:udid pin:nil filePayload:nil app:nil];
    }];



}
-(void)sendMDMCommand:(NSString *)command identifier:(nullable NSString *)identifier deviceUDID:(nullable NSString *)udid pin:(nullable NSString *)pin filePayload:(nullable NSString *)filePayload app:(nullable NSString *)app{

    TCSMDMWebController *webController=[TCSMDMWebController sharedController];
    NSMutableDictionary *payload=[NSMutableDictionary dictionary];
    [payload addEntriesFromDictionary:@{@"request_type":command,@"udid":udid}];

    if (filePayload) {
        [payload addEntriesFromDictionary:@{@"payload":filePayload}];

    }
    if (identifier) {
        [payload addEntriesFromDictionary:@{@"identifier":identifier}];

    }
    if (pin) {
        [payload addEntriesFromDictionary:@{@"pin":pin}];

    }
    if (app) {

        NSString *serverURL=[NSString stringWithFormat:@"https://%@:%@",[USERDEFAULTS objectForKey:SERVERHOSTNAME],[USERDEFAULTS objectForKey:MDMSERVERPORT]];

        NSString *urlComponentsManifest=[[@"repo" stringByAppendingPathComponent:[[app stringByDeletingPathExtension] stringByAppendingPathExtension:@"plist"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];

        NSString *manifestURLString=[NSString stringWithFormat:@"%@/%@",serverURL,urlComponentsManifest];
        [payload addEntriesFromDictionary:@{@"manifest_url":manifestURLString}];


    }
    [webController sendPayload:payload toEndpoint:@"v1/commands" httpAction:@"POST" onCompletion:^(NSInteger responseCode, NSData *responseData) {

        if (responseData) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];

            NSDictionary *payload;
            if (responseDict && (payload=responseDict[@"payload"])){

                if (payload[@"command"] && payload[@"command_uuid"] && payload[@"command"][@"request_type"]){

                    [[MDMDataController dataController] addRequestForDevice:udid requestType:payload[@"command"][@"request_type"] commandUUID:payload[@"command_uuid"]];
                }



            }

        }

    } onError:^(NSError *error) {

        [[NSAlert alertWithError:error] runModal];

    }];
}

@end
