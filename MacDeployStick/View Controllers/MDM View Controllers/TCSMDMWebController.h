//
//  TCSMDMWebController.h
//  MDS
//
//  Created by Timothy Perfitt on 7/30/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCWebConnection.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSMDMWebController : NSObject
@property (strong) NSString *baseURL;
@property (strong) NSString *authAPIKey;
@property (strong) NSArray *devices;
+ (id)sharedController ;
-(void)startWebServerWithCertificatePath:(NSString *)certPath privateKeyPath:(NSString *)keyPath;
-(void)stopWebserver;
-(void)sendPayload:(nullable NSDictionary *)payload toEndpoint:(NSString *)endpoint httpAction:(NSString *)action onCompletion:(TCWebCompletionBlock)completionBlock            onError:(TCWebErrorBlock)errorBlock;
-(void)updateDEPProfile:(NSDictionary *)inProfileDict onCompletion:(void (^)(BOOL success))completion;
-(void)createUsers:(NSArray *)inUsers;
-(void)applyToAutoAssigner:(NSString *)inProfileUUID completion:(void(^)(BOOL success))completion;
-(void)updateBlueprint:(NSDictionary *)inBlueprint onCompletion:(void (^)(BOOL success))completion;
-(void)addProfileFromPath:(NSString *)inPath identifier:(NSString *)identifier onCompletion:(void (^)(BOOL success))completion;
-(void)updateProfiles:(NSArray *)inPathArray onCompletion:(void (^)(NSArray *profiles))completion;
-(void)checkAPNSCertLoadedStatus:(void (^)(BOOL isLoaded))block;
-(void)checkPushTokenLoadedStatus:(void (^)(BOOL isLoaded))block;
-(void)refreshDataFromDeviceWithUDID:(NSString *)udid;
-(void)sendMDMCommand:(NSString *)command identifier:(nullable NSString *)identifier deviceUDID:(nullable NSString *)udid pin:(nullable NSString *)pin filePayload:(nullable NSString *)filePayload app:(nullable NSString *)app;
-(void)deleteProfileFromDeviceWithUDID:(NSString *)udid profileIdentiifer:(NSString *)identifier;
-(void)deleteProfileWithIDs:(NSArray *)inProfileIDArray onCompletion:(void (^)(BOOL success))completion;
-(void)deleteProfilesFromDeviceWithUDID:(NSString *)udid profileIdentiifers:(NSArray *)identifiers;
-(void)deleteDevicesWithIDs:(NSArray *)inDeviceUDIDArray onCompletion:(void (^)(BOOL success))completion;
-(void)blockDevices:(NSArray *)inUDIDArray onCompletion:(void (^)(BOOL success))completion;
-(void)unblockDevices:(NSArray *)inUDIDArray onCompletion:(void (^)(BOOL success))completion;
-(void)eraseDevices:(NSArray *)inDeviceArray withPIN:(NSString *)pin onCompletion:(void (^)(BOOL success))completion;
-(void)installApps:(NSArray *)inAppsArray onDevices:(NSArray *)inDevices onCompletion:(void (^)(BOOL success))completion;
-(void)updateIsAvailable;
@end

NS_ASSUME_NONNULL_END
