//
//  TCSMainContentViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/7/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSMainContentViewController : NSViewController

@end

NS_ASSUME_NONNULL_END
