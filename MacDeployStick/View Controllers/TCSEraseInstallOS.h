//
//  TCSEraseInstallOS.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMainWindowRightViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSEraseInstallOS : TCSMainWindowRightViewController

@end

NS_ASSUME_NONNULL_END
