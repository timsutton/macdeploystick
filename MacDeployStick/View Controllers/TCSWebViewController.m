//
//  TCSMunkiViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/9/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//


#import "TCSWebViewController.h"
#import <GCDWebServers/GCDWebServers.h>
#import "TCSConstants.h"
#import "TCSWebserverSetting.h"
#import "TCSWebServerSettingViewController.h"
#import "MDSPrivHelperToolController.h"
#import "TCSUtility.h"
#import "TCSWebServiceController.h"
#import <SecurityInterface/SFAuthorizationView.h>
@interface TCSWebViewController () <TCSWebServiceConfigurationDelegateProtocol>
@property (strong) GCDWebServer* webServer;
@property (strong) NSString *webserverURL;
@property (strong) NSMutableArray *sharedWebsites;
@property (strong) IBOutlet NSArrayController *webSettingsArrayController;
@property (weak) IBOutlet NSTableView *webserverSettingsTable;
@property (weak) IBOutlet id openLinkTarget;
@property (assign) NSInteger editingWorkflowAtIndex;
@property (strong) NSString *hostname;
@end

@implementation TCSWebViewController
@synthesize serviceButtonState;

@synthesize badgeCount;

@synthesize badgeType;

@synthesize errorMessage;

@synthesize hasError;

@synthesize isInstalled;

@synthesize isRunning;

@synthesize isUpdateAvailable;

@synthesize logFilePath;

@synthesize statusMode;
@synthesize  serviceStatus;
@synthesize statusType;
- (void)awakeFromNib{
    [super awakeFromNib];
    [self updateServiceStatus:self];
    NSString *hostname=[USERDEFAULTS objectForKey:SERVERHOSTNAME];

    self.hostname=hostname;

}
-(void)updateServiceStatus:(id)sender{
       self.isRunning=[[TCSWebServiceController sharedController] isRunning];



    if ([self isRunning]==YES){
        self.serviceButtonState=NSControlStateValueOn;
    }
    else {
        self.serviceButtonState=NSControlStateValueOff;
    }



}
-(IBAction)showLogButtonPressed:(id)sender{
    [[NSWorkspace sharedWorkspace] openFile:@"/var/log/apache2/access_log" withApplication:@"Console"];
    [[NSWorkspace sharedWorkspace] openFile:@"/var/log/apache2/error_log" withApplication:@"Console"];

}
- (IBAction)openWebsiteURLButtonPressed:(id)sender {
    NSInteger row = [self.webserverSettingsTable rowForView:sender];

    TCSWebserverSetting *settings=[self.sharedWebsites objectAtIndex:row];
    NSString *prefix=settings.useTLS?@"https":@"http";
    NSString *hostname=[USERDEFAULTS objectForKey:SERVERHOSTNAME];
    NSInteger port=settings.port;

    NSString *urlToOpen=[NSString stringWithFormat:@"%@://%@:%li",prefix,hostname,port];
    NSURL *url=[NSURL URLWithString:urlToOpen];
    if([[NSWorkspace sharedWorkspace]  openURL:url]==NO){


        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Open Website Error";
        alert.informativeText=[NSString stringWithFormat:@"The URL %@ could not be opened. Please verify the URL is correct and try again.",urlToOpen];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

    }

}
- (IBAction)addWebserverSetting:(id)sender {
    [self performSegueWithIdentifier:@"addWebserverSettingSegue" sender:self];
}
- (IBAction)removeWebserverSetting:(id)sender {

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Remove Website Configuration";
    alert.informativeText=@"Are you sure you want to remove the selected website configuration?";

    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    [self.webSettingsArrayController removeObjects:self.webSettingsArrayController.selectedObjects];
    [[TCSWebServiceController sharedController] saveSettings:[self.webSettingsArrayController arrangedObjects] callback:^(BOOL hadSuccess) {
        if (hadSuccess==NO) [self showApacheError:@"There was an error updating settings and restarting the web service. Please check the webserver log"];

    }];

}
- (IBAction)editWebserverSetting:(id)sender {
    [self performSegueWithIdentifier:@"editWebserverSettingSegue" sender:self];

}

- (IBAction)controlChanged:(id)sender {
    [[TCSWebServiceController sharedController] saveSettings:[self.webSettingsArrayController arrangedObjects] callback:^(BOOL hadSuccess) {
        if (hadSuccess==NO) [self showApacheError:@"There was an error updating settings and restarting the web service. Please check the webserver log"];

    }];
}
-(void)loadSettings:(id)sender{
        NSArray *webserviceSettings=[[TCSWebServiceController sharedController] webserviceSettings];
        if (webserviceSettings && webserviceSettings.count>0) [self.webSettingsArrayController addObjects:webserviceSettings];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sharedWebsites=[NSMutableArray array];
    [self.webserverSettingsTable setDoubleAction:@selector(tableDoubleClick:)];
    [self loadSettings:self];


}

- (IBAction)openBrowserForWebserverURL:(id)sender {

    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:self.webserverURL]];
}

-(void)stopService:(id)sender{
    if (self.isAuthorized==NO) return;
    [[MDSPrivHelperToolController sharedHelper] stopWebserverWithCallback:^(BOOL success) {

        if (success==NO) [self showApacheError:@"There was an error stopping the web service. Please check the webserver log."];
        [self updateServiceStatus:self];

        [self updateSidebar:self];

    }];

}
-(void)startService:(id)sender{
    if (self.isAuthorized==NO) return;
    [self performSelector:@selector(updateSidebar:) withObject:self afterDelay:1];

    [self performSelector:@selector(updateServiceStatus:) withObject:self afterDelay:1];

    [[TCSWebServiceController sharedController] updateApacheSettings:[self.webSettingsArrayController arrangedObjects] restartIfRunningWithCallback:^(BOOL success) {


        [[MDSPrivHelperToolController sharedHelper] startWebserverWithCallback:^(BOOL success) {
            if (success==NO) [self showApacheError:@"There was an error starting the web service. Please check the webserver log."];


        }];
    }];


}

- (void)restartService:(nonnull id)sender {
    if (self.isAuthorized==NO) return;
    [[TCSWebServiceController sharedController] restartServiceWithCallback:^(BOOL hadSuccess) {
        if (hadSuccess==NO) {
            [self showApacheError:@"There was an error updating settings and restarting the web service. Please check the webserver log"];
        }

    }];

    
}
- (void)serviceUpdateAvailable:(nonnull NSNotification *)not {
    
}



- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{
    TCSWebServerSettingViewController *dest=segue.destinationController;

    dest.delegate=self;

    if ([segue.identifier isEqualToString:@"addWebserverSettingSegue"]){
        self.editingWorkflowAtIndex=-1;
        __block NSInteger newPort=8088;
        __block BOOL foundConflict=YES;

        while (foundConflict==YES){
            foundConflict=NO;
            [[self.webSettingsArrayController arrangedObjects] enumerateObjectsUsingBlock:^(TCSWebserverSetting *currSetting, NSUInteger idx, BOOL * _Nonnull stop) {
                if (currSetting.port==newPort){
                    foundConflict=YES;
                    newPort++;
                }

            }];

        }
        dest.newWebserverPort=newPort;
    }
    else if ([segue.identifier isEqualToString:@"editWebserverSettingSegue"]){

        self.editingWorkflowAtIndex=[self.webSettingsArrayController selectionIndex];
        dest.representedObject=[[self.webSettingsArrayController arrangedObjects] objectAtIndex:self.editingWorkflowAtIndex];
    }
}




-(void)showApacheError:(NSString *)errorMsg{

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Web Server Error";
    alert.informativeText=errorMsg;

    [alert addButtonWithTitle:@"OK"];
    [alert runModal];

}
-(BOOL)updateConfiguration:(TCSWebserverSetting *)newSettings{
        //turn off any other ones
    __block BOOL portConflict=NO;
    NSMutableArray <TCSWebserverSetting *> *proxyArray=[self mutableArrayValueForKey:@"sharedWebsites"];

    [proxyArray enumerateObjectsUsingBlock:^(TCSWebserverSetting * currSettings, NSUInteger idx, BOOL * _Nonnull stop) {
        if(newSettings.port==currSettings.port && idx!=self.editingWorkflowAtIndex ) {
            portConflict=YES;
        }
        if (newSettings.useForMunkiReport==YES){
            currSettings.useForMunkiReport=NO;
        }
    }];


    if (portConflict==YES) {


        return NO;
    }


    if (self.editingWorkflowAtIndex==-1) {

        [self.webSettingsArrayController addObject:newSettings];
    }
    else {

        [proxyArray replaceObjectAtIndex:self.editingWorkflowAtIndex withObject:newSettings];
        self.webSettingsArrayController.selectionIndex=self.editingWorkflowAtIndex;

    }
    [[TCSWebServiceController sharedController] saveSettings:[self.webSettingsArrayController arrangedObjects] callback:^(BOOL hadSuccess) {
        if (hadSuccess==NO) [self showApacheError:@"There was an error updating settings and restarting the web service. Please check the webserver log"];

    }];
    return YES;

}

- (IBAction)tableDoubleClick:(id)sender {
    if ([self.webserverSettingsTable clickedRow]!=-1) {

        [self performSegueWithIdentifier:@"editWebserverSettingSegue" sender:self];
    }
}
-(void)dealloc{
    [self.authenticationView setAutoupdate:NO];

}
@end
