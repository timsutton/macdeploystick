//
//  TCSDepProfileViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 8/1/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"
#import "WorkflowViewController.h"
#import "TCSDepProfile.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSDepProfileViewController : WorkflowViewController
@property (strong) TCSDepProfile *depProfile;

@end

NS_ASSUME_NONNULL_END
