//
//  TCSMDMMainWindowController.h
//  MDS
//
//  Created by Timothy Perfitt on 10/31/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSMDMMainWindowController : NSWindowController

@end

NS_ASSUME_NONNULL_END
