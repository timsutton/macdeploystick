//
//  TCSButtonSelectFile.h
//  MDS
//
//  Created by Timothy Perfitt on 7/27/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSButtonSelectFile : NSButton
@property (strong) IBOutlet NSTextField *textField;

@end

NS_ASSUME_NONNULL_END
