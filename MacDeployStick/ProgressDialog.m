//
//  ProgressDialog.m
//  MDS
//
//  Created by Timothy Perfitt on 9/9/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "ProgressDialog.h"

@interface ProgressDialog ()
@property (strong) NSString *statusText;
@property (assign) float percentComplete;
@property (assign) BOOL shouldCancel;
@end

@implementation ProgressDialog

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewWillAppear{
    self.shouldCancel=NO;
//    [self.view window].styleMask &= ~(NSClosableWindowMask | NSMiniaturizableWindowMask | NSResizableWindowMask);
//
//    NSWindow *window=[self.view window] ;
//
//    window.title=@"";
}
-(BOOL)windowShouldClose{

    return NO;

}
-(void)cancelUpdateResources:(id)sender{

    self.shouldCancel=YES;
    [self.delegate cancelUpdate:self];
    [self updateStatusText:@"Cancelling. Please Wait..."];
}

- (void)percentCompleted:(float)percentCompleted {
    dispatch_async(dispatch_get_main_queue(), ^{

        if (percentCompleted>0) self.percentComplete=percentCompleted;
    });
}

- (void)updateStatusText:(nonnull NSString *)statusText {

    dispatch_async(dispatch_get_main_queue(), ^{
        self.statusText=statusText;

    });
}



@end
