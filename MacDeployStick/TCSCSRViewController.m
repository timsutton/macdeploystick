//
//  TCSCSRViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 8/29/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSCSRViewController.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSMDMWebController.h"
#import "NSData+Base64.h"
#import "TCSMDMService.h"
#import "TCSKeychain.h"
#import "TCSConfigHelper.h"
#import "TCSUtility.h"
#import "TCSConstants.h"
#define UNSIGNEDCSRFILENAME @"unsignedCSR.pem"
#define PRIVATEKEYFILENAME   @"private.key"
#define APNSFOLDERNAME @"apns"
@interface TCSCSRViewController ()
@property (strong) IBOutlet NSObjectController *csrSettingsObjectController;
@property (strong) NSArray *countryArray;
@property (strong) IBOutlet NSArrayController *countryArrayController;
@property (strong) TCTaskWrapperWithBlocks *tw;
@property (strong) IBOutlet NSObjectController *importObjectController;
@property (strong) TCTaskWrapperWithBlocks *importTaskWrapper;
@end



@implementation TCSCSRViewController
- (IBAction)closeWindow:(id)sender {

    [[self.view window] close];
}
- (void)importAPNSIdentity:(NSURL *)inURL {

    NSString *password=[self password];

    if (!password){

        [self showPasswordError];
        return;
    }
    NSString *importCertificatePath=inURL.path;

    NSString *appSupport=[TCSConfigHelper applicationSupportPath];
    if (!appSupport){
        NSLog(@"no application support path");
        return;
    }

    NSString *privateKey=[[appSupport stringByAppendingPathComponent:APNSFOLDERNAME] stringByAppendingPathComponent:PRIVATEKEYFILENAME];

    NSFileManager *fm=[NSFileManager defaultManager];
    if ([fm fileExistsAtPath:privateKey]==NO) {

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No Private Key Found";
        alert.informativeText=@"The private key was not found. This key is created when you generate a APNS certificate request and is deleted when successfully imported.";

        [alert addButtonWithTitle:@"OK"];

        [alert runModal];
        return;
    }

    NSString *uuid=[[NSUUID UUID] UUIDString];
    NSString *tempImportKeyPath=[privateKey stringByAppendingString:uuid];
    NSTask *task=[[NSTask alloc] init];
    task.launchPath=@"/usr/bin/openssl";
    task.arguments=@[@"rsa",@"-passin",@"stdin",@"-in",privateKey,@"-out",tempImportKeyPath];

    [task setStandardInput:[NSPipe pipe]];
    [task launch];
    [[task.standardInput fileHandleForWriting] writeData:[[password stringByAppendingString:@"\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [task waitUntilExit];
    if (task.terminationStatus!=0){

        [[NSFileManager defaultManager] removeItemAtPath:tempImportKeyPath error:nil];
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Error converting key";
        alert.informativeText=@"There was an error removing the passphrase from the private key. Please check the password and try again.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;
    }


    NSError *err;
    NSString *key=[NSString stringWithContentsOfFile:tempImportKeyPath encoding:NSUTF8StringEncoding error:&err];
    if (!key){
        [[NSFileManager defaultManager] removeItemAtPath:tempImportKeyPath error:nil];

        [[NSAlert alertWithError:err] runModal];
        return;
    }
    [[NSFileManager defaultManager] removeItemAtPath:tempImportKeyPath error:nil];

    NSString *cert=[NSString stringWithContentsOfFile:importCertificatePath encoding:NSUTF8StringEncoding error:&err];
    if (!cert){

        [[NSAlert alertWithError:err] runModal];
        return;
    }
    [[TCSMDMWebController sharedController] sendPayload:@{@"cert":[[cert dataUsingEncoding:NSUTF8StringEncoding] base64EncodedString],@"key":[[key dataUsingEncoding:NSUTF8StringEncoding] base64EncodedString]}  toEndpoint:@"/v1/config/certificate" httpAction:@"PUT" onCompletion:^(NSInteger responseCode, NSData *responseData) {
        [[self.view window] close];

        NSAlert  *alert=[[NSAlert alloc] init];
        alert.messageText=@"Push Certificate Imported";
        alert.informativeText=@"The Push Certificate was successfully imported.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        [[TCSMDMService sharedManager] restartMDMWithCallback:^(NSError * _Nullable err) {

        }];



        NSFileManager *fm=[NSFileManager defaultManager];
        NSError *err;
        NSString *apnsFolder=[appSupport stringByAppendingPathComponent:APNSFOLDERNAME];
        
        if ([fm removeItemAtPath:apnsFolder error:&err]==NO){
            [[NSAlert alertWithError:err] runModal];
        }
        if([[TCSMDMService sharedManager] isRunning]==YES){
            [[TCSMDMService sharedManager] restartMDMWithCallback:^(NSError * _Nullable err) {
                if (err){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSAlert alertWithError:err] runModal];
                    });
                }
            }];

        }
    } onError:^(NSError *error) {

        [[NSAlert alertWithError:error] runModal];

    }];




}
-(NSString *)password{

    NSError *err;
    NSString *password=[TCSKeychain passwordForAccount:@"com.twocanoes.mds.apns" error:&err];

    if (!password && !err) {
       password=[TCSKeychain randomPassword];
        BOOL res=[TCSKeychain setPassword:password forAccount:@"com.twocanoes.mds.apns"];
        if (res==NO){
            return nil;
        }

    }

    return password;


}

-(void)showPasswordError{


    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Keychain Error";
    alert.informativeText=@"There was an error generating a password to protect your export key. Please verify the keycnain keychain is unlocked and try again.";

    [alert addButtonWithTitle:@"OK"];
    [alert runModal];


}
- (IBAction)exportButtonPressed:(id)sender {

    NSInteger selectCountryIndex=[self.countryArrayController selectionIndex];
    NSString *countryCode=[[self.countryArray objectAtIndex:selectCountryIndex] objectForKey:@"code"];

    NSString *email=[[self.csrSettingsObjectController content] objectForKey:@"Email"];

    NSString *password=[self password];

    if (password==nil){
        [self showPasswordError];
        return;
    }

    NSBundle *mainBundle=[NSBundle mainBundle];

    NSString *mdmctlPath=[mainBundle pathForResource:@"micromdm/mdmctl" ofType:@""];

    NSSavePanel *savePanel=[NSSavePanel savePanel];
    savePanel.allowsOtherFileTypes=NO;
    savePanel.allowedFileTypes=@[@"pem"];

    savePanel.nameFieldStringValue=@"unsignedCSR_to_upload_to_twocanoes";
    savePanel.message=@"Export Unsigned CSR to upload to Twocanoes";
    NSModalResponse res=[savePanel runModal];

    if (res!=NSModalResponseOK) {

        return;
    }

    NSURL *saveURL=savePanel.URL;


    [[self.view window] close];

    NSString *appSupport=[TCSConfigHelper applicationSupportPath];
    if (!appSupport){
        NSLog(@"no application support path");
        return;
    }

    NSString *apnsFolder=[appSupport stringByAppendingPathComponent:APNSFOLDERNAME];
    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:apnsFolder]==NO) {

        NSError *err;
        if([fm createDirectoryAtPath:apnsFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return;
        }
    }

    //openPanel.URL.path;

    NSString *csrPath=[apnsFolder stringByAppendingPathComponent:UNSIGNEDCSRFILENAME];
    NSString *privateKeyPath=[apnsFolder stringByAppendingPathComponent:PRIVATEKEYFILENAME];

    NSArray *arguments=@[mdmctlPath,@"mdmcert",@"push",@"-local-only", @"-country",countryCode,@"-email",email,@"-password",password,@"-out",csrPath,@"-private-key",privateKeyPath];
    self.tw=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{
        NSLog(@"args: %@",arguments);

    } endBlock:^{

        NSFileManager *fm=[NSFileManager defaultManager];


        if (self.tw.terminationStatus!=0) {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error";
            alert.informativeText=@"There was an error saving the files. Please verify permissions and disk space and try again.";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];

            return;
        }
        else {
            NSError *err;
            if ([fm fileExistsAtPath:saveURL.path]){

                if ([fm removeItemAtURL:saveURL error:&err]==NO){

                    [[NSAlert alertWithError:err] runModal];
                    return;
                }
            }

            if([fm copyItemAtURL:[NSURL fileURLWithPath:csrPath] toURL:saveURL error:&err]==NO){

                [[NSAlert alertWithError:err] runModal];
                return;
            }
            
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Successfully Exported";
            alert.informativeText=@"The Certificate Signing Request (CSR) has been successfully exported. Create an account on profile.twocanoes.com and upload this file to be signed.";
            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
            return;

        }
    } outputBlock:^(NSString *output) {

        NSLog(@"%@",output);
    } errorOutputBlock:^(NSString *errorOutput) {
        NSLog(@"%@",errorOutput);
    } arguments:arguments];

    [self.tw startProcess];
}
- (IBAction)importAPNSPushCertificate:(id)sender {

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.allowsOtherFileTypes=NO;
    openPanel.allowedFileTypes=@[@"cer",@"pem",@"crt"];

    openPanel.message=@"Select a file in PEM format that contains a valid certificate for the MDM server:";
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {
        return;
    }
    NSURL *openURL=openPanel.URL;
    [self importAPNSIdentity:openURL];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.preferredContentSize=self.view.frame.size;


    NSString *countryCodeFilePath=[[NSBundle mainBundle] pathForResource:@"CountryCodes" ofType:@"csv"];

    NSError *err;
    NSString *countryCodes=[NSString stringWithContentsOfFile:countryCodeFilePath encoding:NSUTF8StringEncoding error:&err];

    NSArray *lineArray=[countryCodes componentsSeparatedByString:@"\n"];

    __block NSMutableArray *finalArray=[NSMutableArray array];
    [lineArray enumerateObjectsUsingBlock:^(NSString *line, NSUInteger idx, BOOL * _Nonnull stop) {


        NSArray *items=[line componentsSeparatedByString:@","];

        [finalArray addObject:@{@"description":items[0],@"code":items[1]}];


    }];
    self.countryArray=[NSArray arrayWithArray:finalArray];
}

@end
