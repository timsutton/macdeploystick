//
//  MDMToolbarItem.m
//  MDS
//
//  Created by Timothy Perfitt on 9/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "MDMToolbarItem.h"
#import "TCSMDMService.h"
@implementation MDMToolbarItem
-(void)validate{

    [self setEnabled:[[TCSMDMService sharedManager] isRunning]];
}
@end
