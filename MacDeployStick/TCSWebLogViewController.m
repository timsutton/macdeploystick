//
//  TCSWebLogViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/13/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSWebLogViewController.h"
#import <GCDWebServers/GCDWebServers.h>

@interface TCSWebLogViewController ()
@property (strong) GCDWebServer* webServer;
@property (assign) BOOL isRunning;
@property (strong) NSString *webServerURLString;
@property (weak) IBOutlet NSTableView *logTableView;
@end

@implementation TCSWebLogViewController 
- (IBAction)clearButtonPressed:(id)sender {
    [self.arrayController  removeObjects:self.arrayController.arrangedObjects];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.logArray=[NSMutableArray array];
}

-(void)viewWillDisappear{

//    [self.webServer stop];
}
-(IBAction)startStopWebServer:(NSButton *)inButton{


    if (!self.webServer){
        self.webServer = [[GCDWebServer alloc] init];
    }

    if ([self.webServer isRunning]==YES)  {

        [self.webServer stop];
        self.webServer=nil;
        self.isRunning=NO;

        self.webServerURLString=nil;
        return;


    }
    // Add a handler to respond to GET requests on any URL
    [self.webServer addDefaultHandlerForMethod:@"GET"
                             requestClass:[GCDWebServerRequest class]
                             processBlock:^GCDWebServerResponse *(GCDWebServerRequest* request) {

                                 return [GCDWebServerDataResponse responseWithHTML:@"<html><body><p>MDS Connection Verified.</p></body></html>"];

                             }];
__weak TCSWebLogViewController *self_ = self; // that's enough
    [self.webServer addDefaultHandlerForMethod:@"POST" requestClass:[GCDWebServerURLEncodedFormRequest class] processBlock:^GCDWebServerResponse * _Nullable(__kindof GCDWebServerRequest * _Nonnull request) {
        NSString* message = [[(GCDWebServerURLEncodedFormRequest*)request arguments] objectForKey:@"message"];
        NSString* serial = [[(GCDWebServerURLEncodedFormRequest*)request arguments] objectForKey:@"serial"];
        NSString* status = [[(GCDWebServerURLEncodedFormRequest*)request arguments] objectForKey:@"status"];
        NSString* hostname = [[(GCDWebServerURLEncodedFormRequest*)request arguments] objectForKey:@"hostname"];

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self_.arrayController addObject:@{@"hostname":hostname==nil?@"":hostname,@"serial":serial==nil?@"":serial,@"status":status==nil?@"":status,@"message":message==nil?@"":message,@"timestamp":[NSDate date]}];
            NSInteger numberOfRows = [self_.logTableView numberOfRows];

            if (numberOfRows > 0)
                [self_.logTableView scrollRowToVisible:numberOfRows - 1];

        });

//        NSLog(@"%@:%@:%@",serial,status, message);
        return [GCDWebServerResponse responseWithStatusCode:200];

    }];
    // Use convenience method that runs server on port 8080
    // until SIGINT (Ctrl-C in Terminal) or SIGTERM is received
    if([self.webServer startWithPort:8080 bonjourName:@""]==NO){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Error Starting Webserver";
        alert.informativeText=@"The log webserver could not be started. Please check that nothing else is using network port 8080 and try again.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;
    }
    self.isRunning=YES;

    self.webServerURLString=[[self.webServer serverURL] description];


}



@end
