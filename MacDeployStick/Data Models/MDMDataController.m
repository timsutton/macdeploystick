//
//  MDMDataController.m
//  MDS
//
//  Created by Timothy Perfitt on 8/31/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "MDMDataController.h"
#import <AppKit/AppKit.h>
#import "DeviceMO+CoreDataClass.h"
#import "ResponseMO+CoreDataClass.h"
#import "RequestMO+CoreDataClass.h"
#import "ProfileMO+CoreDataClass.h"
#import "CertificateMO+CoreDataClass.h"
#import "SecurityInfoMO+CoreDataClass.h"
#import "FirewallSettingsMO+CoreDataClass.h"
#import "FirewallApplicationsMO+CoreDataClass.h"
#import "FirmwarePasswordStatusMO+CoreDataClass.h"
#import "ManagementStatusMO+CoreDataClass.h"
#import "TCSConfigHelper.h"

#import "NSData+Base64.h"
@interface MDMDataController()
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;

@end
@implementation MDMDataController
+ (instancetype)dataController {
    static id myDataController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        myDataController = [[[self class] alloc] init];


    });
    return myDataController;
}
- (id)init{

    self = [super init];
    if (!self) return nil;

    //This resource is the same name as your xcdatamodeld contained in your project
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    NSAssert(modelURL, @"Failed to locate momd bundle in application");
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    NSManagedObjectModel *mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSAssert(mom, @"Failed to initialize mom from URL: %@", modelURL);

    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];

    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [moc setPersistentStoreCoordinator:coordinator];
    self.managedObjectContext=moc;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSPersistentStoreCoordinator *psc = [[self managedObjectContext] persistentStoreCoordinator];

        NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

        NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

        NSFileManager *fm=[NSFileManager defaultManager];

        if ([fm fileExistsAtPath:appSupportDir]==NO){

            NSError *err;

            if([fm createDirectoryAtPath:appSupportDir withIntermediateDirectories:YES attributes:nil error:&err]==NO){

                NSLog(@"%@",err.localizedDescription);
                abort();

            }
        }
        // The directory the application uses to store the Core Data store file. This code uses a file named "DataModel.sqlite" in the application's documents directory.
        NSURL *storeURL = [[NSURL fileURLWithPath:appSupportDir] URLByAppendingPathComponent:@"DataModel.sqlite"];

        NSError *error = nil;
        NSPersistentStore *store = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
        if (!store) {
            NSLog(@"Failed to initalize persistent store: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
            //A more user facing error message may be appropriate here rather than just a console log and an abort
        }
    });

    self.persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Model"];
    [self.persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *description, NSError *error) {
        if (error != nil) {
            NSLog(@"Failed to load Core Data stack: %@", error);
            abort();
        }
        
    }];

    return self;
}

-(DeviceMO *)deviceForUDID:(NSString *)udid{

    NSManagedObjectContext *moc = self.managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Device"];

    [request setPredicate:[NSPredicate predicateWithFormat:@"udid == %@", udid]];

    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!results) {
        return nil;

    }
    return results.lastObject;

}
-(DeviceMO *)deviceForSerial:(NSString *)serial{

    NSManagedObjectContext *moc = self.managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Device"];

    [request setPredicate:[NSPredicate predicateWithFormat:@"serial == %@", serial]];

    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!results) {
        return nil;

    }
    return results.lastObject;

}

-(BOOL)addRequestForDevice:(NSString *)deviceUDID requestType:(NSString *)requestType commandUUID:(NSString *)uuid{

    DeviceMO *device;
    if (!(device=[self deviceForUDID:deviceUDID])){
        return NO;
    }

    RequestMO *request = [NSEntityDescription insertNewObjectForEntityForName:@"Request" inManagedObjectContext:self.managedObjectContext];

    if (uuid) request.commandUUID=uuid;
    if (requestType) request.requestType=requestType;

    device.requests=[device.requests setByAddingObject:request];

    NSError *err;

    if([self.managedObjectContext save:&err]==NO){

        NSLog(@"%@",err.localizedDescription);
    }

    return YES;
}
-(BOOL)addResponseForDevice:(NSString *)deviceUDID status:(NSString *)status commandUUID:(nullable NSString *)commandUUID dateCreated:(NSDate *)dateCreated payload:(NSString *)payload{

    DeviceMO *device;
    if (!(device=[self deviceForUDID:deviceUDID])){
        return NO;
    }

    
    ResponseMO *response = [NSEntityDescription insertNewObjectForEntityForName:@"Response" inManagedObjectContext:self.managedObjectContext];
    if (status) response.status=status;
    if (dateCreated) response.dateCreated=dateCreated;
    if (payload) response.payload=payload;

    if (commandUUID) {

        NSPredicate *commandPredicate=[NSPredicate predicateWithFormat:@"SELF.commandUUID == %@", commandUUID];
        NSSet *results=[device.requests filteredSetUsingPredicate:commandPredicate];
        RequestMO *request=[results anyObject];
        request.response=response;
        //this is response to a command. find the command
        NSData *payloadData;
        NSDictionary *payloadDict;
        if (payload){
            payloadData=[NSData dataFromBase64String:payload];
            NSError *err;
            payloadDict=[NSPropertyListSerialization propertyListWithData:payloadData options:NSPropertyListImmutable format:nil error:&err];

            if (!payloadDict) {
                NSLog(@"%@",err.localizedDescription);
                return NO;
            }
        }

        if ([request.requestType isEqualToString:@"ProfileList"] && payload ){
        //we have a response to a profileList command. clear out the old ones and add new ones
            NSArray *profileList;
            if ((profileList=payloadDict[@"ProfileList"])){

                device.installedProfiles=[NSSet set];

                [profileList enumerateObjectsUsingBlock:^(NSDictionary *currProfileDict, NSUInteger idx, BOOL * _Nonnull stop) {

                    ProfileMO *profile = [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:self.managedObjectContext];


                    profile.profileDict=currProfileDict;
                    device.installedProfiles=[[device installedProfiles] setByAddingObject:profile];

                }];


            }
        }
        else if ([request.requestType isEqualToString:@"CertificateList"] && payload ){
            //we have a response to a profileList command. clear out the old ones and add new ones
            NSArray *certificateList;
            if ((certificateList=payloadDict[@"CertificateList"])){

                device.certificates=[NSSet set];

                [certificateList enumerateObjectsUsingBlock:^(NSDictionary *currCertDict, NSUInteger idx, BOOL * _Nonnull stop) {

                    CertificateMO *cert = [NSEntityDescription insertNewObjectForEntityForName:@"Certificate" inManagedObjectContext:self.managedObjectContext];


                    if (currCertDict[@"Data"]) cert.data=currCertDict[@"Data"];
                    if (currCertDict[@"CommonName"]) cert.commonName=currCertDict[@"CommonName"];
                    if (currCertDict[@"IsIdentity"]) cert.isIdentity=[currCertDict[@"IsIdentity"] boolValue];

                    device.certificates=[[device certificates] setByAddingObject:cert];

                }];


            }
        }
        else if ([request.requestType isEqualToString:@"DeviceInformation"] && payload ){
            NSDictionary*deviceInfoDict;
            if ((deviceInfoDict=payloadDict[@"QueryResponses"])){
                if (deviceInfoDict[@"DeviceName"]) device.deviceName=deviceInfoDict[@"DeviceName"];
                if (deviceInfoDict[@"EthernetMAC"]) device.ethernetMAC=deviceInfoDict[@"EthernetMAC"];
                if (deviceInfoDict[@"BluetoothMAC"]) device.bluetoothMAC=deviceInfoDict[@"BluetoothMAC"];
                if (deviceInfoDict[@"HostName"]) device.hostName=deviceInfoDict[@"HostName"];
                if (deviceInfoDict[@"LocalHostName"]) device.localHostName=deviceInfoDict[@"LocalHostName"];
                if (deviceInfoDict[@"DeviceCapacity"]) device.deviceCapacity=[deviceInfoDict[@"DeviceCapacity"] floatValue];

                if (deviceInfoDict[@"DeviceCapacity"]) device.buildVersion=deviceInfoDict[@"BuildVersion"];

            }

        }
        else if ([request.requestType isEqualToString:@"SecurityInfo"] && payload ){
            //we have a response to a profileList command. clear out the old ones and add new ones
            NSDictionary*securityInfoDict;
            if ((securityInfoDict=payloadDict[@"SecurityInfo"])){

                device.securityInfo=nil;

                SecurityInfoMO *secInfo = [NSEntityDescription insertNewObjectForEntityForName:@"SecurityInfo" inManagedObjectContext:self.managedObjectContext];

                if (securityInfoDict[@"FDE_Enabled"]) secInfo.fdeEnabled=[securityInfoDict[@"FDE_Enabled"] boolValue];
                if (securityInfoDict[@"FDE_HasInstitutionalRecoveryKey"]) secInfo.fdeHasInstitutionalRecoveryKey=[securityInfoDict[@"FDE_HasInstitutionalRecoveryKey"] boolValue];
                if (securityInfoDict[@"FDE_HasPersonalRecoveryKey"]) secInfo.fdeHasPersonalRecoveryKey=[securityInfoDict[@"FDE_HasPersonalRecoveryKey"] boolValue];
                if (securityInfoDict[@"RemoteDesktopEnabled"]) secInfo.remoteDesktopEnabled=[securityInfoDict[@"RemoteDesktopEnabled"] boolValue];
                if (securityInfoDict[@"SystemIntegrityProtectionEnabled"]) secInfo.systemIntegrityProtectionEnabled=[securityInfoDict[@"SystemIntegrityProtectionEnabled"] boolValue];

                if (securityInfoDict[@"FirewallSettings"]) {
                    NSDictionary *fwSettingsDict=securityInfoDict[@"FirewallSettings"];

                    FirewallSettingsMO *firewallSettings = [NSEntityDescription insertNewObjectForEntityForName:@"FirewallSettings" inManagedObjectContext:self.managedObjectContext];
                    if (fwSettingsDict[@"BlockAllIncoming"]){
                        firewallSettings.blockAllIncoming=[fwSettingsDict[@"BlockAllIncoming"] boolValue];
                    }
                    if (fwSettingsDict[@"FirewallEnabled"]){
                        firewallSettings.firewallEnabled=[fwSettingsDict[@"FirewallEnabled"] boolValue];
                    }
                    if (fwSettingsDict[@"StealthMode"]){
                        firewallSettings.stealthMode=[fwSettingsDict[@"StealthMode"] boolValue];
                    }
                    if (fwSettingsDict[@"Applications"]){
                        NSArray *fwAppsArray=fwSettingsDict[@"Applications"];

                        [fwAppsArray enumerateObjectsUsingBlock:^(NSDictionary * currFirewallAppsDict, NSUInteger idx, BOOL * _Nonnull stop) {
                            FirewallApplicationsMO *firewallApp = [NSEntityDescription insertNewObjectForEntityForName:@"FirewallApplications" inManagedObjectContext:self.managedObjectContext];

                            if (currFirewallAppsDict[@"Allowed"]) firewallApp.allowed=[currFirewallAppsDict[@"Allowed"] boolValue];
                            if (currFirewallAppsDict[@"BundleID"]) firewallApp.bundleID=currFirewallAppsDict[@"BundleID"] ;
                            if (currFirewallAppsDict[@"Name"]) firewallApp.name=currFirewallAppsDict[@"Name"] ;

                            firewallSettings.firewallApplications=[firewallSettings.firewallApplications setByAddingObject:firewallApp];
                        }];
                        secInfo.firewallSettings=[NSSet setWithObject:firewallSettings];


                    }

                }
                if (securityInfoDict[@"FirmwarePasswordStatus"]) {
                    NSDictionary *firmwarePasswordDict=securityInfoDict[@"FirmwarePasswordStatus"];
                    FirmwarePasswordStatusMO *firmwarePassword = [NSEntityDescription insertNewObjectForEntityForName:@"FirmwarePasswordStatus" inManagedObjectContext:self.managedObjectContext];

                    if (firmwarePasswordDict[@"AllowOroms"]) firmwarePassword.allowOroms=[firmwarePasswordDict[@"AllowOroms"] boolValue];
                    if (firmwarePasswordDict[@"ChangePending"]) firmwarePassword.changePending=[firmwarePasswordDict[@"ChangePending"] boolValue];
                    if (firmwarePasswordDict[@"PasswordExists"]) firmwarePassword.passwordExists=[firmwarePasswordDict[@"PasswordExists"] boolValue];
                    secInfo.firmwarePasswordStatus=[NSSet setWithObject:firmwarePassword];

                }

                if (securityInfoDict[@"ManagementStatus"]) {
                    NSDictionary *managementStatusDict=securityInfoDict[@"ManagementStatus"];
                    ManagementStatusMO *managementStatus = [NSEntityDescription insertNewObjectForEntityForName:@"ManagementStatus" inManagedObjectContext:self.managedObjectContext];

                    if (managementStatusDict[@"EnrolledViaDEP"]) managementStatus.enrolledViaDEP=[managementStatusDict[@"EnrolledViaDEP"] boolValue];
                    if (managementStatusDict[@"UserApprovedEnrollment"]) managementStatus.userApprovedEnrollment=[managementStatusDict[@"UserApprovedEnrollment"] boolValue];

                    secInfo.managementStatus=[NSSet setWithObject:managementStatus];

                }

                device.securityInfo=secInfo;
            }
        }
    }

    else {
        [device.responses setByAddingObject:response];

    }
    NSError *err;

    if([self.managedObjectContext save:&err]==NO){

        NSLog(@"%@",err.localizedDescription);
    }


    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONMDMCLIENTUPDATED object:self userInfo:@{TCSNOTIFICATIONUDIDKEY:deviceUDID}];
    return YES;
}
-(void)removeDeviceWithUDID:(NSString *)udid{

    DeviceMO *device=[self deviceForUDID:udid];
    if (device){

        [self.managedObjectContext deleteObject:device];
    }
}
-(void)addDevice:(NSDictionary *)deviceInfo{

    DeviceMO *deviceEntity;


    NSString *serial=[deviceInfo objectForKey:@"serial_number"];

    if (!(deviceEntity=[self deviceForSerial:serial]))
        deviceEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Device" inManagedObjectContext:self.managedObjectContext];

    deviceEntity.serial=serial;

    if ([deviceInfo objectForKey:@"udid"]){
        deviceEntity.udid=[deviceInfo objectForKey:@"udid"];
    }
    if ([deviceInfo objectForKey:@"last_seen"]){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        NSDate *date = [dateFormatter dateFromString:[deviceInfo objectForKey:@"last_seen"]];

        deviceEntity.lastSeen=date;
    }
    if ([deviceInfo objectForKey:@"enrollment_status"]){
        deviceEntity.enrollmentStatus=[[deviceInfo objectForKey:@"enrollment_status"] boolValue];
    }

    NSError *err;
    if([self.managedObjectContext save:&err]==NO){
        [[NSAlert alertWithError:err] runModal];
        NSLog(@"%@",err.localizedDescription);
    }
}
-(NSArray <CertificateMO *> *)certificatesForDeviceUDID:(NSString *)deviceUDID{

   DeviceMO *device=[self deviceForUDID:deviceUDID];

    return [device.certificates allObjects];


}
-(NSArray *)allDeviceSerials{


    NSArray *allDevices=[self allDevices];

    [allDevices enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

    }];
    return nil;
}
-(NSArray *)allDevices{
    NSManagedObjectContext *moc = self.managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Device"];

//    [request setPredicate:[NSPredicate predicateWithFormat:@"udid == %@", uuid]];

    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (!results) {
        return nil;

    }
    return results;
}
@end
