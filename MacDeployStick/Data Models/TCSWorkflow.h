//
//  Workflow.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/23/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSMDSVariable.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, RestartAction) {
    None,
    Restart,
    Shutdown


};

typedef NS_ENUM(NSUInteger, ComputerName) {
    ComputerNameAsk,
    ComputerNameSerialAsk,
    ComputerNameSerialForce,
    ComputerNameRemoved

};


@interface TCSWorkflowUser : NSObject <NSSecureCoding>

@property (strong) NSString *userUUID;

@property (assign) BOOL isAdmin;
@property (assign) BOOL shouldAutologin;
@property (assign) BOOL isHidden;
@property (strong) NSString *fullName;
@property (strong) NSString *shortName;
@property (strong) NSString *sshKey;
@property (strong) NSString *passwordHint;
@property (strong) NSString *jpegPhoto;


@property (strong) NSString *uid;
@property (strong) NSString *password;

@end

@interface TCSWorkflowLocalization : NSObject <NSCopying,NSSecureCoding>
@property (strong) NSString *keyboardLayout;
@property (strong) NSString *language;
@property (strong) NSString *locale;
@property (strong) NSString *timezone;
@end


@interface TCSWorkflow : NSObject <NSCopying,NSSecureCoding>
@property (assign) BOOL isActive;
@property (assign) BOOL isDefault;
@property (assign) BOOL isReadonly;
@property (strong) NSString *workflowName;
@property (strong) NSString *workflowDescription;
@property (strong) NSUUID *workflowUUID;

@property (assign) BOOL useComputerName;
@property (assign) NSInteger computerName;

@property (strong) NSMutableArray *users;
//create user stuff
@property (assign) BOOL shouldCreateUser;
@property (assign) BOOL shouldCreateUserAdmin;
@property (assign) BOOL shouldCreateUserAutologin;
@property (assign) BOOL shouldCreateUserHidden;
@property (strong) NSString *createUserFullName;
@property (strong) NSString *createUserShortName;
@property (strong) NSString *createUserSSHKey;
@property (strong) NSString *passwordHint;

@property (strong) NSString *createUserUID;
@property (strong) NSString *createUserPassword;

@property (assign) BOOL usePackagesFolderPath;
@property (strong) NSString *packagesFolderPath;
@property (strong) NSArray *packagesFolderOrder;

@property (strong) NSString *macOSFolderPath;
@property (strong) NSString *macOSInstallerDMGURL;
@property (strong) NSString *asrImageURL;

@property (strong) NSString *macOSImagePath;


@property (assign) BOOL useScriptFolderPath;
@property (strong) NSString *scriptFolderPath;
@property (strong) NSArray *scriptFolderOrder;

@property (strong) NSArray *prePackageScripts;
@property (strong) NSArray *preInstallPackages;
@property (strong) NSArray *firstRunPackages;
@property (strong) NSArray *disabledPackages;

@property (strong) NSArray *workflowFirstRunScripts;

@property (strong) NSArray *prePackageProfiles;
@property (strong) NSArray *firstRunProfiles;
@property (strong) NSArray *disabledProfiles;

@property (strong) NSData *scriptFolderBookmark;
@property (strong) NSArray *disabledScripts;

@property (assign) BOOL useProfilesFolderPath;
@property (strong) NSString *profilesFolderPath;
@property (strong) NSArray *profilesFolderOrder;

@property (strong) NSData *profilesFolderBookmark;

@property (assign) BOOL shouldRebootAfterInstallingResources;
@property (assign) BOOL shouldRebootAfterInstallingResourcesInstallOS;

@property (assign) NSInteger restartOrShutdownIndex;
@property (assign) NSInteger restartOrShutdownIndexInstallOS;

@property (strong) NSData *packagesFolderBookmark;

@property (assign) BOOL shouldImage;
@property (assign) BOOL shouldSkipMacOSInstall;
@property (assign) BOOL shouldInstall;
@property (assign) BOOL shouldUseMacOSInstallerURL;
@property (assign) BOOL shouldUseAsrDmgURL;


@property (assign) BOOL shouldEraseAndInstall;
@property (assign) BOOL shouldRunSoftwareUpdate;
@property (assign) BOOL shouldSetComputerName;
@property (strong) NSString *computerNameString;
@property (assign) BOOL shouldPromptForComputerName;

@property (assign) NSInteger computerNameTypeIndex;

@property (assign) BOOL shouldSetVolumeName;
@property (strong) NSString *volumeName;

@property (assign) BOOL shouldSkipSetupAssistant;
@property (assign) BOOL shouldWaitForNetwork;

@property (assign) BOOL shouldEnableLocationServices;
@property (assign) BOOL shouldSkipPrivacySetup;
@property (assign) BOOL shouldEnableSSH;
@property (assign) BOOL shouldEnableMunkiReport;
@property (assign) BOOL shouldEnableScreenSharing;
@property (assign) BOOL shouldEnableARD;

@property (strong) NSString *munkiReportEnrollmentScript;


@property (assign) BOOL shouldDisableSIP;


@property (assign) BOOL shouldBlessTarget;

@property (assign) BOOL useWorkflowLocalization;

@property (strong) TCSWorkflowLocalization *workflowLocalization;
@property (assign) BOOL shouldConfigureWifi;
@property (strong) NSString *wifiSSID;
@property (strong) NSString *wifiPassword;

@property (assign) BOOL shouldSetVariables;
@property (strong) NSString *mdsVar1;
@property (strong) NSString *mdsVar2;
@property (strong) NSString *mdsVar3;
@property (strong) NSString *mdsVar4;
@property (strong) NSString *mdsVar5;
@property (strong) NSString *mdsVar6;
@property (strong) NSString *mdsVar7;
@property (strong) NSString *mdsVar8;
@property (strong) NSString *mdsVar9;
@property (strong) NSMutableArray <TCSMDSVariable *>*mdsVariables;

@property (strong) NSString *munkiWorkflowURL;
@property (strong) NSString *munkiClientInstallerPath;
@property (assign) BOOL shouldConfigureMunkiClient;

@property (assign) BOOL shouldTrustMunkiClientCertificate;
@property (assign) BOOL shouldTrustServerCertificate;

@property (strong) NSString *munkiClientCertificate;

@end

NS_ASSUME_NONNULL_END
