//
//  TCSWorkflowTableViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/9/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDeviceTableViewDatasource.h"

@interface TCSDeviceTableViewDatasource ()
@property (strong) NSArray *tableItems;
@property (assign) NSInteger selectedRow;
@end

@implementation TCSDeviceTableViewDatasource
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableItems=@[
                          @{@"name":@"Device Info",@"image":@"DeviceInfo"},
                          @{@"name":@"Profiles",@"image":@"Profiles"},
                          @{@"name":@"Security",@"image":@"Security"},
                          @{@"name":@"Certificates",@"image":@"Certificates"},
                         
                          ];


    }
    return self;
}



- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{


    return self.tableItems.count;
}
-(void)tableViewSelectionDidChange:(NSNotification *)inNot{

    NSTableView *tableView=[inNot object];
    self.selectedRow=tableView.selectedRow;
}
- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row {

    // Retrieve to get the @"MyView" from the pool or,
    // if no version is available in the pool, load the Interface Builder version
    NSTableCellView *result = [tableView makeViewWithIdentifier:@"DeviceInfoView" owner:self];

    // Set the stringValue of the cell's text field to the nameArray value at row
    NSDictionary *currentObject=[self.tableItems objectAtIndex:row];
    result.textField.stringValue =currentObject[@"name"];
    NSImage *rowImage=[NSImage imageNamed:currentObject[@"image"]];

    [rowImage setTemplate:YES];
    [result.imageView setImage:rowImage];    // Return the result
    return result;
}
@end
