//
//  TCSMDSVariable.m
//  MDS
//
//  Created by Timothy Perfitt on 1/21/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMDSVariable.h"
@interface TCSMDSVariable()

@end
@implementation TCSMDSVariable
+ (BOOL)supportsSecureCoding {
    return YES;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.variableOptionsArray=[NSMutableArray array];

    }
    return self;
}
- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    TCSMDSVariable *newMDSVariable=[[TCSMDSVariable alloc] init];

    newMDSVariable.variableOptionsArray=[_variableOptionsArray copy];
    newMDSVariable.mdsVarPrompt=[_mdsVarPrompt copy];
    newMDSVariable.mdsVariableName=[_mdsVariableName copy];

    newMDSVariable.variableTypeIndex=_variableTypeIndex;
    newMDSVariable.variableOtherOption=_variableOtherOption;

    return newMDSVariable;

}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.variableOptionsArray = [decoder decodeObjectForKey:@"variableOptionsArray"];

    if (!self.variableOptionsArray) {

        self.variableOptionsArray=[decoder decodeObjectForKey:@"variableOptionsArray"];
    }

    self.mdsVarPrompt=[decoder decodeObjectForKey:@"mdsVarPrompt"];
    self.mdsVariableName=[decoder decodeObjectForKey:@"mdsVariableName"];
    self.variableTypeIndex=[decoder decodeIntegerForKey:@"variableTypeIndex"];;
    self.variableOtherOption=[decoder decodeBoolForKey:@"variableOtherOption"];;


    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.variableOptionsArray forKey:@"variableOptionsArray"];
    [encoder encodeObject:self.mdsVarPrompt forKey:@"mdsVarPrompt"];
    [encoder encodeObject:self.mdsVariableName forKey:@"mdsVariableName"];

    [encoder encodeInteger:self.variableTypeIndex forKey:@"variableTypeIndex"];
    [encoder encodeBool:self.variableOtherOption forKey:@"variableOtherOption"];
}

@end
