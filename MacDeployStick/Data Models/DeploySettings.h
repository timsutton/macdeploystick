//
//  DeploySettings.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSWorkflow.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeploySettings : NSObject <NSSecureCoding>

@property (nonatomic,strong) NSURL *backgroundImageURL;
@property (nonatomic,strong) NSURL *externalVolumeURL;
@property (nonatomic,assign) BOOL shouldSaveToDMG;
@property (readonly) NSMutableArray *workflows;

@property (strong)  NSString *saveFilePath;
@property (atomic,assign) BOOL stopCopying;
+(NSString *)syncFolder;
+ (id)sharedSettings ;
+(NSArray *)workflowArrayFromData:(NSData *)inData;
+(NSData *)workflowDataFromArray:(NSArray *)inArray;

- (instancetype)initWithWorkflows:(NSArray *)workflows;
-(void)addWorkflow:(TCSWorkflow *)workflow;
-(void)replaceWorkflow:(TCSWorkflow *)oldWorkflow withWorkflow:(TCSWorkflow *)workflow;
-(void)saveToPrefs:(id)sender;
-(NSDictionary *)imagrDictionaryWithError:(NSError **)err;
-(BOOL)saveResourcesToURL:(NSURL *)inPath withPrefix:(nullable NSURL *)inPrefix error:(NSError **)error;
-(NSArray *)macOSDiskImages;
-(BOOL)copyOSesToDestination:(NSString *)inDestinationFolder error:(NSError *__autoreleasing *)error;

-(BOOL)addWorkflowsFromData:(NSData *)inData;
-(NSData *)workflowData;
@end

NS_ASSUME_NONNULL_END
