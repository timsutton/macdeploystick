//
//  TCSConfigHelper.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/30/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSWorkflow.h"
#define TCSNOTIFICATIONMDMCLIENTUPDATED @"TCSNOTIFICATIONMDMCLIENTUPDATED"
#define TCSNOTIFICATIONUDIDKEY @"UDID"
#define TCSSelectKeyPressed @"TCSSelectKeyPressed"
#define TCSMDMRUNNING @"MDMServiceRunning"
NS_ASSUME_NONNULL_BEGIN

@interface TCSConfigHelper : NSObject
+(NSDictionary *)WifiConfigWithSSID:(NSString *)ssid password:(NSString *)password;
+(BOOL)savePackageNamed:(NSString *)inName path:(NSString *)inPath fromProfile:(NSDictionary *)inProfile;
+(BOOL)saveScripts:(NSArray *__nullable )inScripts toPath:(NSString *)destinationPackagePath profiles:(NSArray * __nullable)inProfiles resourcePath:(NSString *__nullable)resourcesPath waitForNetworking:(BOOL)shouldWaitForNetworking shouldSetComputerName:(BOOL)shouldSetComputerName workflow:(TCSWorkflow * __nullable)inWorkflow shouldSetOneTimeSettings:(BOOL)shouldSetOneTimeSettings shouldSetPasswordHint:(BOOL)shouldSetPasswordHint wifiAlreadySet:(BOOL)wifiAlreadySet shouldRunSoftwareUpdate:(BOOL)shouldRunSoftwareUpdate
      rebootAction:(int)rebootIndex
 shouldCreateUsers:(BOOL)shouldCreateUsers
             error:(NSError **)err;
+(NSString *)imagrVersion;
+(NSString *)imagrPath;
+(NSString *)imagrVersionAtPath:(nullable NSString *)inImgrPath;
+(BOOL)checkIfResourcesNeeded;
+(NSString *)manifestForPackagePath:(NSString *)finalPackagePath packageName:(NSString *)packageName;
+(NSString *)applicationSupportPath;
@end


NS_ASSUME_NONNULL_END
