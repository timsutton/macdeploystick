//
//  TCSTabViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 7/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSTabViewController.h"

@interface TCSTabViewController ()

@end

@implementation TCSTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *sourceRunPath=[[NSBundle mainBundle] pathForResource:@"run" ofType:@""];

    NSImage *icon=[[NSWorkspace sharedWorkspace] iconForFile:sourceRunPath];
    self.tabViewItems[0].image=[NSImage imageNamed:@"gear"];
    self.tabViewItems[1].image=icon;
    self.tabViewItems[2].image=[NSImage imageNamed:@"keys"];
    self.tabViewItems[3].image=[NSImage imageNamed:@"munkibutton"];
    self.tabViewItems[4].image=[NSImage imageNamed:@"package"];
    self.tabViewItems[5].image=[NSImage imageNamed:@"mdm"];
    self.tabViewItems[6].image=[NSImage imageNamed:@"logging"];
    self.tabViewItems[7].image=[NSImage imageNamed:@"RefreshSyncing"];


}

@end
