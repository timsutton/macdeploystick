//
//  TCSMunkiReportEditUserViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/15/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMunkiReportEditUserViewController.h"
@interface TCSMunkiReportEditUserViewController ()
@property (strong) NSMutableDictionary *createdUser;
@end

@implementation TCSMunkiReportEditUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.createdUser=[NSMutableDictionary dictionary];
}
-(IBAction)createUserButtonPressed:(id)sender{
    if ([self.createdUser[@"password"] isEqualToString:self.createdUser[@"verifyPassword"]]==NO){

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Password Mismatch";
        alert.informativeText=@"The password does not match the verify password.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;
    }
    [self.delegate userCreated:[NSDictionary dictionaryWithDictionary:self.createdUser]];

    [self dismissViewController:self];

}

@end
