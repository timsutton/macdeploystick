//
//  TCSMunkiReportEditUserViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/15/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMunkiReportEditUserViewController.h"
NS_ASSUME_NONNULL_BEGIN
@protocol TCSMunkiReportUserCreatedProtocol <NSObject>

-(void)userCreated:(NSDictionary *)user;

@end

@interface TCSMunkiReportEditUserViewController : NSViewController
@property (assign) id <TCSMunkiReportUserCreatedProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
