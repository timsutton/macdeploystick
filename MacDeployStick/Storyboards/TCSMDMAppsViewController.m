//
//  TCSMDMAppsViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 2/27/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMDMAppsViewController.h"
#import "TCSConstants.h"
#import "NSNumber+HumanReadable.h"
#import "MDMUtilities.h"
#import "ProgressDialog.h"
@interface TCSMDMAppsViewController () <ProgressDialogDelegateProtocol>
@property (strong) NSMutableArray *appsArray;
@property (strong) id <UpdateStatus> progressViewController;
@property (strong) IBOutlet NSArrayController *appsArrayController;
@end

@implementation TCSMDMAppsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.appsArray=[NSMutableArray array];
    [self setupApps:self];
}
- (IBAction)addAppButtonPressed:(id)sender {

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.allowsOtherFileTypes=YES;
    openPanel.allowsMultipleSelection=YES;
    openPanel.allowedFileTypes=@[@"pkg",@"mpkg",@"app"];
    openPanel.canChooseDirectories=YES;
    openPanel.message=@"Select a package or app to add:";
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {
        return;
    }
    NSArray <NSURL *> *urlArray=openPanel.URLs;


    NSFileManager *fm=[NSFileManager defaultManager];

    NSMutableArray *appsArrayWithFullPaths=[NSMutableArray array];
    BOOL isDir;
    if (urlArray.count==1 && [[urlArray.firstObject.path.pathExtension lowercaseString] isEqualToString:@"app"]==NO && [fm fileExistsAtPath:urlArray.firstObject.path isDirectory:&isDir] && isDir==YES){

        NSError *err;
        NSArray *items=[fm contentsOfDirectoryAtPath:urlArray.firstObject.path error:&err];
        if (!items){
            [[NSAlert alertWithError:err] runModal];
            return;
        }


        [items enumerateObjectsUsingBlock:^(NSString *currItem, NSUInteger idx, BOOL * _Nonnull stop) {
            [appsArrayWithFullPaths addObject:[urlArray.firstObject.path stringByAppendingPathComponent:currItem]];
        }];
    }
    else {
        [urlArray enumerateObjectsUsingBlock:^(NSURL *currItem, NSUInteger idx, BOOL * _Nonnull stop) {
            [appsArrayWithFullPaths addObject:currItem.path];
        }];

    }
    [self performSegueWithIdentifier:@"updateProgressSegue2" sender:self];

    [[MDMUtilities sharedUtilities] setupApps:[NSArray arrayWithArray:appsArrayWithFullPaths] statusBlock:^(NSString * _Nonnull status, float percentComplete) {

        [self.progressViewController percentCompleted:percentComplete];
        [self.progressViewController updateStatusText:status];

        NSLog(@"%@",status);
    } completeBlock:^(NSArray * _Nonnull manifestArray) {
        [self setupApps:self];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewController:(NSViewController*) self.progressViewController];
        });
        
    }];
    
    
    
}
- (IBAction)removeAppButtonPressed:(id)sender {

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Delete Apps Confirmation";
    alert.informativeText=@"Are you sure you want delete all the packages from the repository? This will remove the packages and associated manifests.";

    [alert addButtonWithTitle:@"Delete"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    NSArray *appArray=[self.appsArrayController selectedObjects];
    NSMutableArray *appsToRemove=[NSMutableArray array];

    [appArray enumerateObjectsUsingBlock:^(NSDictionary *currentApp, NSUInteger idx, BOOL * _Nonnull stop) {
        [appsToRemove addObject:currentApp[@"appName"]];
    }];
    [[MDMUtilities sharedUtilities] removeMDMRepositoryApps:[NSArray arrayWithArray:appsToRemove] completeBlock:^(NSError * _Nonnull err) {

        if (err) {

            [[NSAlert alertWithError:err] runModal];
        }
    }];
    [self setupApps:self];
}
-(void)setupApps:(id)sender{

    NSString *repoResolvedPath=[[[NSUserDefaults standardUserDefaults] objectForKey:MDMSERVERFILEREPOPATH] stringByExpandingTildeInPath];

    NSFileManager *fm=[NSFileManager defaultManager];

    NSError *err;
    NSArray <NSString *> *dir;
    if((dir=[fm contentsOfDirectoryAtPath:repoResolvedPath error:&err])==nil){

        [[NSAlert alertWithError:err] runModal];
        return;

    }

    NSMutableArray *appsArray=[NSMutableArray array];

    [dir enumerateObjectsUsingBlock:^(NSString * name, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[[name pathExtension] lowercaseString] isEqualToString:@"pkg"]) {

            NSString *currFilePath=[repoResolvedPath stringByAppendingPathComponent:name];
            NSError *err;
            NSDictionary *fileAttributes=[fm attributesOfItemAtPath:currFilePath error:&err];
            if(!fileAttributes) {
                [[NSAlert alertWithError:err] runModal];
                return;
            }
            NSNumber *size=fileAttributes[NSFileSize];
            [appsArray addObject:@{@"appName":name,@"size":[size stringWithHumanReadableInt]}];
        }


    }];

    [self.appsArrayController removeObjects:[self.appsArrayController arrangedObjects]];
    [self.appsArrayController addObjects:appsArray];
    
}


- (IBAction)installAppsButtonPressed:(id)sender {
    NSArray *selectedObjects=[self.appsArrayController selectedObjects];

    if (selectedObjects.count>0) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Application Install Confirmation";
        alert.informativeText=[NSString stringWithFormat:@"Are you sure you want to install %li applications on %li devices?",selectedObjects.count,self.devices.count];

        [alert addButtonWithTitle:@"OK"];
        [alert addButtonWithTitle:@"Cancel"];
        NSInteger res=[alert runModal];
        if (res==NSAlertSecondButtonReturn) return;

        NSMutableArray *appNameArray=[NSMutableArray array];

        [selectedObjects enumerateObjectsUsingBlock:^(NSDictionary *appInfo, NSUInteger idx, BOOL * _Nonnull stop) {
            [appNameArray addObject:appInfo[@"appName"]];

        }];
        [self.delegate installApps:[NSArray arrayWithArray:appNameArray] toDevices:self.devices];
        [self dismissController:self];

    }

}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"updateProgressSegue2"]){

        ProgressDialog *dialogViewController=segue.destinationController;
        dialogViewController.delegate=self;
        self.progressViewController=dialogViewController;
    }
    else {
        [super prepareForSegue:segue sender:sender];

    }

}

- (void)cancelUpdate:(nonnull id)sender {
    [MDMUtilities sharedUtilities].shouldCancel=YES;
}

@end
