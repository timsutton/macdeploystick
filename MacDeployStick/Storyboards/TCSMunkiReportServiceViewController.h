//
//  TCSMunkiReportViewController.h
//  MDS_appstore
//
//  Created by Timothy Perfitt on 3/13/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSWebServerSettingViewController.h"
#import "TCSServiceViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSMunkiReportServiceViewController : TCSServiceViewController <TCSWebServiceConfigurationDelegateProtocol,TCSServiceViewProtocol>

@end

NS_ASSUME_NONNULL_END
