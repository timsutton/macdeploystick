//
//  TCSMunkiReportViewController.m
//  MDS_appstore
//
//  Created by Timothy Perfitt on 3/13/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMunkiReportServiceViewController.h"
#import "TCSUtility.h"
#import "TCSWebServerSettingViewController.h"
#import "TCSWebServiceController.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSMunkiReportStatusViewController.h"
#import "MDSPrivHelperToolController.h"
#import "NSNumber+HumanReadable.h"
#import "TCSConstants.h"
#import "TCSMunkiReportEditUserViewController.h"
#import "TCSServiceUpdateManager.h"
@interface TCSMunkiReportServiceViewController () <TCSMunkiReportFeedbackProtocol,TCSMunkiReportUserCreatedProtocol>
@property (weak) IBOutlet NSTabView *munkiReportMainTabView;
@property (assign) NSInteger editingIndex;
@property (strong) TCTaskWrapperWithBlocks *updateDatabaseTaskWrapper;
@property (strong) NSMutableDictionary *statusDictionary;
@property (strong) NSString *updateInfo;
@property (strong) NSArray *users;
@property (strong) IBOutlet NSArrayController *userArrayController;
//dbSize
@property (strong) NSString *munkiReportURLString;
@property (strong) NSString *dbSize;
@property (strong) NSString *installedVersion;


@end
@implementation TCSMunkiReportServiceViewController
@synthesize isRunning;
@synthesize isUpdateAvailable;
@synthesize badgeCount;
@synthesize badgeType;
@synthesize errorMessage;
@synthesize hasError;
@synthesize isInstalled;
@synthesize logFilePath;
@synthesize statusMode;
@synthesize statusType;
@synthesize serviceButtonState;
@synthesize serviceStatus;


- (void)awakeFromNib {
    [self updateState:self];
    self.users=[NSMutableArray array];
    [self updateUsers:self];
    [self updateServiceStatus:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serviceUpdateAvailable:) name:TCSNOTIFICATIONUPDATEAVAILABLE object:nil];
    [[TCSServiceUpdateManager updateManager] checkForMunkiReportUpdate:self];

    if ([self isInstalled]==NO){
        [self.munkiReportMainTabView selectTabViewItemAtIndex:1];
        
    }

}
//-(IBAction)showLogButtonPressed:(id)sender{
////    [[NSWorkspace sharedWorkspace] openFile:@"/var/log/apache2/access_log" withApplication:@"Console"];
////    [[NSWorkspace sharedWorkspace] openFile:@"/var/log/apache2/error_log" withApplication:@"Console"];
//
//}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (IBAction)munkiReportServiceStateButtonPressed:(NSButton *)sender {

    if (sender.state==NSControlStateValueOn){


        [self startService:self];
    }
    else{
        [self stopService:self];
    }

}
- (IBAction)removeUser:(id)sender {
    if (self.isAuthorized==NO) return;
    NSArray <NSDictionary *> *selectedUsers=[self.userArrayController selectedObjects];

    if (selectedUsers.count==0) return;
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Delete Users?";
    alert.informativeText=[NSString stringWithFormat:@"Are you sure you want to delete the selected %li users?",selectedUsers.count];

    [alert addButtonWithTitle:@"Delete"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;


    NSMutableArray *usernames=[NSMutableArray array];
    [selectedUsers enumerateObjectsUsingBlock:^(NSDictionary *currUser, NSUInteger idx, BOOL * _Nonnull stop) {

        if (currUser[@"username"]) [usernames addObject:currUser[@"username"]];
    }];
    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];
    NSString *userFolder=[sharedFolder stringByAppendingPathComponent:@"local/users"];

    [[MDSPrivHelperToolController sharedHelper] removeUsers:usernames fromFolder:userFolder callback:^(NSError * _Nonnull err) {

        if (err){

            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSAlert alertWithError:err] runModal];
            });
        }
        else {

            [self updateUsers:self];
        }
    }];



}

-(void)updateUsers:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{

        [self.userArrayController removeObjects:[self.userArrayController arrangedObjects]];
        NSMutableArray *userArray=[NSMutableArray array];
        NSFileManager *fm=[NSFileManager defaultManager];

        NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];
        NSString *userFolder=[sharedFolder stringByAppendingPathComponent:@"local/users"];


        if ([fm fileExistsAtPath:userFolder]==NO) { return; }
        NSError *err;
        NSArray *contentsOfUserFolder=[fm contentsOfDirectoryAtPath:userFolder error:&err];

        if (err){
            [[NSAlert alertWithError:err] runModal];
            return;
        }

        [contentsOfUserFolder enumerateObjectsUsingBlock:^(NSString *filename, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([filename.pathExtension isEqualToString:@"yml"]) {
                [userArray addObject:@{@"username":[filename stringByDeletingPathExtension]}];
            }
        }];


        [self.userArrayController addObjects:[NSArray arrayWithArray:userArray]];
    });

}
-(void)serviceUpdateAvailable:(NSNotification *)not{
    [self updateState:self];

}
- (IBAction)refreshButtonPressed:(id)sender {
    self.serviceStatus=@"Checking for Updates";
    [[TCSServiceUpdateManager updateManager] checkForMunkiReportUpdate:self];
    [self updateState:self];
    [self performSelector:@selector(updateMunkiReportServiceStatus:) withObject:self afterDelay:1];

}
- (IBAction)openMunkiReportDownloadWebsiteButtonPressed:(id)sender {
    NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"downloadMunkiReport"]];
    if (url) [[NSWorkspace sharedWorkspace] openURL:url];

}
-(IBAction)openMunkiReportButtonPressed:(id)sender{
    [self openMunkiReportWebPage:self];
}
-(void)updateMunkiReportServiceStatus:(id)sender{

    if ([self isInstalled]==NO){

        self.serviceStatus=@"Not Installed. Select Import Munki Files under Setup to install.";
        return;
    }

    if ([self isActive]==NO){

        self.serviceStatus=@"Not Active";
        return;

    }
    BOOL isRunning=[[TCSWebServiceController sharedController] isRunning];

    if (isRunning==YES) {
        self.serviceStatus=@"Running";
        return;
    }
    else {

        self.serviceStatus=@"Web Services Stopped";
        return;
    }

}
-(BOOL)isInstalled{
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];

    NSString *publicFolder=[sharedFolder stringByAppendingPathComponent:@"public"];
    return [fm fileExistsAtPath:publicFolder];
}
-(BOOL)isWebServerRunning{
    return [[TCSWebServiceController sharedController] isRunning];

}
-(BOOL)isActive{

    TCSWebserverSetting *munkiReportSetting=[[TCSWebServiceController sharedController]  munkiReportWebserverSettings];
    if (!munkiReportSetting) return NO;
    return munkiReportSetting.isActive;

}
-(void)stopService:(id)sender{
    
    if (self.isAuthorized==NO) return;
    if ([self isActive]==YES){
        TCSWebserverSetting *currSettings=[[TCSWebServiceController sharedController]  munkiReportWebserverSettings];
        currSettings.isActive=NO;
        [self updateConfiguration:currSettings];
    }
    [self updateServiceStatus:self];
    [self updateSidebar:self];

}
-(void)startService:(id)sender{

    if (self.isAuthorized==NO) return;
    if ([self isActive]==NO){

        if ([self isInstalled]==NO){

            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"MunkiReport Not Installed";
            alert.informativeText=@"MunkiReport has not been installed yet. Install it by selecting Importing MunkiReport Files in Setup.";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
            [self updateServiceStatus:self];

            return;
        }

        TCSWebserverSetting *currSettings=[[TCSWebServiceController sharedController]  munkiReportWebserverSettings];
                if (!currSettings){
            if ([[TCSWebServiceController sharedController] hasTLSCertificates:self]==NO){

                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"No SSL Certificates Defined";
                alert.informativeText=@"There are no SSL certificates defined for this server. It is recommended that you create or import an SSL in Security under Preferences.";

                [alert addButtonWithTitle:@"Cancel"];
                [alert addButtonWithTitle:@"Continue"];
                NSInteger res=[alert runModal];
                if (res==NSAlertFirstButtonReturn)
                {
                    [self updateServiceStatus:self];
                    return;

                }

            }
            currSettings=[self defaultMunkiReportSettings];
        }
        currSettings.isActive=YES;
        [self updateConfiguration:currSettings];
    }
    else {

        if ([self isWebServerRunning]==NO){
            [[TCSWebServiceController sharedController] startWebServicesWithCallback:^(BOOL success, BOOL didCancel) {
                dispatch_async(dispatch_get_main_queue(), ^{

                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Web Service Status";
                    alert.informativeText=@"The webserver is now coming up. Please wait a minute for MunkiReport to be available.";

                    [alert addButtonWithTitle:@"Open MunkiReport"];
                    [alert addButtonWithTitle:@"Close"];
                    NSInteger res=[alert runModal];
                    if (res==NSAlertSecondButtonReturn) return;

                    [self openMunkiReportWebPage:self];
                });

            }];


        }

    }
    self.serviceStatus=@"Updating...";
    [self updateSidebar:self];

    [self performSelector:@selector(updateServiceStatus:) withObject:self afterDelay:1];

}

-(void)updateState:(id)sender{
    self.isUpdateAvailable=NO;
    TCSServiceUpdateManager *updateManager=[TCSServiceUpdateManager updateManager];

    NSDictionary *updateInfo=[[updateManager updatesAvailable] objectForKey:@"munkireport"];
    if (updateInfo){
        NSString *latestVersion=[updateInfo objectForKey:@"latest_version"];
        self.installedVersion=[updateInfo objectForKey:@"installed_version"];


        self.installedVersion=[updateInfo objectForKey:@"installed_version"];
        if (latestVersion){
            if ([self.installedVersion hasPrefix:latestVersion]==NO) {

                self.isUpdateAvailable=YES;
                self.updateInfo=[NSString stringWithFormat:@"Update Available! Installed version: %@, New Version: %@",self.installedVersion,latestVersion];
                self.isUpdateAvailable=YES;
            }
        }
    }
    else{
        self.isUpdateAvailable=NO;
    }
}

-(void)updateDBSize{
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];

    NSString *dbFile=[sharedFolder stringByAppendingPathComponent:@"app/db/db.sqlite"];
    if ([fm fileExistsAtPath:dbFile]){
        NSError *err;
        NSDictionary *fileAttributes=[fm attributesOfItemAtPath:dbFile error:&err];
        if(!fileAttributes) {
            self.dbSize=@"Unknown";
            return;
        }
        NSNumber *size=fileAttributes[NSFileSize];

        if ([size intValue]==0) {
            self.dbSize=@"Waiting for migration";
            return;
        }

        self.dbSize=[size stringWithHumanReadableInt];
        return;

    }
    self.dbSize=@"Unknown";

}

- (void)extracted:(NSString *)destinationInTempLocation {

    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];

    [[MDSPrivHelperToolController sharedHelper] setupMunkiReportWithSource:destinationInTempLocation destination:sharedFolder withCallback:^(NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{

            if (err){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSAlert alertWithError:err] runModal];
                    return;
                });

            }
            else {
                TCSWebserverSetting *munkiReportSetting=[[TCSWebServiceController sharedController] munkiReportWebserverSettings];
                
                if (!munkiReportSetting) {
                    munkiReportSetting=[self defaultMunkiReportSettings];
                    self.editingIndex=-1;
                }
                else {
                    munkiReportSetting.path=[sharedFolder stringByAppendingPathComponent:@"public"];
                    munkiReportSetting.enablePHP7=YES;
                    munkiReportSetting.isActive=YES;

                }
                [self updateConfiguration:munkiReportSetting];

//                [[TCSWebServiceController sharedController] startWebServicesWithCallback:^(BOOL success, BOOL didCancel) {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//
//                       NSAlert *alert=[[NSAlert alloc] init];
//                           alert.messageText=@"Web Service Status";
//                           alert.informativeText=@"The webserver is now coming up. Please wait a minute for MunkiReport to be available.";
//
//                           [alert addButtonWithTitle:@"Open MunkiReport"];
//                           [alert addButtonWithTitle:@"Close"];
//                           NSInteger res=[alert runModal];
//                           if (res==NSAlertSecondButtonReturn) return;
//
//                           [self openMunkiReportWebPage:self];
//                       });
//
//                }];
                [self startService:self];


            }
        });
    }];
}
-(TCSWebserverSetting *)defaultMunkiReportSettings{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *certPath=[ud objectForKey:CERTIFICATEPATH];
    NSString *keyPath=[ud objectForKey:INDENTITYKEYPATH];
    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];

    TCSWebserverSetting *munkiReportSetting=[[TCSWebserverSetting alloc] init];
    munkiReportSetting.path=[sharedFolder stringByAppendingPathComponent:@"public"];
    munkiReportSetting.port=[[ud objectForKey:@"munkiReportServerPort"] intValue];

    if (munkiReportSetting.port<=0) munkiReportSetting.port=8881;
    if (certPath && keyPath){
        munkiReportSetting.useTLS=YES;
    }
    else {
        munkiReportSetting.useTLS=NO;
    }
    munkiReportSetting.useForMunkiReport=YES;
    self.editingIndex=-1;
    munkiReportSetting.isActive=YES;
    munkiReportSetting.enablePHP7=YES;
    return munkiReportSetting;

}

-(void)updateServiceStatus:(id)sender{
    [self updateMunkiReportURLString];
    [self updateDBSize];
    [self updateMunkiReportServiceStatus:self];

    self.isRunning=[self isInstalled] && [self isWebServerRunning] && [self isActive];


    if ([self isRunning]==YES){
        self.serviceButtonState=NSControlStateValueOn;
    }
    else {
        self.serviceButtonState=NSControlStateValueOff;
    }

}

-(void)updateMunkiReportURLString{
    NSArray *webSettings=[[TCSWebServiceController sharedController] webserviceSettings];

    __block TCSWebserverSetting *munkiReportSetting;
    self.editingIndex=-1;
    [webSettings enumerateObjectsUsingBlock:^(TCSWebserverSetting *setting, NSUInteger idx, BOOL * _Nonnull stop) {

        if (setting.useForMunkiReport==YES){
            self.editingIndex=idx;
            munkiReportSetting=setting;
            *stop=YES;
        }
    }];


    NSString *prefix=munkiReportSetting.useTLS?@"https":@"http";
    NSString *hostname=[USERDEFAULTS objectForKey:SERVERHOSTNAME];
    NSInteger port=munkiReportSetting.port;

    NSString *urlString=[NSString stringWithFormat:@"%@://%@:%li",prefix,hostname,port];

    self.munkiReportURLString=urlString;
}
-(void)openMunkiReportWebPage:(id)sender{
    [self updateMunkiReportURLString];
    NSURL *url=[NSURL URLWithString:[self munkiReportURLString]];
    [[NSWorkspace sharedWorkspace]  openURL:url];

}
-(void)importMunkiReport:(id)sender{
    if (self.isAuthorized==NO) return;

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseDirectories=YES;

    openPanel.message=@"Select a folder that contains the MunkiReport App files:";
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {
        return;
    }
    NSURL *openURL=openPanel.URL;

    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:[openURL.path stringByAppendingPathComponent:@"public/index.php"]]==YES){
        NSString *tempPath=[NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
        NSError *err;

        if([fm createDirectoryAtPath:tempPath withIntermediateDirectories:YES attributes:nil error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return;
        }
        NSString *destinationInTempLocation=[tempPath stringByAppendingPathComponent:openURL.path.lastPathComponent];
        if([fm copyItemAtPath:openURL.path toPath:destinationInTempLocation error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return;

        }
        [self extracted:destinationInTempLocation];

    }
    else {

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Invalid MunkiReport folder";
        alert.informativeText=@"The MunkiReport folder does not contain the correct files. Please verify and try again.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
    }

}
- (IBAction)importMunkiReportButtonPressed:(id)sender {

    if ([[TCSWebServiceController sharedController] isRunning]==YES){

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Web Service Running Error";
        alert.informativeText=@"MunkiReport cannot be imported while the web service is running. Do you want to stop the web service and continue?";

        [alert addButtonWithTitle:@"Continue"];
        [alert addButtonWithTitle:@"Close"];

        NSInteger res=[alert runModal];

        if (res==NSAlertSecondButtonReturn) return;
        [[MDSPrivHelperToolController sharedHelper] stopWebserverWithCallback:^(BOOL success) {

            dispatch_async(dispatch_get_main_queue(), ^{
                [self importMunkiReport:self];

            });
        }];
    }
    else {
        [self importMunkiReport:self];

    }

}

- (IBAction)updateDatabaseButtonPressed:(id)sender {
    self.statusDictionary=[@{@"message":@"",@"buttonTitle":@"Cancel"} mutableCopy];

    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];

    NSString *migrateScript=[sharedFolder stringByAppendingPathComponent:@"database/migrate.php"];
    if ([fm fileExistsAtPath:@"/usr/bin/php"]==YES && [fm fileExistsAtPath:migrateScript]==YES){

        self.updateDatabaseTaskWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{
            NSLog(@"starting MunkiReport database migration");
            [self.statusDictionary setObject:[[self.statusDictionary objectForKey:@"message"] stringByAppendingString:@"starting MunkiReport database migration.\n"] forKey:@"message"];

        } endBlock:^{
            NSLog(@"MunkiReport database migration complete.\n");
            dispatch_async(dispatch_get_main_queue(), ^{

                [self.statusDictionary setObject:[[self.statusDictionary objectForKey:@"message"] stringByAppendingString:@"MunkiReport database migration complete."] forKey:@"message"];
                [self.statusDictionary setObject:@"Done" forKey:@"buttonTitle"];

            });

        } outputBlock:^(NSString *output) {
            NSLog(@"%@",output);
            dispatch_async(dispatch_get_main_queue(), ^{

                [self.statusDictionary setObject:[[self.statusDictionary objectForKey:@"message"] stringByAppendingString:output] forKey:@"message"];

            });
        } errorOutputBlock:^(NSString *errorOutput) {
            NSLog(@"Munki Report Error: %@",errorOutput);

        } arguments:@[@"/usr/bin/php",migrateScript]];
        [self performSegueWithIdentifier:@"showProgressSegue" sender:self];
        [self.updateDatabaseTaskWrapper startProcess];

    }
    else {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"File not found error";
        alert.informativeText=[NSString stringWithFormat:@"There was an error finding the files to migrate. Please verify /usb/bin/php and %@ exist.",migrateScript];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];


    }
}
- (IBAction)backupFilesButtonPressed:(id)sender {
}
- (IBAction)restoreSettingsButtonPressed:(id)sender {
}
- (IBAction)showMunkiReportShareFoldeButtonPressed:(id)sender {
    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];

    if (sharedFolder){
        [[NSWorkspace sharedWorkspace] openFile:sharedFolder];
    }
    else {
        NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"downloadMunkiReport"]];
        if (url) [[NSWorkspace sharedWorkspace] openURL:url];

    }


}
//- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{
//    TCSWebServerSettingViewController *dest=segue.destinationController;
//    self.editingIndex=-1;
//    dest.delegate=self;
//    NSArray *webSettings=[[TCSWebServiceController sharedController] webserviceSettings];
//
//    __block TCSWebserverSetting *munkiReportSetting;
//    [webSettings enumerateObjectsUsingBlock:^(TCSWebserverSetting *setting, NSUInteger idx, BOOL * _Nonnull stop) {
//
//        if (setting.useForMunkiReport==YES){
//            self.editingIndex=idx;
//            munkiReportSetting=setting;
//            *stop=YES;
//        }
//    }];
//    if (munkiReportSetting) dest.representedObject=munkiReportSetting;
//
//
//}
- (BOOL)updateConfiguration:(nonnull TCSWebserverSetting *)newSettings {
    NSMutableArray *webSettings=[[[TCSWebServiceController sharedController] webserviceSettings] mutableCopy];

    if(self.editingIndex==-1){
        [webSettings enumerateObjectsUsingBlock:^(TCSWebserverSetting *currSetting, NSUInteger idx, BOOL * _Nonnull stop) {

            currSetting.useForMunkiReport=NO;
        }];
        [webSettings addObject:newSettings];

    }
    else {
        [webSettings replaceObjectAtIndex:self.editingIndex withObject:newSettings];
    }
    [[TCSWebServiceController sharedController] saveSettings:webSettings callback:^(BOOL hadSuccess) {
        if (hadSuccess==NO) {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Web Server Error";
            alert.informativeText=@"There was an error updating settings and restarting the web service. Please check the webserver log";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];

        }

    }];
    return YES;

}




- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"addUserSegue"]){

        TCSMunkiReportEditUserViewController *vc=segue.destinationController;
        vc.delegate=self;

    }
    else {
        TCSMunkiReportStatusViewController *controller=segue.destinationController;
        controller.delegate=self;
        controller.representedObject=self.statusDictionary;

    }
}

- (void)cancelProcess:(nonnull id)sender {
    if ([self.updateDatabaseTaskWrapper isRunning]==YES){
        [self.updateDatabaseTaskWrapper stopProcess];
        [self.statusDictionary setObject:@"Done" forKey:@"buttonTitle"];

    }
    else {


        [self dismissViewController:self.presentedViewControllers.firstObject];
    }

}


- (void)userCreated:(nonnull NSDictionary *)user {

    NSString *ePassword=[TCSUtility encryptPassword:user[@"password"]];


    NSString *line=[NSString stringWithFormat:@"password_hash: %@",ePassword];

    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];
    NSString *userFolder=[sharedFolder stringByAppendingPathComponent:@"local/users"];

    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:userFolder]==NO) { return; }

    NSString *path=[[userFolder stringByAppendingPathComponent:user[@"username"]] stringByAppendingPathExtension:@"yml"];

    [[MDSPrivHelperToolController sharedHelper] addMunkiReportUserFile:path contents:line withCallback:^(NSError * _Nonnull err) {
        if (err){

            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSAlert alertWithError:err] runModal];
                return;
            });

        }
        [self updateUsers:self];

    }];
}


@end
