//
//  TCSServiceNode.h
//  MDS
//
//  Created by Timothy Perfitt on 3/7/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSServiceNode : NSObject

@property (strong) NSArray <TCSServiceNode *> *children;
@property (strong) NSString *name;
@property (strong) NSString *icon;
@property (strong) NSString *storyboardName;
@property (strong) NSString *storyboardID;
@property (nullable, assign) NSImage *statusImage;
@property (assign) BOOL hasBadge;
@property (assign) BOOL hasStatusIcon;


-(BOOL)isLeaf;
@end

NS_ASSUME_NONNULL_END
