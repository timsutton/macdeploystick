//
//  TCSDeleteProfilesViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 2/25/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSDeleteProfilesViewController : NSViewController
@property (strong) NSArray *profiles;
-(void)deleteProfiles:(NSArray *)devices;
@end

NS_ASSUME_NONNULL_END
