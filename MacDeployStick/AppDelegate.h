//
//  AppDelegate.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#ifndef APPSTORE
#import <Paddle/Paddle.h>
#endif

#ifdef APPSTORE
@interface AppDelegate : NSObject <NSApplicationDelegate,NSFileManagerDelegate>
#else
@interface AppDelegate : NSObject <NSApplicationDelegate,PaddleDelegate,NSFileManagerDelegate>



#endif
-(void)showPaddleInfo;
-(BOOL)resourcesNeeded;

@end

