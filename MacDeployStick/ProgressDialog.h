//
//  ProgressDialog.h
//  MDS
//
//  Created by Timothy Perfitt on 9/9/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ProgressDialogDelegateProtocol <NSObject>

-(void)cancelUpdate:(id)sender;

@end

@protocol UpdateStatus <NSObject>
-(void)percentCompleted:(float)percentCompleted;
-(void)updateStatusText:(NSString *)statusText;
-(BOOL)shouldCancel;
@end

@interface ProgressDialog : NSViewController <UpdateStatus>
@property (weak) id <ProgressDialogDelegateProtocol> delegate;
-(IBAction)cancelUpdateResources:(id)sender;

@end

NS_ASSUME_NONNULL_END
