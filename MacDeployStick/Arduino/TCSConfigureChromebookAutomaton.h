//
//  TCSCreateAutobot.h
//  MDS
//
//  Created by Timothy Perfitt on 5/8/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSConfigureAutomaton.h"

NS_ASSUME_NONNULL_BEGIN

@interface TCSConfigureChromebookAutomaton : TCSConfigureAutomaton

@end

NS_ASSUME_NONNULL_END
