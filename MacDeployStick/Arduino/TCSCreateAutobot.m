//
//  TCSCreateAutobot.m
//  MDS
//
//  Created by Timothy Perfitt on 5/8/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSCreateAutobot.h"

@interface TCSCreateAutobot ()

@end

@implementation TCSCreateAutobot

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

-(NSString *)firmwareName{
    return @"Chromebook.ino.itsybitsy32u4";
}

@end
