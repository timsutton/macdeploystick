//
//  TCSConfigureAutomaton.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/31/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//
#include <sys/select.h>
#import "TCSConfigureAutomaton.h"
#include <stdio.h>

extern int select(int, fd_set * __restrict, fd_set * __restrict,
                  fd_set * __restrict, struct timeval * __restrict);

struct settings_t
{

    char command[255];
    char firmware_password[32];
    uint32 startup_delay;
    uint32 pre_command_delay;
    bool autorun;
    bool erase_volume;
    uint16 version;
    uint16 recovery_mode;
    bool language_english;

} __attribute__((packed));

@interface TCSConfigureAutomaton ()
typedef NS_ENUM(NSUInteger, SERIALMODE) {
    VERSION,
    VERSION_RESPONSE,
    COMMAND,
    VERIFY,
    IDLE

};

@property (strong) NSTimer *checkForArduinoTimer;
@property (assign) int filedesc;
@property (strong) NSTimer *serialInputTimer;
@property (strong) NSString *leftoverString;
@property (assign) SERIALMODE mode;
@property (strong) id serialNotification;
@property (strong) NSString *command;
@property (strong) NSString *firmwarePassword;
@property (assign) BOOL hasFirmwarePassword;

@property (assign) BOOL shouldAutorun;
@property (assign) BOOL shouldEraseWithDiskUtility;
@property (assign) BOOL shouldSetLanguageToEnglish;

@property (assign) NSInteger startupDelay;
@property (assign) NSInteger preCommandDelay;
@property (assign) NSInteger serialInputTries;
@property (assign) NSInteger version;
@property (assign) NSInteger recoveryMode;


@end

@implementation TCSConfigureAutomaton
struct settings_t *new_settings;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setPreferredContentSize:self.view.frame.size];

}
- (IBAction)firmwareCheckboxChanged:(id)sender {

    if (self.hasFirmwarePassword==NO){
        self.firmwarePassword=@"";
    }
}



-(void)readSettings{
    ssize_t n = write(self.filedesc, "show\r\n",6 );
    if (n < 0)
        fputs("write() of 8 bytes failed!\n", stderr);

    self.mode=VERSION_RESPONSE;
    [self monitorSerialForInput];


}
-(void)viewDidAppear{


    self.serialNotification=[[NSNotificationCenter defaultCenter] addObserverForName:@"TCSSerialDataNotFound" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Timeout Error";
        alert.informativeText=@"No data was received. Please try again.";

        [alert addButtonWithTitle:@"OK"];

        [alert runModal];


    }];

    self.dataLoaded=NO;
    self.serialNotification=[[NSNotificationCenter defaultCenter] addObserverForName:@"TCSSerialDataLine" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {

        switch (self.mode) {
            case VERSION:{

                [self.serialInputTimer invalidate];
                [self readSettings];
                break;
            }

            case VERSION_RESPONSE:
                [self processCommand:[note.userInfo objectForKey:@"line"]];
                break;
                
            case COMMAND:
                [self processCommand:[note.userInfo objectForKey:@"line"]];
                break;
            case VERIFY:
                [self processCommand:[note.userInfo objectForKey:@"line"]];
            case IDLE:
                break;
        }


    }];

    [self kickOffCheckForArduinoTimer];

}

-(void)kickOffCheckForArduinoTimer{
    self.checkForArduinoTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSFileManager *fm=[NSFileManager defaultManager];

        NSError *err;
        NSArray *devices=[fm contentsOfDirectoryAtPath:@"/dev" error:&err];

        if (!devices) {
            NSLog(@"no devices!");
            return;
        }
        __block int count=0;
        __block NSString *devicePath=nil;

        [devices enumerateObjectsUsingBlock:^(NSString *currDeviceName, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *currDevicePath=nil;

            currDevicePath=[@"/dev" stringByAppendingPathComponent:currDeviceName];

            if ([currDevicePath containsString:@"cu.usbmodem"]) {
                devicePath=currDevicePath;
                count++;
            }

        }];
        if (count>1) {
            NSAlert *alert=[[NSAlert alloc] init];
            //            alert.mess
            alert.messageText=@"more than 1 device found. app can only configure one device at a time";
            [alert runModal];
            close(self.filedesc);
            [self dismissController:self];

            return;
        }
        if (count==1) {
            [self.checkForArduinoTimer invalidate];
            self.checkForArduinoTimer=nil;
            [self connect:devicePath];
        }


    }];

}
-(NSInteger)automatonVersion{
    NSBundle *mainBundle=[NSBundle mainBundle];

        NSInteger automatonVersion=[[[mainBundle infoDictionary] objectForKey:@"AutomatonFirmwareVersion"] integerValue];
    return automatonVersion;
}
-(BOOL)isFirmwareUpdateAvailable{
    NSInteger automatonVersion=[self automatonVersion];
    if (self.version<automatonVersion) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Automaton Firmware Update Available";
        alert.informativeText=@"There is a updated version of the firmware for the current automaton. Please update the firmware by selecting Create Automaton and re-programming the device.";
        close(self.filedesc);
        [alert runModal];
        [self dismissController:self];
        return YES;
    }
    else if (self.version>automatonVersion){
        NSAlert *alert=[[NSAlert alloc] init];

        alert.messageText=@"Automaton Firmware Mismatch";
        alert.informativeText=[NSString stringWithFormat:@"The currently connected Automaton has a newer firmware version than is supported by this version of MDS. Please update MDS."];
        close(self.filedesc);
        [alert runModal];
        [self dismissController:self];
        return YES;
    }
    else return NO;
}
-(void)processCommand:(NSString *)inCommand{

    NSMutableString *workingString=[inCommand mutableCopy];
    if ([inCommand hasPrefix:@">"]){
        [workingString deleteCharactersInRange:NSMakeRange(0, 1)];
    }


    if ([workingString hasPrefix:@"DATA:"] && [self settingsSize]*2+5==workingString.length){
        const char *source_val=[workingString substringFromIndex:5].UTF8String;
        uint8 *settings_ptr=(void *)[self settings];
        NSInteger size= [self settingsSize];

        for(int i = 0;i<size; i++){
            sscanf(&source_val[i*2],"%02x",(int *)&settings_ptr[i]);
        }

        [self.serialInputTimer invalidate];
        self.serialInputTimer=nil;
        //firmware password is not returned, just a flag set in the struct. Simplfies marshalling.


        if (self.mode==VERIFY){
            struct settings_t *ptr=(struct settings_t *)settings_ptr;
            memset(new_settings->firmware_password,0,sizeof(new_settings->firmware_password));
            memset(ptr->firmware_password,0,sizeof(ptr->firmware_password));

            if(memcmp(new_settings, settings_ptr, size)!=0){
                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Write Error";
                alert.informativeText=@"There was a problem writing the data. Please try again.";

                [alert addButtonWithTitle:@"OK"];
                [alert runModal];
            }
            else {

                [self completSendSettingsWithSuccess:YES];
            }
    }
        else [self updateWithSettings:settings_ptr];

    }
    else if ([workingString hasPrefix:@"Version: "]){

        [self.serialInputTimer invalidate];

        self.version=[[[workingString componentsSeparatedByString:@": "] lastObject] integerValue];
        if([self isFirmwareUpdateAvailable]==YES) return;
        self.mode=COMMAND;

        char get_settings[]="get_settings\r\n";
        ssize_t n = write(self.filedesc, get_settings, sizeof(get_settings));
        if (n < 0)
            fputs("write() failed!\n", stderr);
        [self monitorSerialForInput];

    }


}
-(void)updateWithSettings:(void *)inSettings{

    
    struct settings_t *settings=(struct settings_t *)inSettings;

    if (settings->firmware_password[0]==0x01) self.hasFirmwarePassword=YES;

    self.startupDelay=settings->startup_delay;
    self.preCommandDelay=settings->pre_command_delay;
    self.command=[NSString stringWithUTF8String:settings->command];
    self.shouldAutorun=settings->autorun;
    self.shouldEraseWithDiskUtility=settings->erase_volume;
    self.shouldSetLanguageToEnglish=settings->language_english;
    self.recoveryMode=settings->recovery_mode;
    self.version=settings->version;
    self.dataLoaded=YES;

}
-(void)viewWillDisappear{
    [self.checkForArduinoTimer invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self.serialNotification];
}
-(void)connect:(NSString *)inDevicePath{

    self.filedesc = open( inDevicePath.UTF8String, O_RDWR );

    if (self.filedesc<0) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Connection Error";
        alert.informativeText=@"The connection could not be opened to the arduino. Please make sure no other processes are connected.";
        [alert runModal];
        [self dismissController:self];
        return;

    }
    self.mode=VERSION;

    ssize_t n = write(self.filedesc, "\r\n\r\n", 4);
    if (n < 0) {
        fputs("write() of 2 bytes failed!\n", stderr);
    }

    [self monitorSerialForInput];

}
-(void)monitorSerialForInput{
    self.serialInputTries=0;

    self.serialInputTimer=[NSTimer scheduledTimerWithTimeInterval:0.25 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [self checkForSerialInput];
    }];


}
-(void)checkForSerialInput{
    if (self.serialInputTries>10) {
        [self.serialInputTimer invalidate];
        self.serialInputTimer=nil;
        self.serialInputTries=0;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSSerialDataNotFound" object:self];

//        [self kickOffCheckForArduinoTimer];
        return;
    }

    self.serialInputTries++;
    int rv;
    char buff[1024];
    int len = 1024;
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 10000;
    fd_set set;

    FD_ZERO(&set); /* clear the set */
    FD_SET(self.filedesc, &set); /* add our file descriptor to the set */

    rv = select(self.filedesc + 1, &set, NULL, NULL, &timeout);
    if(rv == -1){
        NSLog(@"select error");
        perror("select"); /* an error accured */
    }
    else if(rv == 0){
    }
    else {
        ssize_t total_bytes=read( self.filedesc, buff, len ); /* there was data to read */
        NSString *readString=[[NSString alloc] initWithBytes:buff length:total_bytes encoding:NSUTF8StringEncoding];
        if (self.leftoverString && readString){
            readString=[self.leftoverString stringByAppendingString:readString];
            self.leftoverString=nil;
        }
        if (readString && [readString containsString:@"\r\n"]){
            NSArray *lines=[readString componentsSeparatedByString:@"\r\n"];

            self.leftoverString=[lines lastObject];

            [lines enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

                [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSSerialDataLine" object:self userInfo:@{@"line":obj}];
                if (idx==lines.count-2) *stop=YES;

            }];
        }

    }

}
- (IBAction)updateArduino:(id)sender {
    if (!self.command){
        NSBeep();
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No Command";
        alert.informativeText=@"The command field cannot be left empty. Please type in a command and try again";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

        return;

    }

    if (self.hasFirmwarePassword==YES && self.firmwarePassword.length==0){

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No Firmware password specified";
        alert.informativeText=@"Please enter in a firmware password or unselect the checkbox for setting a firmware password.";
        [alert runModal];
        return;


    }

    if (self.command.length>240) {

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Command Length Error";
        alert.informativeText=[NSString stringWithFormat:@"Commands cannot be longer than 240 characters. The current command is %li characters long. Please shorten to 240 characters or less.",self.command.length];

        [alert addButtonWithTitle:@"OK"];
        [alert addButtonWithTitle:@"Cancel"];
        [alert runModal];
        return;
    }

    new_settings=[self settings];

    strlcpy(new_settings->command, self.command.UTF8String, sizeof(new_settings->command));

    new_settings->version=self.version;

    if (self.hasFirmwarePassword==YES ) {
        strlcpy(new_settings->firmware_password, self.firmwarePassword.UTF8String, sizeof(new_settings->firmware_password));
    }
    else {

        memset(new_settings->firmware_password, 0, sizeof(new_settings->firmware_password));
    }
    new_settings->startup_delay=(uint32)self.startupDelay;
    new_settings->pre_command_delay=(uint32)self.preCommandDelay;
    new_settings->autorun=self.shouldAutorun;
    new_settings->erase_volume=self.shouldEraseWithDiskUtility;
    new_settings->language_english=self.shouldSetLanguageToEnglish;
    new_settings->recovery_mode=self.recoveryMode;
    NSInteger n=[self sendSettings:new_settings];

    if (n<0) [self completSendSettingsWithSuccess:NO];

    char get_settings[]="get_settings\r\n";
    ssize_t b = write(self.filedesc, get_settings, sizeof(get_settings));
    if (b < 0)
        fputs("write() failed!\n", stderr);

    self.mode=VERIFY;
    [self monitorSerialForInput];

//    [self completSendSettingsWithSuccess:n<0?NO:YES];
}

-(void)completSendSettingsWithSuccess:(BOOL)success{

    if (success==NO) {

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Data write error";
        alert.informativeText=@"The data could not be written. Please check the arduino and try again.";
        [alert runModal];

    }
    else {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Update Successful";
        alert.informativeText=@"The arduino was updated with the new values. Please unplug now.";
        [alert runModal];

    }
    [self dismissController:self];
    close(self.filedesc);


}
-(NSInteger)sendSettings:(void *)inSettings{
    NSMutableString *stringToSend=[NSMutableString string];


    uint8* p = (uint8*)(const void*)inSettings;
    unsigned int i;

    char curr_bytes[3];
    for (i = 0; i < [self settingsSize]; i++){
        sprintf(curr_bytes,"%02x",p[i]);
        if (i%128==0) {
            [stringToSend appendString:[NSString stringWithFormat:@"\r\nset_settings %02i ",i/128]];
        }

        [stringToSend appendString:[NSString stringWithUTF8String:curr_bytes]];
    }

//    self.mode=IDLE;
//    [self monitorSerialForInput];

    NSString *fullString=[NSString stringWithFormat:@"%@\r\n",stringToSend];
    ssize_t n=-1;
    long blocksize=100;
    long currpos=0;

    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 100000000;
    while (currpos<fullString.length) {
         n = write(self.filedesc, [fullString substringFromIndex:currpos].UTF8String,blocksize);
        currpos+=blocksize;
        if (currpos+blocksize>fullString.length) {
            blocksize=fullString.length-currpos;
        }

        nanosleep(&tim , &tim2);
    }
//    close(self.filedesc);
    return n;
}
-(struct settings_t *)settings{

    struct settings_t *settings=calloc(1,sizeof(struct settings_t));
    return settings;

}
-(NSInteger)settingsSize{
    return sizeof(struct settings_t);

}

@end
