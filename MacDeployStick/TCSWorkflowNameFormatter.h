//
//  TCSWorkflowNameFormatter.h
//  MDS
//
//  Created by Timothy Perfitt on 3/11/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSWorkflowNameFormatter : NSFormatter

@end

NS_ASSUME_NONNULL_END
