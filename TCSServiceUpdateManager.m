//
//  TCSServiceUpdateManager.m
//  MDS
//
//  Created by Timothy Perfitt on 3/11/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSServiceUpdateManager.h"
#import "TCWebConnection.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSConstants.h"
@interface TCSServiceUpdateManager()
@property (strong) TCTaskWrapperWithBlocks *munkiTask;
@end
@implementation TCSServiceUpdateManager
+ (instancetype)updateManager {
    static TCSServiceUpdateManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[TCSServiceUpdateManager alloc] init];
        sharedMyManager.updatesAvailable=[NSMutableDictionary dictionary];

    });
    return sharedMyManager;
}
-(void)checkForMunkiUpdate:(id)sender{

    if ([self.updatesAvailable objectForKey:@"munki"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONUPDATEAVAILABLE object:self userInfo:[self.updatesAvailable objectForKey:@"munki"]];
        return;
    }
    __block NSMutableString *installedVersion=[NSMutableString string];
    NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"munki_version"]];
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *munkiPath=@"/usr/local/munki/munkiimport";
    if ([fm fileExistsAtPath:munkiPath]==NO) {
        return;
    }

    self.munkiTask=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

    } endBlock:^{

        TCWebConnection *wc=[[TCWebConnection alloc] initWithBaseURL:url];

        [wc sendRequestToPath:url.path type:@"GET" payload:@{} useAPIAuth:NO onCompletion:^(NSInteger responseCode, NSData *responseData) {

            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];

            if (!responseDict) return;
            NSString *latestVersion=[responseDict objectForKey:@"tag_name"];
            if (!latestVersion) return;
            latestVersion=[[latestVersion componentsSeparatedByString:@"v"] lastObject];
            [installedVersion replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, installedVersion.length)];


            NSLog(@"%@:%@",latestVersion,installedVersion);
            NSDictionary *updateInfo=@{@"service":@"munki",@"installed_version":installedVersion,@"latest_version":latestVersion};

            if ([installedVersion hasPrefix:latestVersion]==YES) {
                NSLog(@"we are current!");
                if ([self.updatesAvailable objectForKey:@"munki"]){
                    [self.updatesAvailable removeObjectForKey:@"munki"];
                }
            }
            else {
                
                [self.updatesAvailable setObject:updateInfo forKey:@"munki"];

            }
            [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONUPDATEAVAILABLE object:self userInfo:updateInfo];

        } onError:^(NSError *error) {
            NSDictionary *updateInfo=@{@"service":@"munki",@"installed_version":installedVersion};
            [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONUPDATEAVAILABLE object:self userInfo:updateInfo];
            [self.updatesAvailable setObject:updateInfo forKey:@"munki"];

            NSLog(@"could not get current version of Munki");
        }];

    } outputBlock:^(NSString *output) {
        [installedVersion appendString:output];
    } errorOutputBlock:^(NSString *errorOutput) {

    } arguments:@[munkiPath,@"-V"]];

    [self.munkiTask startProcess];

}
-(void)checkForMunkiReportUpdate:(id)sender{

    if ([self.updatesAvailable objectForKey:@"munkireport"]){
        dispatch_async(dispatch_get_main_queue(), ^{

        [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONUPDATEAVAILABLE object:self userInfo:[self.updatesAvailable objectForKey:@"munkireport"]];
            });
        return;
    }
    __block NSMutableString *installedVersion=[NSMutableString string];
    NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"munkireport_version"]];
    NSFileManager *fm=[NSFileManager defaultManager];


    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiReportFolderPath"];

    NSString *versionFile=[sharedFolder stringByAppendingPathComponent:@"app/helpers/site_helper.php"];
    if ([fm fileExistsAtPath:versionFile]){

        NSError *err;
        NSString *versionFileContents=[NSString stringWithContentsOfFile:versionFile encoding:NSUTF8StringEncoding error:&err];

        if (!versionFileContents){

            NSLog(@"versionFileContents: %@",err.localizedDescription);
            return;
        }
        NSArray *contents=[versionFileContents componentsSeparatedByString:@"\n"];

        [contents enumerateObjectsUsingBlock:^(NSString *string, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([string containsString:@"$GLOBALS['version'] ="]){
                NSError *error = nil;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"GLOBALS.*'(.*)'" options:NSRegularExpressionCaseInsensitive error:&error];
                NSTextCheckingResult *match = [regex firstMatchInString:string
                                                                options:0
                                                                  range:NSMakeRange(0, [string length])];
                if (match) {
                    NSRange firstHalfRange = [match rangeAtIndex:1];

                    installedVersion=[[string substringWithRange:firstHalfRange] mutableCopy];
                    NSLog(@"%@",installedVersion);
                    TCWebConnection *wc=[[TCWebConnection alloc] initWithBaseURL:url];


                    [wc sendRequestToPath:url.path type:@"GET" payload:@{} useAPIAuth:NO onCompletion:^(NSInteger responseCode, NSData *responseData) {

                        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];

                        if (!responseDict) return;
                        NSString *latestVersion=[responseDict objectForKey:@"tag_name"];
                        if (!latestVersion) {
                            NSString *responseString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                            NSLog(@"%@",responseString);
                            NSDictionary *updateInfo=@{@"service":@"munkireport",@"installed_version":installedVersion,@"latest_version":@"Unknown"};

                            [self.updatesAvailable setObject:updateInfo forKey:@"munkireport"];

                            return;
                        }
                        latestVersion=[[latestVersion componentsSeparatedByString:@"v"] lastObject];
                        [installedVersion replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, installedVersion.length)];


                        NSLog(@"%@:%@",latestVersion,installedVersion);
                        NSDictionary *updateInfo=@{@"service":@"munkireport",@"installed_version":installedVersion,@"latest_version":latestVersion};

                        [self.updatesAvailable setObject:updateInfo forKey:@"munkireport"];

                        [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONUPDATEAVAILABLE object:self userInfo:updateInfo];
                            

                    } onError:^(NSError *error) {
                        NSDictionary *updateInfo=@{@"service":@"munkireport",@"installed_version":installedVersion};
                        [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONUPDATEAVAILABLE object:self userInfo:updateInfo];
                        [self.updatesAvailable setObject:updateInfo forKey:@"munkireport"];

                        NSLog(@"could not get current version of MunkiReport");
                    }];
                }

            }
        }];


    }
}
@end
