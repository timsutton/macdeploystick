//
//  MDSPrivHelperToolController.h
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <SecurityInterface/SFAuthorizationView.h>
NS_ASSUME_NONNULL_BEGIN

@interface MDSPrivHelperToolController : NSObject
@property (strong) SFAuthorization *authorization;
- (void)installToolIfNecessary;
+ (instancetype)sharedHelper ;
- (void)installInstallMacOSWithProductID:(NSString *)productID catalog:(NSString *)catalog workingPath:(NSString *)workingPath withCallback:(void (^)(BOOL success))callback;
-(void)stopRunningProcessesWithCallback:(void (^)(BOOL success))callback;
-(void)updateWebserverWithConfigurations:(NSDictionary *)configurations withCallback:(void (^)(BOOL success))callback;
-(void)stopWebserverWithCallback:(void (^)(BOOL success))callback;
-(void)startWebserverWithCallback:(void (^)(BOOL success))callback;
-(void)trustCertificateAtPath:(NSString *)certPath withCallback:(void (^)(NSError *err))callback;

-(void)restartWebserverWithCallback:(void (^)(BOOL success))callback;
-(void)setupMunkiReportWithSource:(NSString *)source destination:(NSString *)destination withCallback:(void (^)(NSError *err))callback;
-(void)addMunkiReportUserFile:(NSString *)path contents:(NSString *)contents withCallback:(void (^)(NSError *err))callback;
-(void)removeUsers:(NSArray *)users fromFolder:(NSString *)path callback:(void (^)(NSError *err))callback;
-(void)startMicroMDMWithSettings:(NSString *)settings callback:(void (^)(NSError * _Nullable err))callback;
-(void)stopMicroMDMWithCallback:(void (^)(NSError * _Nullable err))callback;
-(void)restartMicroMDMWithCallback:(void (^)(NSError * _Nullable err))callback;
-(void)createMunkiRepoAtPath:(NSString *)munkiRepoPath callback:(void (^)(NSError * _Nullable))callback;
-(void)createMacOSInstallVolume:(NSString *)volumePath withInstaller:(NSString *)installerPath callback:(void (^)(BOOL isDone, NSString *statusMsg,NSError * _Nullable))callback;
-(NSData *)token;
@end

NS_ASSUME_NONNULL_END
