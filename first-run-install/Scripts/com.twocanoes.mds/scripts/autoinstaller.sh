#!/bin/sh

if [ -f /var/com.twocanoes.mds/watch/install ]; then
	
	if  [ -d /var/com.twocanoes.mds/packages ]; then 
	    /var/com.twocanoes.mds/LoginLog.app/Contents/MacOS/LoginLog -logfile /var/log/install.log &
		for pkg in /var/com.twocanoes.mds/packages/*.pkg; do
			[ -e "$pkg" ] || continue
			/usr/sbin/installer -target / -pkg "${pkg}"
		done
	fi

    /usr/bin/killall LoginLog
	/bin/rm /Library/LaunchDaemons/com.twocanoes.mds.autoinstall.plist
	/bin/rm /Library/LaunchAgents/com.twocanoes.mds-kickstartinstall.plist
	/bin/rm -rf /var/com.twocanoes.mds

    /bin/launchctl bootout system/com.twocanoes.mds.autoinstall

fi


