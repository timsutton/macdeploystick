#include <HID-Project.h>
#include <HID-Settings.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <avr/wdt.h>
#include <avr/wdt.h>

#define LAST_INSTALLED_RECOVERY 0
#define LATEST_RECOVERY 1
#define EARLIEST_RECOVERY 2

#define CURRENTVERSION 34
#define BLOCK_SIZE 256
#define DEFAULTCOMMAND "/Volumes/mds/run"
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

#ifdef SHOWFREEMEM
int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}
#endif
enum mode_types {RECOVERY, DEP, CLI, UAMDM} current_mode;

struct settings_t
{
  char command[255];
  char firmware_password[32];
  long startup_delay;
  long pre_command_delay;
  bool autorun;
  bool erase_volume;
  int version;
  int recovery_mode;
  bool language_english;

} __attribute__((packed)) settings;

void reboot() {
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}

void setup() {
  pinMode(13, OUTPUT);

}

char wifi_ssid[32];
char wifi_password[32];

void flash_led(int count) {

  for (int i = 0; i < count; i++) {
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(100);
  }
#ifdef SHOWFREEMEM
     Serial.print("Free memory: ");
    Serial.println(freeMemory());
#endif
}

void presskey(KeyboardKeycode key, int times, int delayms) {

  for (int i = 0; i < times; i++) {
    flash_led(1);
    BootKeyboard.press(key);
    delay(delayms);
    BootKeyboard.releaseAll();
    delay(100);

  }
#ifdef SHOWFREEMEM
    Serial.print("Free memory: ");
    Serial.println(freeMemory());
#endif
}
void showusage() {

  Serial.println(F("\nCopyright 2019 Twocanoes Software, Inc."));
  Serial.println(F("help: this message"));
  Serial.println(F("show: show current settings"));
  Serial.println(F("reset: reset settings to defaults"));
  Serial.println(F("reboot: reboot the device"));
  Serial.println(F("set_command <command>: set command to run in recovery."));
  Serial.println(F("set_firmware_password <password>: set firmware password to enter prior to booting to recovery."));
  Serial.println(F("set_startup_delay <seconds>: how many seconds to wait from booting in recovery to launching terminal."));
  Serial.println(F("set_pre_command_delay <seconds>: how many seconds to wait after launching terminal until typing command."));
  Serial.println(F("set_settings <json>: Provide all settings in JSON format."));
  Serial.println(F("get_settings: Get all settings in JSON format."));
  Serial.println(F("dep: Automatically configure DEP in setup assistant"));
  Serial.println(F("uamdm: Open System Preferences and click Approve for UAMDM in Profiles pane. Must be logged in."));
  Serial.println(F("profile <path_to_profile> <admin name> <admin password>: install profile."));
  Serial.println(F("recovery: provide keyboard commands to boot into recovery and run resources from external volume or remote disk image."));
  Serial.println(F("set_autorun <on|off>: automatically enter recovery mode after admin time."));
  Serial.println(F("set_erase_volume <on|off>: open up disk utility and erase first volume."));
  Serial.println(F("set_recovery_mode: 0|1|2: recovery mode - command-r (0), option-command-r (1), shift-option-command-r (2)"));
  Serial.println(F("set_language_english <on|off>: turn on or off setting the language to English before running workflow."));

}

int setdefaults() {
  strcpy(settings.command, DEFAULTCOMMAND);
  strcpy(settings.firmware_password, "");
  settings.version = CURRENTVERSION;
  settings.startup_delay = 180;
  settings.autorun = true;
  settings.erase_volume = false;
  settings.pre_command_delay = 6;
  settings.recovery_mode=0;
  settings.language_english=false;
  if (EEPROM_writeAnything(0, settings)<0) {
     if (EEPROM_writeAnything(0, settings)<0) {
        return -1;
     }
  }
  
  EEPROM_readAnything(0, settings);
  return 0;

}
void enter_cli() {
  
  Serial.begin(9600);

  Serial.println(F("Configuration Mode. Enter help for assistance. Copyright 2018-2019 Twocanoes Software, Inc."));
  while (current_mode == CLI) {
    Serial.setTimeout(1000000);
    flash_led(1);
    Serial.print(">");
    String s = Serial.readStringUntil('\n');
    s.trim();
    Serial.println(s);
 #ifdef SHOWFREEMEM
    Serial.print("Free memory: ");
    Serial.println(freeMemory());
#endif


    if (s.startsWith("show") == true) {
      Serial.print("Version: ");
      Serial.println(settings.version, DEC);
      Serial.print(F("Command:"));
      Serial.println(settings.command);
      Serial.print(F("Startup Delay:"));
      Serial.println(settings.startup_delay, DEC);
      Serial.print(F("Pre Commmand Delay:"));
      Serial.println(settings.pre_command_delay, DEC);
      Serial.print(F("Firmware is Set:"));
      if (strlen(settings.firmware_password) > 0) Serial.println("YES");
      else Serial.println(F("NO"));
      Serial.print(F("Autorun:"));
      if (settings.autorun == 1) {
        Serial.println("on");
        Serial.print(F("Recovery mode: "));
        Serial.println(settings.recovery_mode, DEC);
      }
      else Serial.println("off");
      Serial.print(F("Erase Volume:"));
      if (settings.erase_volume == 1) Serial.println("on");
      else Serial.println("off");

      Serial.print(F("Set Language to English:"));
       if (settings.language_english == 1) Serial.println("on");
      else Serial.println("off");
    }
    else if (s.startsWith("set_command") == true) {

      String new_command = s.substring(12);
      strncpy(settings.command, new_command.c_str(), 255);
      EEPROM_writeAnything(0, settings);

    }
    else if (s.startsWith("set_firmware_password") == true) {

      String new_firmware_password = s.substring(22);
      strncpy(settings.firmware_password, new_firmware_password.c_str(), 32);
      EEPROM_writeAnything(0, settings);

    }
    else if (s.startsWith(F("help")) == true) {
      showusage();

    }
    else if (s.startsWith(F("set_startup_delay")) == true) {
      String new_command = s.substring(18);
      settings.startup_delay = new_command.toInt();
      EEPROM_writeAnything(0, settings);

    }
    else if (s.startsWith(F("set_pre_command_delay")) == true) {
      String new_command = s.substring(22);
      settings.pre_command_delay = new_command.toInt();
      EEPROM_writeAnything(0, settings);

    }
    else if (s.startsWith(F("set_recovery_mode")) == true) {
      String new_recovery_mode = s.substring(18);
      settings.recovery_mode = new_recovery_mode.toInt();
      EEPROM_writeAnything(0, settings);

    }

    else if (s.startsWith(F("reset")) == true) {
      reset();
    }
    else if (s.startsWith(F("recovery")) == true) {
      current_mode = RECOVERY;
    }
    else if (s.startsWith(F("bootloader")) == true) {
      enter_bootloader();
    }
    else if (s.startsWith(F("dep")) == true) {
      current_mode = DEP;
    }
    else if (s.startsWith(F("uamdm")) == true) {
      current_mode = UAMDM;
    }
    else if (s.startsWith(F("set_autorun")) == true) {
        char *line = (char *)s.c_str();
        char *ptr = NULL;
        byte index = 0;
        char *tokens[2];
        ptr = strtok(line, " ");  // takes a list of delimiters
        while (ptr != NULL && index < 2) {
          tokens[index] = ptr;
          index++;
          ptr = strtok(NULL, " ");  // takes a list of delimiters
        }
        if (index != 2) {
        
          Serial.println(F("Invalid commmand. Please provide on or off for the set_autorun command"));
        }
        else {
          if (strncmp(tokens[1], "on", 3) == 0) {
    
            settings.autorun = true;
            Serial.println(F("Autorun turned on"));
            EEPROM_writeAnything(0, settings);
          }
          else if (strncmp(tokens[1], "off", 3) == 0) {
            settings.autorun = false;
            Serial.println(F("Autorun turned off"));
            EEPROM_writeAnything(0, settings);
          }
          else {
            Serial.println(F("Invalid value. Please specify on or off"));
          }
        }
    }
    else if (s.startsWith(F("profile")) == true) {
      char *line = (char *)s.c_str();
        char *ptr = NULL;
        byte index = 0;
        char *tokens[3];
        ptr = strtok(line, " ");  // takes a list of delimiters
        while (ptr != NULL && index < 3) {
          tokens[index] = ptr;
          index++;
          ptr = strtok(NULL, " ");  // takes a list of delimiters
        }
        if (index != 3) {
        
          Serial.println(F("Invalid commmand. Provide path to profile and a admin password separated by a space. Path and password cannot contain the space character"));
          
        }
        else {

        
        enter_install_profile(tokens[1], tokens[2]);
        }
    }
    else if (s.startsWith(F("set_erase_volume")) == true) {
        char *line = (char *)s.c_str();
        char *ptr = NULL;
        byte index = 0;
        char *tokens[2];
        ptr = strtok(line, " ");  // takes a list of delimiters
        while (ptr != NULL && index < 2) {
          tokens[index] = ptr;
          index++;
          ptr = strtok(NULL, " ");  // takes a list of delimiters
        }
        if (index != 2) {
        
          Serial.println(F("Invalid commmand. Please provide on or off for the set_erase_volume command"));
        }
        else {
          if (strncmp(tokens[1], "on", 3) == 0) {
    
            settings.erase_volume = true;
            Serial.println(F("Erase Volume turned on"));
            EEPROM_writeAnything(0, settings);
          }
          else if (strncmp(tokens[1], "off", 3) == 0) {
            settings.erase_volume = false;
            Serial.println(F("Erase Volume turned off"));
            EEPROM_writeAnything(0, settings);
          }
          else {
            Serial.println(F("Invalid value. Please specify on or off"));
          }
        }
    }
    else if (s.startsWith(F("set_language_english")) == true) {
        char *line = (char *)s.c_str();
        char *ptr = NULL;
        byte index = 0;
        char *tokens[2];
        ptr = strtok(line, " ");  // takes a list of delimiters
        while (ptr != NULL && index < 2) {
          tokens[index] = ptr;
          index++;
          ptr = strtok(NULL, " ");  // takes a list of delimiters
        }
        if (index != 2) {
        
          Serial.println(F("Invalid commmand. Please provide on or off for the set_erase_volume command"));
        }
        else {
          if (strncmp(tokens[1], "on", 3) == 0) {
    
            settings.language_english = true;
            Serial.println(F("Set English Language turned on"));
            EEPROM_writeAnything(0, settings);
          }
          else if (strncmp(tokens[1], "off", 3) == 0) {
            settings.language_english = false;
            Serial.println(F("Set English Language turned off"));
            EEPROM_writeAnything(0, settings);
          }
          else {
            Serial.println(F("Invalid value. Please specify on or off"));
          }
        }
   
    }
    else if (s.startsWith(F("reboot")) == true) {

      reboot();
    }
    else if (s.length() == 0) {


    }
   else if (s.startsWith(F("set_settings")) == true){
              static unsigned int block_number;
          
              int string_length=s.length()-16;
              s.c_str();  //convert to c string
  
              char *pos=&s[13]; //jump past header
              block_number=3;
              sscanf(pos,"%d",&block_number);
              pos=&s[16];
            
              byte b;
              int start_byte=block_number*BLOCK_SIZE/2;
              unsigned int end_byte=start_byte+string_length/2-1;
              if ((end_byte>sizeof(settings)-1)|| (string_length>BLOCK_SIZE) ){
                 Serial.println(F("Invalid block or length."));
  
                 continue;
             
              }
           
            
              for(long i = 0; i<string_length/2; i++){
                  byte *ptr=(byte *)(void *)&settings;
                  sscanf(&pos[i*2],"%02x",&b);
                
                  memcpy(&ptr[start_byte+i],&b,1); 
              }

              
              if (block_number>=sizeof(settings)/(BLOCK_SIZE/2)){
                
                EEPROM_writeAnything(0, settings);
              }
        }

    else if (s.startsWith("get_settings") == true) {

      char temp_firmware_password[sizeof(settings.firmware_password)];
      strncpy(temp_firmware_password,settings.firmware_password,sizeof(settings.firmware_password));

      memset(settings.firmware_password,0,sizeof(settings.firmware_password));
      int firmware_pw_len = strlen(temp_firmware_password);

      if (firmware_pw_len>0) {

        settings.firmware_password[0]=0x01;
        
      }
      const byte* p = (const byte*)(const void*)&settings;
      unsigned int i;
      char curr_bytes[3];
      Serial.print(F("DATA:"));
      for (i = 0; i < sizeof(settings); i++){
        sprintf(curr_bytes,"%02x",p[i]);
        Serial.print(curr_bytes);
      }
      Serial.println();  

      strncpy(settings.firmware_password,temp_firmware_password,sizeof(settings.firmware_password));
    }
    else {
      Serial.println(F("Invalid command. Enter help for usage."));
      Serial.println(s);

    }
  }
  Serial.end();
}
void hold_recovery_key(int seconds) {
  int j;
  int led_flash_delay = 200; // ms
  int key_press_interval = 500; // ms
  BootKeyboard.begin();
  for (j = 0; j < (seconds * 1000) / key_press_interval; j++) {

  switch (settings.recovery_mode) {
    case LAST_INSTALLED_RECOVERY: 
        BootKeyboard.press(KEY_LEFT_GUI);
        BootKeyboard.press('r');
        break;
    case LATEST_RECOVERY:
        BootKeyboard.press(KEY_LEFT_ALT);
        BootKeyboard.press(KEY_LEFT_GUI);
        BootKeyboard.press('r');
        break;
    case EARLIEST_RECOVERY:
        BootKeyboard.press(KEY_LEFT_SHIFT);
        BootKeyboard.press(KEY_LEFT_ALT);
        BootKeyboard.press(KEY_LEFT_GUI);
        BootKeyboard.press('r');
        break;
    default:
      break;
  }

    delay(key_press_interval - led_flash_delay);
    flash_led(1);
    BootKeyboard.releaseAll();
  }
}
void enter_dep() {
  // Enter setup assistant. Assumes all screens that can be are skipped. 
  flash_led(3);
  BootKeyboard.begin();
  // Country picker - tab once, Continue
  presskey(KEY_TAB, 1, 40);
  presskey(KEY_SPACE, 1, 40);
  // Keyboard layout picker - tab x3, Continue
  presskey(KEY_TAB, 3, 40);
  presskey(KEY_SPACE, 1, 40);
  // Remote Management screen - tab x3, Continue
  presskey(KEY_TAB, 3, 40);
  presskey(KEY_SPACE, 1, 40);

  BootKeyboard.end();

  current_mode = CLI;

}
void enter_install_profile(char *profile_path, char*password){
 Serial.println(F("installing profile..."));
  // Enter setup assistant. Assumes all screens that can be are skipped. 
  flash_led(4);
  BootKeyboard.begin();

  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press(' ');
delay(50);
BootKeyboard.releaseAll();
BootKeyboard.print(F("Finder"));
delay(500);
BootKeyboard.println();
delay(500);

//press command-shift-g to go to folder
   BootKeyboard.press(KEY_LEFT_GUI);
   BootKeyboard.press(KEY_LEFT_SHIFT);
    BootKeyboard.press('g');
    delay(50);
    BootKeyboard.releaseAll();
    delay(1000);
    BootKeyboard.println(profile_path);
    delay(1000);


     BootKeyboard.press(KEY_LEFT_GUI);
    BootKeyboard.press('o');
    delay(50);
    BootKeyboard.releaseAll();
    delay(3000);
  


  presskey(KEY_TAB, 2, 50);

BootKeyboard.print(' ');

delay(2000);

BootKeyboard.print(password);
delay(1000);
BootKeyboard.println("");
Keyboard.releaseAll();
  BootKeyboard.end();

  current_mode = CLI;

}
void enter_uamdm() {
  Serial.println(F("Running UAMDM..."));
  // Enter setup assistant. Assumes all screens that can be are skipped. 
  flash_led(3);
  //delay for a bit since it is freaky to start so fast
  delay(2000);
  
  BootKeyboard.begin();

//Apple menu
  //turn on menu navigation
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F2);
  BootKeyboard.releaseAll();


//move to System Preferences
  presskey(KEY_DOWN_ARROW, 3, 50);
  BootKeyboard.println("");

//Go to Profiles Pane
  delay(5000);
  BootKeyboard.print("Profiles");
  delay(1000);
  BootKeyboard.println("");
  delay(5000);
  
//Press control F7 to turn on keboard nav
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F7);
  BootKeyboard.releaseAll();

//tab to button
presskey(KEY_TAB, 2, 40);

//press button
presskey(KEY_SPACE, 1, 40);

//tab to Approve
presskey(KEY_TAB, 1, 40);

//Approve it!
presskey(KEY_SPACE, 1, 40);

//sleep for 3 seconds to bask in the glory
delay(3000);

//Press control F7 to turn off keboard nav
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F7);
  BootKeyboard.releaseAll();
  
//exit
    BootKeyboard.press(KEY_LEFT_GUI);
    BootKeyboard.press('q');
    delay(50);
    BootKeyboard.releaseAll();
   
    
    BootKeyboard.end();

  current_mode = CLI;

}
void erase_volume(){

 // select disk utility
    presskey(KEY_DOWN_ARROW, 4, 50);

//select continue
    presskey(KEY_TAB, 1, 50); 
// press continue
    presskey(KEY_SPACE, 1, 50);  
    //wait 20 seconds to open Disk Utility. was 5, now it is 20 from a request.
    delay(20000);
    //hold up arrow for 3 seconds
    presskey(KEY_UP_ARROW,1,3000);
    BootKeyboard.press(KEY_LEFT_GUI);
    BootKeyboard.press(KEY_LEFT_SHIFT);
    BootKeyboard.press('e');
    delay(50);
    BootKeyboard.releaseAll();
    
    // move to erase button
     presskey(KEY_TAB,3,50);
    // select erase button
     presskey(KEY_SPACE,1,50);
    // wait 30 seconds to erase the disk. was at 15 seconds, then a request for 30.
    delay(30000);
// Press return to complete
    presskey(KEY_RETURN,1,50);
   
// quit with command q and wait a bit
    BootKeyboard.press(KEY_LEFT_GUI);
    BootKeyboard.press('q');
    delay(50);
    BootKeyboard.releaseAll();
    delay(3000);

}
void enter_bootloader(){
  uint16_t bootKey = 0x7777;
  uint16_t *const bootKeyPtr = (uint16_t *)0x0800;

// Stash the magic key
*bootKeyPtr = bootKey;

// Set a watchdog timer
wdt_enable(WDTO_120MS);

while(1) {} // This infinite loop ensures nothing else
            // happens before the watchdog reboots us

}
void enter_recovery() {
  Serial.println(F("Running..."));
  Serial.end();
  BootKeyboard.begin();
  delay(500);


  int firmware_pw_len = strlen(settings.firmware_password);
  if (firmware_pw_len > 0) {
    int i;
    delay(500);
    for (i = 0; i < firmware_pw_len; i++) {

      BootKeyboard.press(settings.firmware_password[i]);
      delay(20);
      BootKeyboard.releaseAll();
      delay(500);
    }
    BootKeyboard.press(KEY_RETURN);
    delay(500);
    BootKeyboard.releaseAll();
  }
  delay(500);

  hold_recovery_key(30);
  long delay_completed=0;
  long total_delay = settings.startup_delay * 1000;
  int led_flash_delay = 200; // ms
  int led_flash_interval = 1000; // ms

  while(delay_completed<total_delay) {
    flash_led(2);
    delay(led_flash_interval - led_flash_delay * 2);
    delay_completed += led_flash_interval;
  }

  //not sure why theses escapes are here
  presskey(KEY_ESC, 1, 400);
  presskey(KEY_ESC, 1, 400);
  delay(500);

  //if desktop, need to hit space to dismiss mouse setup. otherwise it is just ignored
  presskey(KEY_SPACE, 1, 400);
  delay(10000);

  //skip language
  BootKeyboard.println("\n");
  delay(30000);

  if (settings.erase_volume==true){
      erase_volume();
  }
  if (settings.language_english==true) {
    //turn on menu navigation
    BootKeyboard.press(KEY_LEFT_CTRL);
    BootKeyboard.press(KEY_F2);
    BootKeyboard.releaseAll();
  
    //move to language menu
    presskey(KEY_RIGHT_ARROW, 2, 50);
    //move to terminal menu
    presskey(KEY_DOWN_ARROW, 2, 400);
    BootKeyboard.println("\n");
  
    presskey(KEY_UP_ARROW, 2, 5000);
    BootKeyboard.println("\n");
    delay(10000);
  }
    //turn on menu navigation
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F2);
  BootKeyboard.releaseAll();

  //move to utilities menu
  presskey(KEY_RIGHT_ARROW, 4, 50);
  //move to terminal menu
  presskey(KEY_DOWN_ARROW, 4, 400);
  //go go activate!
  BootKeyboard.println("\n");

  //run command
  delay(settings.pre_command_delay * 1000);
  BootKeyboard.println(settings.command);
  BootKeyboard.end();

  //done, so go back to CLI
  Serial.begin(9600);
  delay(2000);
  current_mode = CLI;
}
void reset(){
  if(setdefaults()!=0){
    Serial.println("error writing defaults!");
      while(1) {
        digitalWrite(13, HIGH);
        delay(50);
        digitalWrite(13, LOW);
        delay(50);
      }
    }
}
void loop() {

digitalWrite(13, HIGH);
  Serial.begin(9600);
  delay(2000);
digitalWrite(13, LOW);
digitalWrite(13, HIGH);
  EEPROM_readAnything(0, settings);
  if (settings.version != CURRENTVERSION) {
      
    reset();
    
  }
  Serial.println(F("Copyright 2018 Twocanoes Software, Inc."));
  Serial.println(F("Press <return> to enter configuration mode."));
  int i;
  current_mode = RECOVERY;
  if (settings.autorun == true) {
    for (i = 0; i < 10; i++) {
      if (Serial.available()) {
        while (Serial.available()) {
          Serial.read();
        }
        current_mode = CLI;
        break;
      }
      else {
        delay(500);
      }
    }
    
  }
  else {
    current_mode = CLI;
  }
digitalWrite(13, LOW);
  if (current_mode != CLI) {  
    Serial.end();
  }
  while (1) {
    switch (current_mode) {

      case CLI:
        enter_cli();
        break;
      case DEP:
        enter_dep();
        break;
      case UAMDM:
        enter_uamdm();
        break;
      case RECOVERY:
        enter_recovery();
        break;
    }
  }
}
