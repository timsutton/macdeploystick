#!/bin/bash

BASENAME=${0##*/}
SCRIPTDIR=${0%$BASENAME}

cp "${SCRIPTDIR}/Resources/com.apple.dock.plist" "/System/Library/User Template/English.lproj/Library/Preferences/"

for user in /Users/* ; do
	cp "${SCRIPTDIR}/Resources/com.apple.dock.plist" "${user}"/Library/Preferences/
done

exit 0
