#!/bin/bash

FOLDER_NAME="AdditionalResources"
USERNAME="testuser"
DEST_FOLDER="Desktop"

################ DONT MAKE CHANGES BELOW UNLESS REQUIRED ################

BASENAME=${0##*/}
SCRIPTDIR=${0%$BASENAME}

cp -Rv "${SCRIPTDIR}/Resources/${FOLDER_NAME}" "/Users/${USERNAME}/${DEST_FOLDER}"

chown -R "${USERNAME}" "/Users/${USERNAME}/${DEST_FOLDER}/${FOLDER_NAME}"

exit 0
