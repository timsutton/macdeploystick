//
//  TCSMunkiReportStatusViewController.m
//  MDS_appstore
//
//  Created by Timothy Perfitt on 3/13/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMunkiReportStatusViewController.h"

@interface TCSMunkiReportStatusViewController ()

@end

@implementation TCSMunkiReportStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
-(IBAction)munkiReportStatusWindowActionButtonPressed:(id)sender{
    [self.delegate cancelProcess:self];
}
@end
