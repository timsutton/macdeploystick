//
//  TCSBleuStationPassword.h
//  Bleu Configurator
//
//  Created by Steve Brokaw on 7/21/14.
//  Copyright (c) 2014 Twocanoes. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TCSKeychain : NSObject

+ (NSString *)randomPasswordLength:(NSUInteger)length;
+ (NSString *)randomPassword;

+ (NSString *)passwordForService:(NSString *)service account:(NSString *)account error:(NSError **)err;
+ (BOOL)setPassword:(NSString *)password forService:(NSString *)service account:(NSString *)account;
+ (void)deletePasswordForService:(NSString *)service account:(NSString *)account;

+ (NSString *)passwordForAccount:(NSString *)account error:(NSError **)err;
+ (BOOL)setPassword:(NSString *)password forAccount:(NSString *)account;
+ (void)deletePasswordForAccount:(NSString *)account;


@end

@interface TCSKeychain (BleuStation)

+ (NSString *)passwordForUUID:(NSString *)uuid major:(NSNumber *)major minor:(NSNumber *)minor;
+ (void)setPassword:(NSString *)password forUUID:(NSString *)uuid major:(NSNumber *)major minor:(NSNumber *)minor;
+ (void)deletePasswordForUUID:(NSString *)uuid major:(NSNumber *)major minor:(NSNumber *)minor;

@end

@interface TCSPassword : NSObject

@property (nonatomic, copy) NSString *service;
@property (nonatomic, copy) NSString *accessGroup;
@property (nonatomic, copy) NSString *password;

- (instancetype)initWithService:(NSString *)service account:(NSString *)account group:(NSString *)group;

@end
