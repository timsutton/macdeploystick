//
//  TCSSyncViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/26/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSSyncViewController.h"
#import "DeploySettings.h"
@interface TCSSyncViewController ()
@property (weak) IBOutlet NSPopUpButton *syncURLPopupButton;
@property (strong) IBOutlet NSArrayController *syncArrayController;
@property (strong) NSMutableDictionary *syncItem;
@property (weak) IBOutlet NSTextField *folderTextField;
@end

@implementation TCSSyncViewController
- (IBAction)openSyncFolderPressed:(id)sender {
    [[NSWorkspace sharedWorkspace] openFile:self.folderTextField.stringValue];

    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    int i=[[ud objectForKey:@"SelectedSyncURLIndex"] intValue];
    self.syncArrayController.selectionIndex=i;
    [self updateDetails];

    
}
- (IBAction)syncURLPopupChanged:(NSPopUpButton *)button {

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    [ud setObject:@([button indexOfSelectedItem]) forKey:@"SelectedSyncURLIndex"];
    //    NSString *syncURLString=[ud objectForKey:@"syncURL"];
   NSString *selectedURLString=[[[self.syncArrayController selectedObjects] lastObject] objectForKey:@"url"];
    [ud setObject:selectedURLString forKey:@"syncURL"];
    [self updateDetails];
}

-(void)updateDetails{
    NSString *syncURLString=[[[self.syncArrayController selectedObjects] lastObject] objectForKey:@"url"];
    NSString *hash=[NSString stringWithFormat:@"%lx",[syncURLString hash]];
    NSString *syncFolder=[[DeploySettings syncFolder] stringByAppendingPathComponent:hash];

    self.folderTextField.stringValue=syncFolder;
    NSFileManager *fm=[NSFileManager defaultManager];
    if ([fm fileExistsAtPath:syncFolder]==NO){

        NSError *err;
        if([fm createDirectoryAtPath:syncFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){
            [[NSAlert alertWithError:err] runModal];

        }

    }

}
- (IBAction)removeButtonPressed:(id)sender {
    [self.syncArrayController removeObject:self.syncArrayController.selectedObjects.firstObject];

    self.syncArrayController.selectionIndex=0;
    [self updateDetails];
}
- (IBAction)clearSyncCache:(id)sender {

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Clear Sync Cache?";
    alert.informativeText=@"Are you sure you want to remove all the sync files from the cache?";

    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    NSError *err;
    NSString *syncFolder=[DeploySettings syncFolder];

    NSFileManager *fm=[NSFileManager defaultManager];

    if ([fm fileExistsAtPath:syncFolder]==NO){
        return;
    }
    if([fm removeItemAtPath:syncFolder error:&err]==NO){

        [[NSAlert  alertWithError:err] runModal];

    }

}
- (void)dismissViewController:(NSViewController *)viewController{
    [super dismissViewController:viewController];
    if (viewController.representedObject && [[viewController.representedObject allKeys] count]>0) {
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        [self.syncArrayController addObject:viewController.representedObject];
        NSDictionary *dict=viewController.representedObject;
        [ud setObject:[dict objectForKey:@"url"] forKey:@"syncURL"];

    }
    [self updateDetails];
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{

    self.syncItem=[@{} mutableCopy];
    NSViewController *dest=segue.destinationController;
    dest.representedObject=self.syncItem;
}
@end
