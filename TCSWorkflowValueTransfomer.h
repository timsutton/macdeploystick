//
//  TCSWorkflowValueTransfomer.h
//  MDS
//
//  Created by Timothy Perfitt on 5/4/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSWorkflowValueTransfomer : NSValueTransformer

@end

NS_ASSUME_NONNULL_END
