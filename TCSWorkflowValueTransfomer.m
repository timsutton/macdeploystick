//
//  TCSWorkflowValueTransfomer.m
//  MDS
//
//  Created by Timothy Perfitt on 5/4/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSWorkflowValueTransfomer.h"

@implementation TCSWorkflowValueTransfomer
+ (Class)transformedValueClass {
    return [NSString class];
}
+ (BOOL)allowsReverseTransformation {
    return NO;
}
- (id)transformedValue:(id)value {
    return (value == nil) ? nil : NSStringFromClass([value class]);
}
@end
