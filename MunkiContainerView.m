//
//  MunkiContainerView.m
//  MDS
//
//  Created by Timothy Perfitt on 9/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "MunkiContainerView.h"
#import "MunkiView.h"
@implementation MunkiContainerView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}
- (instancetype)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSNib *nib=[[NSNib alloc] initWithNibNamed:@"MunkiView" bundle:nil];
        [nib instantiateWithOwner:self topLevelObjects:nil];
        [self addSubview:self.view];
    }
    return self;
}



@end


