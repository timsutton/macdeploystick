//
//  TCSMunkiReportStatusViewController.h
//  MDS_appstore
//
//  Created by Timothy Perfitt on 3/13/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TCSMunkiReportFeedbackProtocol <NSObject>

-(void)cancelProcess:(id)sender;

@end

@interface TCSMunkiReportStatusViewController : NSViewController
@property (assign) id <TCSMunkiReportFeedbackProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
