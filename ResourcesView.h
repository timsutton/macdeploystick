//
//  MunkiView.h
//  MDS
//
//  Created by Timothy Perfitt on 9/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSWorkflow.h"
#import "ResourcesView.h"
#import "ResourcesConfigProtocol.h"

NS_ASSUME_NONNULL_BEGIN


@interface ResourcesView : NSView
@end

NS_ASSUME_NONNULL_END
