//
//  TCSLogWindowController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/18/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSLogWindowController.h"

@interface TCSLogWindowController ()

@end

@implementation TCSLogWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    [self.window setFrameAutosaveName:@"logwindowposition"];

}
- (BOOL)windowShouldClose:(id)sender {
    [self.window orderOut:self];
    return NO;
}
@end
