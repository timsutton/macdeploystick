//
//  TCSLogWindowController.m
//  Winclone
//
//  Created by Timothy Perfitt on 8/16/19.
//  Copyright © 2019 Twocanoes Software, Inc. All rights reserved.
//

//#define self.logFile @"/Library/Logs/mdshelper.log"

#import "TCSLogFileWindowController.h"
#import "BDFileTailer.h"

@interface TCSLogFileWindowController ()
@property (unsafe_unretained) IBOutlet NSTextView *logTextView;
@property (strong) BDFileTailer *tailer;
@property (assign) BOOL shouldTail;
@property (strong ) NSTimer*timer;

@end

@implementation TCSLogFileWindowController


-(IBAction)scrollToEnd:(id)sender{
    [self.logTextView scrollToEndOfDocument:self];
}
-(IBAction)clearLog:(id)sender{
    NSMutableString *mutableString= [[self.logTextView textStorage] mutableString];
    [mutableString deleteCharactersInRange:NSMakeRange(0, mutableString.length)];
}
-(IBAction)showLogButtonPressed:(id)sender{
    [[NSWorkspace sharedWorkspace] openFile:self.logFile withApplication:@"Console"];
}
- (IBAction)clearLogButtonPressed:(id)sender {
    NSFileManager *fm=[NSFileManager defaultManager];

    NSError *err;
    if( [fm removeItemAtPath:self.logFile error:&err]==NO){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"File Delete Error";
        alert.informativeText=[NSString stringWithFormat:@"The log file could not be deleted at %@.(%@)",self.logFile,err.localizedDescription];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

    }
    else {
        if([fm createFileAtPath:self.logFile contents:nil attributes:nil]==NO){
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"File Creation Error";
            alert.informativeText=[NSString stringWithFormat:@"The log file could not be created at %@.",self.logFile];

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];


        }
        else {


            [self setup];
        }
    }

}

- (void)windowDidLoad {
    [super windowDidLoad];

    [self.window setFrameAutosaveName:self.logFile.lastPathComponent];
    [self setup];

}
-(void)setup{
    NSFileManager *fm=[NSFileManager defaultManager];
    self.tailer = [[BDFileTailer alloc] initWithURL:[NSURL fileURLWithPath:self.logFile]];
    self.shouldTail=YES;
    if (![fm fileExistsAtPath:self.logFile]){

        self.timer=nil;
       self.timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setup) userInfo:nil repeats:NO];

    }
    else{
        self.logTextView.string=@"";
        [self startUpdatingLog:self.logFile];
    }


}
-(void)startUpdatingLog:(NSString *)logPath{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        self.tailer = [[BDFileTailer alloc] initWithURL:[NSURL fileURLWithPath:logPath]];
        NSMutableString *currString=[NSMutableString string];
        self.tailer.shouldTail = NO;
        self.tailer.tailFrequency = 0.0;
        NSString *line = [self.tailer readLine];

        while (line) {
            [currString appendString:line];
            line = [self.tailer readLine];


        }
        dispatch_async(dispatch_get_main_queue(), ^{

            [[[self.logTextView textStorage] mutableString] appendString:currString];
            self.logTextView.textColor=[NSColor colorNamed:@"Wyse"];

            [self.logTextView scrollToEndOfDocument:self];

        });
        self.tailer.shouldTail = YES;
        self.tailer.tailFrequency = 1.0;

        while (self.shouldTail){
            NSString *line = [self.tailer readLine];
            dispatch_async(dispatch_get_main_queue(), ^{
               if (line) [self addLine:line];

            });
        }

    });
}
-(void)stop{
    self.shouldTail=NO;
    [self.tailer stopTailing];

}
-(void)addLine:(NSString *)line{


    [[[self.logTextView textStorage] mutableString] appendString:line];


}

-(void)dealloc{


}
-(void)windowWillClose:(NSNotification *)notification{

    [self stop];
    self.timer=nil;
    self.tailer=nil;
    [self.delegate logWindowClosed];

}

@end
